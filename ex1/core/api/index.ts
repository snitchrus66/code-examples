import * as domain from '../domain'

export type MongoDBDocumentObject = { id?: any }

export type StringFilterForCategory =
    & MongoDBDocumentObject
    & Pick<domain.IFeature, 'name' | 'kind'>
    & { values: (MongoDBDocumentObject & Pick<domain.IValueString, 'val'>)[] }
export type NumberFilterForCategory =
    & MongoDBDocumentObject
    & Pick<domain.IFeature, 'name' | 'kind'>
    & { min: number | null, max: number | null }
export type FilterForCategory = StringFilterForCategory | NumberFilterForCategory
export type GetFilterForCategoryResponse = FilterForCategory[]

export type SelectedFilter =
    | {
          featureId: string
          valueType: 'String'
          values: string[]
      }
    | {
          featureId: string
          valueType: 'Number'
          min: number | null
          max: number | null
      }
