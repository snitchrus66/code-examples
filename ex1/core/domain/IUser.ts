export interface IUser {
    surname: string
    name: string
    patronym: string
    email: string
    passwordHash: string
    salt: string
    isActive: boolean
    roles: string[]
    expireAt: Date
}
