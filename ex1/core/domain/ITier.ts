import { IProduct } from './IProduct'

export interface ITier {
    name: string
    description: string
    products: IProduct[]
}
