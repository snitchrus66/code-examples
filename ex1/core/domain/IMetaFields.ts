export interface IMetaFields {
    metaTitle: string
    metaDescription: string
    metaKeywords: string
}
