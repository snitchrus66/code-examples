export interface ICategory {
    name: string
    description: string
    parentCategory: ICategory
    published: boolean
}

export interface ICategory1C extends ICategory {
    id1C: string
    parentId1C: string
}
