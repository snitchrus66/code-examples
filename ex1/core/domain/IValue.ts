import { IFeature } from './IFeature'

export interface IValue {
    feature: IFeature
    val: string | number
}

export interface IValueString extends IValue {
    val: string
}

export interface IValueNumber extends IValue {
    val: number
}
