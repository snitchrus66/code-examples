import { ICategory } from './ICategory'
import { IValue } from './IValue'

export interface IProduct {
    category: ICategory
    coverImagePath: string
    imagePaths: string[]
    videoIds: string[]
    name: string
    price: number
    discountPrice: number
    values: IValue[]
    description: string
    published: boolean
}


export interface IProduct1C extends IProduct {
    id1C: string
    parentId1C: string
}
