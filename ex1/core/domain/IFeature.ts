import { ICategory } from './ICategory'

export enum EFeatureKind {
    CHECK = 'CHECK',
    RADIO = 'RADIO',
    NUMBER = 'NUMBER',
    GROUP = 'GROUP'
}

export interface IFeature {
    category: ICategory,
    parentFeature: IFeature,
    name: string,
    kind: EFeatureKind
}
