import { IUser } from './IUser'

export enum ETokenKind {
    PASSWORD_RESET = 'PASSWORD_RESET',
    USER_ACTIVATION = 'USER_ACTIVATION'
}

export interface IToken {
    user: IUser
    kind: ETokenKind
    salt: string
    expireAt: Date
}
