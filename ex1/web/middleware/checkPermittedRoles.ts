/* eslint-disable @typescript-eslint/no-explicit-any */
import { Middleware } from '@nuxt/types'

const checkPermittedRolesMiddleware: Middleware = ({ route, store, error }) => {
    // Get permittedRoles from `.permittedRoles` array defined in page components (parent + children)
    const permittedRoles = [].concat(
        ...route.matched.map((matchedRoute) => {
            return (matchedRoute.components.default as any).options
                ? (matchedRoute.components.default as any).options
                      .permittedRoles
                : (matchedRoute.components.default as any).permittedRoles
        })
    )
    let accessIsPermitted = false

    permittedRoles.forEach((role) => {
        if (
            store.state.user &&
            store.state.user.roles &&
            store.state.user.roles.find((x: string) => x === role)
        ) {
            accessIsPermitted = true
        }
    })
    if (!accessIsPermitted) {
        return error({
            statusCode: 403,
            message: 'У вас нет прав для просмотра этой страницы'
        })
    }
}

export default checkPermittedRolesMiddleware
