module.exports = {
    root: true,
    env: {
        browser: true,
        node: true
    },
    overrides: [
        {
            files: ['./**/*.vue'],
            parser: 'vue-eslint-parser', // This parser allows to lint the <template> of .vue files. (https://github.com/mysticatea/vue-eslint-parser)
            parserOptions: {
                // option: Custom parser to parse the <script> of .vue files.
                parser: '@typescript-eslint/parser' // An ESLint-specific parser which leverages typescript-estree and is designed to be used as a replacement for ESLint's default parser, espree. (https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/parser)
            },
            plugins: [
                // package: @typescript-eslint/eslint-plugin
                '@typescript-eslint', // An ESLint-specific plugin which, when used in conjunction with @typescript-eslint/parser, allows for TypeScript-specific linting rules to run. (https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin)
                // package: eslint-plugin-vue
                'vue', // This plugin allows to check the <template> and <script> of .vue files with ESLint. (https://eslint.vuejs.org/, https://github.com/vuejs/eslint-plugin-vue)
                'prettier',
                'vuetify'
            ],
            extends: [
                'eslint:recommended', // Default eslint recommended set of rules.
                'plugin:@typescript-eslint/eslint-recommended', // @typescript-eslint/eslint-plugin eslint-recommended set of riles.
                'plugin:@typescript-eslint/recommended', // @typescript-eslint/eslint-plugin recommended set of riles.
                'plugin:vue/recommended', // eslint-plugin-vue recommended set of riles.
                '@nuxtjs',
                'prettier',
                'prettier/vue',
                'plugin:prettier/recommended',
                'plugin:nuxt/recommended'
            ],
            rules: {
                'semi': ['error', 'never'],
                'quotes': ['error', 'single', {
                    allowTemplateLiterals: true
                }],
                'quote-props': ['error', 'consistent'],
                'comma-dangle': ['error', 'never'],
                'arrow-parens': 'off',
                'lines-between-class-members': 'off',
                //'vue/html-indent': ['error', 4, {
                //    alignAttributesVertically: true
                //}],
                //'vue/script-indent': ['error', 4, {
                //    baseIndent: 0,
                //    switchCase: 1,
                //    ignores: []
                //}],
                //'vue/max-attributes-per-line': ['error', {
                //    singleline: 1,
                //    multiline: {
                //        max: 1,
                //        allowFirstLine: false
                //    }
                //}],
                //'vue/html-closing-bracket-newline': ['error', {
                //    singleline: 'never',
                //    multiline: 'never'
                //}],
                //'vue/html-closing-bracket-spacing': ['error', {
                //    startTag: 'never',
                //    endTag: 'never',
                //    selfClosingTag: 'always'
                //}],
                'vue/no-v-html': 'off',
                '@typescript-eslint/explicit-function-return-type': 'off',
                '@typescript-eslint/explicit-member-accessibility': 'off',
                '@typescript-eslint/member-delimiter-style': 'off',
                'vuetify/no-deprecated-classes': 'error',
                'vuetify/grid-unknown-attributes': 'error',
                'vuetify/no-legacy-grid': 'error'
            }
        },
        {
            files: ['./**/*.ts'],
            parser: '@typescript-eslint/parser',
            plugins: [
                '@typescript-eslint',
                'prettier'
            ],
            extends: [
                'eslint:recommended',
                'plugin:@typescript-eslint/eslint-recommended',
                'plugin:@typescript-eslint/recommended',
                'prettier',
                'plugin:prettier/recommended'
            ],
            rules: {
                'semi': ['error', 'never'],
                'quotes': ['error', 'single', {
                    allowTemplateLiterals: true
                }],
                'quote-props': ['error', 'consistent'],
                'comma-dangle': ['error', 'never'],
                'arrow-parens': 'off',
                'lines-between-class-members': 'off',
                '@typescript-eslint/explicit-function-return-type': 'off',
                '@typescript-eslint/explicit-member-accessibility': 'off',
                '@typescript-eslint/no-empty-function': 'off',
                '@typescript-eslint/member-delimiter-style': 'off'
            }
        },
        {
            files: ['./**/*.js'],
            parserOptions: {
                parser: 'babel-eslint'
            },
            extends: [
                'eslint:recommended',
                '@nuxtjs',
                'prettier',
                'plugin:prettier/recommended',
                'plugin:nuxt/recommended'
            ],
            plugins: [
                'prettier'
            ],
            rules: {
            }
        }
    ]
}
