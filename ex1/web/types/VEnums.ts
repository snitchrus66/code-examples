export enum VAlertTypes {
    success = 'success',
    info = 'info',
    warning = 'warning',
    error = 'error'
}

export enum VColors {
    primary = 'primary',
    secondary = 'secondary',
    accent = 'accent',
    success = 'success',
    info = 'info',
    warning = 'warning',
    error = 'error'
}

export enum VTransitions {
    bottomSheetTransition = 'bottom-sheet-transition',
    carouselTransition = 'carousel-transition',
    carouselReverseTransition = 'carousel-reverse-transition',
    dialogTransition = 'dialog-transition',
    dialogBottomTransition = 'dialog-bottom-transition',
    pickerTransition = 'picker-transition',
    pickerReverseTransition = 'picker-reverse-transition',
    pickerTitleTransition = 'picker-title-transition',
    tabTransition = 'tab-transition',
    tabReverseTransition = 'tab-reverse-transition',
    expandTransition = 'expand-transition',
    scaleTransition = 'scale-transition',
    messageTransition = 'message-transition',
    slideYTransition = 'slide-y-transition',
    slideYReverseTransition = 'slide-y-reverse-transition',
    scrollYTransition = 'scroll-y-transition',
    scrollYReverseTransition = 'scroll-y-reverse-transition',
    scrollXTransition = 'scroll-x-transition',
    scrollXReverseTransition = 'scroll-x-reverse-transition',
    slideXTransition = 'slide-x-transition',
    slideXReverseTransition = 'slide-x-reverse-transition',
    fadeTransition = 'fade-transition',
    fabTransition = 'fab-transition'
}
