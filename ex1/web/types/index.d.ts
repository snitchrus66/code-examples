import Vue, { ComponentOptions } from 'vue'
import { FieldFlagsBag, ErrorBag } from 'vee-validate'
import { NuxtAxiosInstance } from '@nuxtjs/axios'

declare module '*.vue' {
    import Vue from 'vue'
    export default Vue
}

declare module 'vue/types/options' {
    /* Аугументация объекта опций передаваемого в декоратор Component */
    interface ComponentOptions<V extends Vue> {
        permittedRoles?: string[]
    }
}

declare module 'vue/types/vue' {
    /* Аугументация экземпляра класса Vue */
    interface Vue {
        fields: FieldFlagsBag
        errors: ErrorBag
        $axios: NuxtAxiosInstance
    }
}

declare module '@nuxt/vue-app' {
    /* Аугументация NuxtContext */
    //interface Context {
    //    $axios: NuxtAxiosInstance
    //}
}
