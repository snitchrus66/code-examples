import packageJSON from './package.json'
import webpack from 'webpack'
import dotenv from 'dotenv'
import path from 'path'
import colors from 'vuetify/es5/util/colors'
import { Configuration } from '@nuxt/types'

const dotEnvPath = path.join(__dirname)
let dotEnvFileName

if (process.env?.NODE_ENV?.trim() == 'production') {
    dotEnvFileName = '.env.prod'
} else {
    dotEnvFileName = '.env.dev'
}
dotenv.config({ path: path.join(dotEnvPath, `/${dotEnvFileName}`) })

const config: Configuration = {
    mode: 'universal',
    server: {
        host: process.env.HTTP_HOST,
        port: process.env.HTTP_PORT
    },
    router: {
        extendRoutes(routes) {
            const catalogIdSlugRoute = routes.find(
                (x) => x.path == '/catalog/:id?/:slug?'
            )
            const productIdSlugRoute = routes.find(
                (x) => x.path == '/product/:id?/:slug?'
            )

            if (catalogIdSlugRoute && productIdSlugRoute) {
                catalogIdSlugRoute.path = '/catalog/:id/:slug?'
                productIdSlugRoute.path = '/product/:id/:slug?'
            }
        },
        scrollBehavior(to, from, savedPosition) {
            if (savedPosition) {
                // savedPosition is only available for popstate navigations.
                return savedPosition
            } else {
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                const position: any = {}

                // scroll to anchor by returning the selector
                if (to.hash) {
                    position.selector = to.hash
                    if (document.querySelector(to.hash)) {
                        return position
                    }
                    // if the returned position is falsy or an empty object,
                    // will retain current scroll position.
                    return false
                }

                return new Promise((resolve) => {
                    // check if any matched route config has meta that requires scrolling to top
                    if (to.matched.some((m) => m.meta.scrollToTop)) {
                        // coords will be used if no selector is provided,
                        // or if the selector didn't match any element.
                        position.x = 0
                        position.y = 0
                    }
                    // wait for the out transition to complete (if necessary)
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    ;(this as any).app.$root.$once('triggerScroll', () => {
                        // if the resolved position is falsy or an empty object,
                        // will retain current scroll position.
                        resolve(position)
                    })
                })
            }
        }
    },
    /*
     ** Headers of the page
     */
    head: {
        //titleTemplate: `%s ${packageJSON.name ??
        //    process.env.npm_package_name ??
        //    ''}`,
        title: `${packageJSON.name ?? process.env.npm_package_name ?? ''}`,
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content:
                    packageJSON.description ??
                    process.env.npm_package_description ??
                    ''
            }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'stylesheet',
                href:
                    'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
            }
        ]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: colors.red.darken3 },
    /*
     ** Global CSS
     */
    css: [
        // 'quill/dist/quill.snow.css',
        // 'quill/dist/quill.bubble.css',
        // 'quill/dist/quill.core.css'
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '@/plugins/vee-validate',
        { src: '@/plugins/vue-quill-editor', ssr: false },
        '@/plugins/vue-router-extend'
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        '@nuxt/typescript-build',
        // Doc: https://github.com/nuxt-community/eslint-module
        '@nuxtjs/eslint-module',
        // Doc: https://github.com/nuxt-community/stylelint-module
        //'@nuxtjs/stylelint-module',
        '@nuxtjs/vuetify'
    ],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        // Doc: https://github.com/nuxt-community/dotenv-module
        [
            '@nuxtjs/dotenv',
            {
                path: dotEnvPath,
                filename: dotEnvFileName
            }
        ],
        '@nuxtjs/style-resources',
        [
            '@nuxtjs/moment',
            {
                locales: ['ru'],
                defaultLocale: 'ru',
                plugin: false
            }
        ]
    ],
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {
        baseURL:
            `http://${process.env.API_HTTP_HOST}` +
            `${
                process.env.API_HTTP_PORT ? `:${process.env.API_HTTP_PORT}` : ''
            }`,
        credentials: true
    },
    /*
     ** vuetify module configuration
     ** https://github.com/nuxt-community/vuetify-module
     */
    vuetify: {
        customVariables: ['~/assets/variables.scss'],
        treeShake: true,
        theme: {
            themes: {
                light: {
                    primary: colors.red.darken3,

                    secondary: colors.amber.darken3, // WTF?!
                    accent: colors.grey.darken3, // WTF?!
                    success: colors.green.accent3, // WTF?!
                    info: colors.teal.lighten1, // WTF?!
                    warning: colors.amber.base, // WTF?!
                    error: colors.red.base // WTF?!
                }
            }
        }
    },
    styleResources: {
        sass: ['vuetify/src/styles/tools/_functions.sass'],
        scss: [
            'vuetify/src/styles/settings/_colors.scss',
            'vuetify/src/styles/settings/_variables.scss',
            'vuetify/src/styles/settings/_light.scss',
            'vuetify/src/styles/settings/_dark.scss',
            'vuetify/src/styles/settings/_theme.scss'
        ]
    },
    /*
     ** Build configuration
     */
    build: {
        plugins: [
            new webpack.ProvidePlugin(
                /* prettier-ignore*/ {
                    '$': 'jquery',
                    'jQuery': 'jquery',
                    'window.jQuery': 'jquery'
                }
            )
        ],
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {
            console.log(JSON.stringify(config.output, null, 2))
            // config.output = {
            //     publicPath: ''
            // }

            if (ctx.isDev) {
                config.devtool = ctx.isClient
                    ? 'source-map'
                    : 'inline-source-map'
            }
        }
    }
}

export default config
