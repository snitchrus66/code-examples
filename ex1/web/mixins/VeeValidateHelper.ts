import { Vue, Component } from 'vue-property-decorator'

@Component({
    inject: ['$validator'],
    // eslint-disable-next-line @typescript-eslint/camelcase
    $_veeValidate: { validator: 'new' }
})
export default class VeeValidateHelper extends Vue {
    getFieldErrorMessages(fieldName: string, fieldScope?: string) {
        if (fieldScope) {
            if (
                this.fields &&
                this.fields[`$${fieldScope}`] &&
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                (this.fields[`$${fieldScope}`] as any)[fieldName] &&
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                (this.fields[`$${fieldScope}`] as any)[fieldName].touched
            ) {
                return this.errors.collect(fieldName, fieldScope)
            }
        } else {
            if (
                this.fields &&
                this.fields[fieldName] &&
                this.fields[fieldName].touched
            ) {
                return this.errors.collect(fieldName)
            }
        }
    }

    setAllFieldsTouched(fieldsScope?: string) {
        if (fieldsScope) {
            if (this.fields && this.fields[`$${fieldsScope}`]) {
                for (const fieldKey in this.fields[`$${fieldsScope}`]) {
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    ;(this.fields[`$${fieldsScope}`] as any)[
                        fieldKey
                    ].touched = true
                }
            }
        } else {
            if (this.fields) {
                for (const fieldKey in this.fields) {
                    this.fields[fieldKey].touched = true
                }
            }
        }
    }
}
