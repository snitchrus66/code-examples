import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from '~/store'

export interface PageState {
    title: string | null
}

export const state: () => PageState = () => ({
    title: null
})

export const getters: GetterTree<PageState, RootState> = {}

export const actions: ActionTree<PageState, RootState> = {}

export const mutations: MutationTree<PageState> = {
    setTitle(state, payload: string) {
        state.title = payload
    }
}
