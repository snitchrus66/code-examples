import { ActionContext, GetterTree, ActionTree, MutationTree } from 'vuex'
import { Context } from '@nuxt/types'

export interface User {
    id: string
    email: string
    name?: string
    surname?: string
    patornym?: string
    roles: string[]
    isActive: boolean
    createdAt: Date
    updatedAt: Date
    expireAt?: Date
}

export interface Breadcrumb {
    path: string
    label: string
}

export interface MenuCategory {
    id: string
    name: string
    parentCategory: string | null
    active: boolean
    id1C?: string
    parentId1C?: string
}

export interface MenuCategoryTreeNode extends MenuCategory {
    children?: MenuCategoryTreeNode[]
}

export interface RootState {
    user: User | null
    //breadcrumbs: Breadcrumb[] | null;
    productsMenu: MenuCategoryTreeNode[] | null
    sparesMenu: MenuCategoryTreeNode[] | null
    sparesViewing: boolean
}

export const state: () => RootState = () => ({
    user: null,
    //breadcrumbs: null,
    productsMenu: null,
    sparesMenu: null,
    sparesViewing: false
})

export const getters: GetterTree<RootState, RootState> = {}

export const actions: ActionTree<RootState, RootState> = {
    async nuxtServerInit(
        actionContext: ActionContext<RootState, RootState>,
        nuxtContext: Context
    ) {
        const getUserInfoPromise = () => {
            return (
                nuxtContext.$axios
                    .get('/account/userInfo')
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    .then((res: any) => {
                        actionContext.commit('setUser', res.data)
                    })
                    .catch(() => {
                        // Анонимный юзер
                        actionContext.commit('setUser', null)
                    })
            )
        }
        const getMenuProductsPromise = () => {
            return (
                nuxtContext.$axios
                    .get('/menu/products')
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    .then((res: any) => {
                        actionContext.commit('setProductsMenu', res.data)
                    })
                    .catch(() => {})
            )
        }

        await Promise.all([getUserInfoPromise(), getMenuProductsPromise()])
    }
}

export const mutations: MutationTree<RootState> = {
    setUser(state: RootState, payload: User) {
        state.user = payload
    },
    // setBreadcrumbs(state: RootState, payload: Breadcrumb[]) {
    //     state.breadcrumbs = payload
    // },
    setProductsMenu(state: RootState, payload: MenuCategoryTreeNode[]) {
        state.productsMenu = payload
    },
    setSparesMenu(state: RootState, payload: MenuCategoryTreeNode[]) {
        state.sparesMenu = payload
    },
    setSparesViewing(state: RootState, payload: boolean) {
        state.sparesViewing = payload
    }
}
