import * as api from '@core/api'
//import * as domain from '@core/domain'

export type StringCategoryFilter = api.StringFilterForCategory & {
    selectedValueIds: string[]
}

export type NumberCategoryFilter = api.NumberFilterForCategory & {
    selectedMin: number | null | undefined
    selectedMax: number | null | undefined
}

export default class CategoryFilter {
    filters: (StringCategoryFilter | NumberCategoryFilter)[] = []

    constructor(ffc: api.FilterForCategory[]) {
        ffc.forEach((filter) => {
            if ('values' in filter) {
                this.filters.push({
                    id: filter.id,
                    kind: filter.kind,
                    name: filter.name,
                    values: filter.values,
                    selectedValueIds: []
                })
            }
            if ('min' in filter && 'max' in filter) {
                this.filters.push({
                    id: filter.id,
                    kind: filter.kind,
                    name: filter.name,
                    min: filter.min,
                    max: filter.max,
                    selectedMin: null,
                    selectedMax: null
                })
            }
        })
    }

    get selectedFilters(): api.SelectedFilter[] {
        const result: api.SelectedFilter[] = []

        this.filters.forEach((filter) => {
            if (filter != undefined) {
                if ('values' in filter) {
                    if (filter.selectedValueIds.length) {
                        result.push({
                            featureId: filter.id,
                            valueType: 'String',
                            values: filter.selectedValueIds
                        })
                    }
                }
                if ('min' in filter && 'max' in filter) {
                    const selectedNumberFilter: api.SelectedFilter = {
                        featureId: filter.id,
                        valueType: 'Number',
                        min: null,
                        max: null
                    }
                    const wasSelectedMinValue =
                        filter.selectedMin !== null &&
                        filter.selectedMin !== undefined
                    const wasSelectedMaxValue =
                        filter.selectedMax !== null &&
                        filter.selectedMax !== undefined

                    if (wasSelectedMinValue || wasSelectedMaxValue) {
                        selectedNumberFilter.min = wasSelectedMinValue
                            ? (filter.selectedMin as number)
                            : null
                        selectedNumberFilter.max = wasSelectedMaxValue
                            ? (filter.selectedMax as number)
                            : null
                        result.push(selectedNumberFilter)
                    }
                }
            }
        })

        return result
    }

    get selectedFiltersAsQueryString() {
        const result: {
            [featureId: string]:
                | string[]
                | { min: number | null; max: number | null }
        } = {}

        this.selectedFilters.forEach((selectedFilter) => {
            if (selectedFilter.valueType == 'String') {
                result[selectedFilter.featureId] = selectedFilter.values
            }
            if (selectedFilter.valueType == 'Number') {
                result[selectedFilter.featureId] = {
                    min: selectedFilter.min != null ? selectedFilter.min : null,
                    max: selectedFilter.max != null ? selectedFilter.max : null
                }
            }
        })

        return result
    }
}
