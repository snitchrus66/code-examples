import Vue from 'vue'
import VeeValidate from 'vee-validate'

Vue.use(VeeValidate, {
    locale: 'ru',
    dictionary: {
        ru: {
            messages: {
                required: 'Необходимо заполнить',
                email: 'Некорректный email',
                min: (fieldName, args) => `Минимальная длина ${args[0]}`,
                max: (fieldName, args) => `Максимальная длина ${args[0]}`,
                confirmed: 'Значения не совпадают'
            }
        }
    },
    //errorBagName: '$errors',
    //fieldsBagName: '$fields',
    inject: false
})
