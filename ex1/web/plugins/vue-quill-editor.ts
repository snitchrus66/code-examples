import Vue from 'vue'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const VueQuillEditor = require('vue-quill-editor/dist/ssr')

Vue.use(VueQuillEditor)
