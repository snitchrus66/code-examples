import { Context } from '@nuxt/types'

// const breadcrumbs = {
//     '/': 'Главная',
//     '/login': 'Вход',
//     '/logout': 'Выход',
//     '/register': 'Регистрация',
//     '/activateAccount': 'Активация аккаунта',
//     '/resetPassword': 'Сброс пароля',
//     '/forgetPassword': 'Восстановление пароля',
//     '/admin': 'Админка',
//     '/admin/categories': 'Категории',
//     '/admin/categories/:id': 'Категория',
//     '/admin/features': 'Характеристики',
//     '/admin/features/:id': 'Характеристика',
//     '/admin/products': 'Товары',
//     '/admin/products/:id': 'Товар',
//     '/admin/tiers': 'Серии',
//     '/admin/tiers/:id': 'Серия',
//     '/admin/sparesImport': 'Импорт запчастей',
//     '/products': 'Товары',
//     '/products/:id': 'Категория товаров'
// }

export default (nuxtContext: Context) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    nuxtContext.app.router!.beforeEach((to, from, next) => {
        if (to.name != 'catalog-id-slug' && to.name != 'product-id-slug') {
            nuxtContext.store.commit('setSparesViewing', false)
        }
        next()
    })
    // Every time the route changes (fired on initialization too)
    // nuxtContext.app['router'].afterEach((to, from) => {
    //     if (to.path != '') {
    //         let pathParts = [
    //             '/',
    //             ...nuxtContext.route.matched[0]['path']
    //                 .split('/')
    //                 .filter((x) => x)
    //                 .map((x) => `/${x}`)
    //         ]
    //         let result: { label: string; path: string }[] = []
    //         let routeMemo = ''

    //         pathParts.forEach((routePart, index) => {
    //             if (routePart != '/') {
    //                 routeMemo += routePart
    //             }

    //             result.push({
    //                 label:
    //                     breadcrumbs[routePart == '/' ? routePart : routeMemo],
    //                 path: routePart
    //             })
    //         })
    //         nuxtContext.store.commit('setBreadcrumbs', result)
    //     }
    // })
}
