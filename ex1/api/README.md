# atv-vostok-api

> ATV-VOSTOK API.

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with watch
$ yarn run watch

# build for production and launch server
$ yarn run build
$ yarn start
```
