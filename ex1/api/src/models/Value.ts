import mongoose from 'mongoose'
import * as domain from '@core/domain'
import { IFeature } from '@/src/models/Feature'

// #region Value

export interface IValue extends domain.IValue, mongoose.Document {
    feature: IFeature['_id']
}

const schema = new mongoose.Schema({
    feature: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Feature'
    }
}, {
    timestamps: true
})

export const Value = mongoose.model<IValue>('Value', schema)

export default Value

// #endregion

// #region ValueString

export interface IValueString extends domain.IValueString, mongoose.Document {
    val: string
}

const schemaString = new mongoose.Schema({
    val: { type: String, trim: true }
})

export const ValueString = Value.discriminator<IValueString>('ValueString', schemaString)

// #endregion

// #region ValueNumber

export interface IValueNumber extends domain.IValueNumber, mongoose.Document {
    val: number | null
}

const schemaNumber = new mongoose.Schema({
    val: { type: Number }
})

export const ValueNumber = Value.discriminator<IValueNumber>('ValueNumber', schemaNumber)

// #endregion
