import mongoose from 'mongoose'
import * as domain from '@core/domain'
import { ICategory } from '@/src/models/Category'

export interface IFeature extends domain.IFeature, mongoose.Document {
    category: ICategory['_id'],
    parentFeature: IFeature['_id']
}

const schema = new mongoose.Schema({
    category: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Category'
    },
    parentFeature: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Feature'
    },
    name: { type: String, trim: true, required: true },
    kind: { type: String, trim: true, required: true }
}, {
    timestamps: true
})

export const Feature = mongoose.model<IFeature>('Feature', schema)

export default Feature
