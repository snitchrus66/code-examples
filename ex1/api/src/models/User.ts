import _ from 'lodash'
import crypto from 'crypto'
import mongoose from 'mongoose'
import validator from 'validator'
import * as domain from '@core/domain'
import encryptString from '@/src/utils/encryptString'

export interface IUser extends domain.IUser, mongoose.Document {
    password?: string
    /* Instance methods */
    isPasswordValid(password: string): boolean,
    gravatar(size: number): string
}

const schema = new mongoose.Schema({
    surname: { type: String, trim: true },
    name: { type: String, trim: true },
    patronym: { type: String, trim: true },
    email: {
        type: String,
        unique: 'Пользователь с таким email-ом уже зарегистрирован',
        required: [true, 'Не указан email'],
        lowercase: true,
        trim: true,
        validate: {
           validator: (value: any) => validator.isEmail(value),
           msg: 'Некорректный email'
        }
    },
    passwordHash: { type: String, required: true, select: false, private: true },
    salt: { type: String, required: true, select: false, private: true },
    isActive: { type: Boolean },
    roles: { type: [String] },
    expireAt: { type: Date }
}, {
    timestamps: true
})

schema.index({ 'expireAt': 1 }, { expireAfterSeconds: 0 })

schema
    .virtual('password')
    .set(function (password: string) {
        this.salt = ' '
        this.passwordHash = ' '

        if (_.isUndefined(password) || validator.isEmpty(password)) {
            this.invalidate('password', 'Не указан пароль')

            return
        }
        if (!validator.isLength(password, { min: 5, max: 16 })) {
            this.invalidate('password', 'Пароль должен быть длиной минимум 5, максимум 16 символов')

            return
        }
        this.salt = crypto.randomBytes(Math.ceil(16 / 2))
            .toString('hex')
            .slice(0, 16)
        this.passwordHash = encryptString(password, this.salt)
    })
    .get((): undefined => {
        return undefined
    })

schema.methods = {
    isPasswordValid(password: string) {
        return this.passwordHash === encryptString(password, this.salt)
    },
    gravatar(size: number) {
        if (!size) {
            size = 200
        }

        if (!this.email) {
            return `https://gravatar.com/avatar/?s=${size}&d=retro`
        }

        const md5 = crypto.createHash('md5').update(this.email).digest('hex')

        return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`
    }
}

export const User = mongoose.model<IUser>('User', schema)

export default User
