import crypto from 'crypto'
import moment from 'moment'
import mongoose from 'mongoose'
import * as domain from '@core/domain'
import { IUser } from '@/src/models/User'
import encryptString from '@/src/utils/encryptString'

export interface IToken extends domain.IToken, mongoose.Document {
    user: IUser['_id']
    /* Instance methods */
    toIssuedToken(): string
}

export interface ITokenModel extends mongoose.Model<IToken> {
    /* Static methods */
    issueToken(userId: string, tokenKind: domain.ETokenKind): Promise<string>
    validateIssuedToken(issuedToken: string): Promise<IToken | false>
}

const schema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    kind: { type: String },
    salt: { type: String, select: false },
    expireAt: { type: Date }
}, {
    timestamps: true
})

schema.index({ 'expireAt': 1 }, { expireAfterSeconds: 0 })

schema.methods = {
    toIssuedToken() {
        let sign = encryptString(this.id, this.salt)

        return Buffer.from(`${this.tokenKind}.${this.id}.${sign}`).toString('base64')
    }
}

schema.statics = {
    async issueToken(userId: string, tokenKind: domain.ETokenKind) {
        let Token: ITokenModel = this
        let newToken = new Token({
            user: userId,
            kind: tokenKind,
            salt: crypto.randomBytes(Math.ceil(16 / 2))
                .toString('hex')
                .slice(0, 16)
        })
        let tokenLifeTimeValue =
            process.env[`${tokenKind}_TOKEN_LIFETIME_VALUE`]
        let tokenLifeTimeValueType =
            process.env[`${tokenKind}_TOKEN_LIFETIME_VALUE_TYPE`]

        if (tokenLifeTimeValue
            && tokenLifeTimeValueType) {
            newToken.expireAt = (<any>moment)
                .utc()
                .add(tokenLifeTimeValue, tokenLifeTimeValueType)
                .toDate()
        }

        await newToken.save()

        let sign = encryptString(newToken.id, newToken.salt)

        return Buffer.from(`${tokenKind}.${newToken.id}.${sign}`).toString('base64')
    },
    async validateIssuedToken(issuedToken: string) {
        let Token: ITokenModel = this
        let decodedTokenParts = Buffer.from(issuedToken, 'base64').toString('ascii').split('.')
        let kind = decodedTokenParts[0]
        let id = decodedTokenParts[1]
        let sign = decodedTokenParts[2]
        let existingToken = await Token.findById(id).select('+salt').exec()

        if (existingToken
            && kind == existingToken.kind
            && sign === encryptString(existingToken.id, existingToken.salt)) {
            return existingToken
        } else {
            return false
        }
    }
}

export const Token: ITokenModel = mongoose.model<IToken, ITokenModel>('Token', schema)

export default Token
