import mongoose from 'mongoose'
import * as domain from '@core/domain'
import { IProduct } from '@/src/models/Product'

export interface ITier extends domain.ITier, mongoose.Document {
    products: IProduct['_id'][]
}

const schema = new mongoose.Schema({
    name: { type: String, trim: true, required: true },
    description: { type: String, trim: true },
    products: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    }]
}, {
    timestamps: true
})

export const Tier = mongoose.model<ITier>('Tier', schema)

export default Tier
