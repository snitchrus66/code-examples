import mongoose from 'mongoose'
import * as domain from '@core/domain'
import { MongooseSchemaMetaFields } from '@/src/models/_Common'

// #region Category

export interface ICategory extends domain.ICategory, domain.IMetaFields, mongoose.Document {
    parentCategory: ICategory['_id']
    id1C?: string
    parentId1C?: string
}

const schema = new mongoose.Schema({
    name: { type: String, trim: true, required: true },
    description: { type: String, trim: true },
    parentCategory: { type: mongoose.Schema.Types.ObjectId, ref: 'Category' },
    published: { type: Boolean, default: false },
    ...MongooseSchemaMetaFields
}, {
    timestamps: true
})

export const Category = mongoose.model<ICategory>('Category', schema)

export default Category

// #endregion

// #region Category1C

export interface ICategory1C extends domain.ICategory1C, mongoose.Document  {}

const schema1C = new mongoose.Schema({
    id1C: { type: String, trim: true, required: true },
    parentId1C: { type: String, trim: true }
})

export const Category1C = Category.discriminator<ICategory1C>('Category1C', schema1C)

// #endregion
