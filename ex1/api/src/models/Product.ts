import mongoose from 'mongoose'
import * as domain from '@core/domain'
import { ICategory } from '@/src/models/Category'
import { IValue } from '@/src/models/Value'
import { MongooseSchemaMetaFields } from '@/src/models/_Common'

// #region Product

export interface IProduct extends domain.IProduct, domain.IMetaFields, mongoose.Document {
    category: ICategory['_id']
    values: IValue['_id'][]
    id1C?: string
    parentId1C?: string
}

const schema = new mongoose.Schema({
    category: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Category'
    },
    coverImagePath: { type: String, trim: true },
    imagePaths: [{ type: String, trim: true }],
    videoIds: [{ type: String, trim: true }],
    name: { type: String, trim: true, required: true },
    price: { type: Number },
    discountPrice: { type: Number },
    values: [{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Value'
    }],
    description: { type: String, trim: true },
    published: { type: Boolean, default: false },
    ...MongooseSchemaMetaFields
}, {
    timestamps: true
})

export const Product = mongoose.model<IProduct>('Product', schema)

export default Product

// #endregion

// #region Product1C

export interface IProduct1C extends domain.IProduct1C, mongoose.Document {
    id1C: string
    parentId1C: string
}

const schema1C = new mongoose.Schema({
    id1C: { type: String, trim: true, required: true },
    parentId1C: { type: String, trim: true }
})

export const Product1C = Product.discriminator<IProduct1C>('Product1C', schema1C)

// #endregion
