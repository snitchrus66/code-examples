import _ from 'lodash'
import passport from 'passport'
import { Strategy as passportLocalStrategy } from 'passport-local'
import { User, IUser } from '@/src/models/User'

passport.serializeUser<IUser, string>((user, done) => {
    done(undefined, user.id)
})

passport.deserializeUser<IUser, string>(async (id, done) => {
    let user = await User.findById(id).exec().catch((err: any) => done(err))

    if (!user) {
        return done(new Error('Пользователь не найден'))
    }

    return done(undefined, user)
})

passport.use(new passportLocalStrategy({ usernameField: 'email' }, async (email, password, done) => {
    let user = await User.findOne({ email: email.toLowerCase() })
        .select('+passwordHash +salt').exec().catch((err: any) => done(err))

    if (!user) {
        return done(undefined, undefined, { message: 'Нет пользователя с таким email' })
    }

    if (user.isPasswordValid(password)) {
        return done(undefined, user)
    }

    return done(undefined, undefined, { message: 'Неверный пароль' })
}))
