declare namespace Express {
    interface User {
        toJSON: () => any
        roles: string[]
    }

    export interface Request {
       user?: User
    }
 }
