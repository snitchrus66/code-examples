export default function makeTree(startItems: any[], startItemId: any, parentIdKey: string) {
    if (!startItemId) {
        startItemId = null
    }

    let branchItems = startItems.filter(item => item[parentIdKey] == startItemId)

    branchItems.forEach((branchItem: any) => {
        let children = startItems.filter(item => item[parentIdKey] == branchItem.id)

        if (children && children.length) {
            branchItem.children = children
            makeTree(startItems, branchItem.id, parentIdKey)
        }
    })

    return branchItems
}
