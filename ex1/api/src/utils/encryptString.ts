import crypto from 'crypto'

export default (string: string, salt: string) => {
    return crypto
        .createHmac('sha256', salt)
        .update(string)
        .digest('hex')
}
