import { default as nodemailer, SendMailOptions } from 'nodemailer'
import { AuthOptions, SmtpOptions } from 'nodemailer-smtp-transport'

const smtpOptions: SmtpOptions = {
    host: process.env.SMTP_HOST,
    port: Number(process.env.SMTP_PORT),
    secure: false,
    auth: <AuthOptions>{
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS
    }
}
const webClientAddress = `${process.env.WEB_CLIENT_HOST}` +
        `${process.env.WEB_CLIENT_PORT
            ? `:${process.env.WEB_CLIENT_PORT}`
            : ''}`

export async function sendMail(options: SendMailOptions) {
    let smtpTransport = nodemailer.createTransport(smtpOptions)

    return await smtpTransport.sendMail(options)
}

export async function sendAccountActivationEmail(to: string, token: string) {
    return await sendMail({
        from: '"ATV-VOSTOK" <noreply@atv-vostok.ru>',
        to: to,
        subject: 'Активация аккаунта',
        html: `Активируйте ваш аккаунт, перейдя по <a href="http://${webClientAddress}/activateAccount?t=${token}">ссылке</a>.`
    })
}

export async function sendResetPasswordEmail(to: string, token: string) {
    return await sendMail({
        from: '"ATV-VOSTOK" <noreply@atv-vostok.ru>',
        to: to,
        subject: 'Сброс пароля',
        html: 'Для вашего аккаунта был запрошен сброс пароля, если это были не вы, то просто проигнорируйте это письмо.<br/>' +
            `Чтобы сбросить пароль перейдите по <a href="http://${webClientAddress}/resetPassword?t=${token}">ссылке</a>.`
    })
}
