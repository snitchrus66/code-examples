import { Request, Response, NextFunction } from 'express'

export default (err: any, req: Request, res: Response, next: NextFunction) => {
    let responseJSON: any = {}

    responseJSON.message = err && err.message ? err.message : 'Internal Error'

    if (err.errors) {
        responseJSON.errors = err.errors
    }

    if (process.env.NODE_ENV != 'production'
        && err.stack) {
        responseJSON.stack = err.stack
    }

    return res.status(500).json(responseJSON)
}
