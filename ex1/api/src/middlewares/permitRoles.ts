import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

export default (permittedRoles: string[]) => {
    return (req: Request, res: Response, next: NextFunction) => {
        if (req.user && req.user.roles && req.user.roles.length) {
            let userHasPermittedRole = _.some(req.user.roles, (role) => {
                return _.indexOf(permittedRoles, role) > -1
            })

            if (userHasPermittedRole) {
                return next()
            }
        }

        return res.sendStatus(403)
    }
}
