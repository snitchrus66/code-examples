import { Request, Response, NextFunction } from 'express'

export default (req: Request, res: Response, next: NextFunction) => {
    res.header('Access-Control-Allow-Origin', <string>req.headers.origin)
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.header('Access-Control-Allow-Headers', 'Content-Type')
    res.header('Access-Control-Allow-Credentials', 'true')
    // intercept OPTIONS method
    if (req.method == 'OPTIONS') {
        return res.sendStatus(200)
    } else {
        return next()
    }
}
