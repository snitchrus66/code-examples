import { Controller, Get } from '@tsed/common'

@Controller('')
export default class HomeController {
    @Get('')
    async indexGet() {
        return 'Привет, это ATV-VOSTOK API.'
    }
}
