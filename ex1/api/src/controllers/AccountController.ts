import { Controller, Use, Get, Post, Req, Res, Next } from '@tsed/common'
import { Request, Response, NextFunction } from 'express'
import moment from 'moment'
import passport from 'passport'
import { IVerifyOptions } from 'passport-local'
import { User, IUser } from '@/src/models/User'
import * as domain from '@core/domain'
import { Token } from '@/src/models/Token'
import { sendAccountActivationEmail, sendResetPasswordEmail } from '@/src/utils/mailSender'
import permitAuth from '@/src/middlewares/permitAuth'

@Controller('/account')
export default class AccountController {
    @Post('/register')
    async register(@Req() req: Request, @Res() res: Response) {
        let existingUser = await User.findOne({ email: req.body.email }).exec()

        if (existingUser && !existingUser.isActive) {
            let existingUserActivationToken = await Token.findOne({ user: existingUser.id }).select('+salt').exec()
            let tokenToSend: string

            if (existingUserActivationToken) {
                tokenToSend = existingUserActivationToken.toIssuedToken()
            } else {
                tokenToSend = await Token.issueToken(existingUser.id, domain.ETokenKind.USER_ACTIVATION)
            }
            await sendAccountActivationEmail(existingUser.email, tokenToSend)
                .catch(async () => {
                    User.findByIdAndDelete(existingUser.id).exec()

                    let tokenToDelete = await Token.validateIssuedToken(tokenToSend)

                    if (tokenToDelete) {
                        tokenToDelete.remove()
                    }

                    throw Error('Не удается отправить email на указанную почту')
                })

            return res.sendStatus(200)
        } else {
            let newUser = new User({
                email: req.body.email,
                password: req.body.password,
                isActive: false,
                roles: ['User'],
                expireAt: (<any>moment)
                    .utc()
                    .add(
                        process.env.INACTIVE_USER_LIFETIME_VALUE,
                        process.env.INACTIVE_USER_LIFETIME_VALUE_TYPE
                    )
                    .toDate()
            })

            await newUser.save()

            let newUserActivationToken = await Token.issueToken(newUser.id, domain.ETokenKind.USER_ACTIVATION)

            await sendAccountActivationEmail(newUser.email, newUserActivationToken)
                .catch(async () => {
                    User.findByIdAndDelete(newUser.id).exec()

                    let tokenToDelete = await Token.validateIssuedToken(newUserActivationToken)

                    if (tokenToDelete) {
                        tokenToDelete.remove()
                    }

                    throw Error('Не удается отправить email на указанную почту')
                })

            return res.sendStatus(200)
        }
    }

    @Post('/login')
    login(@Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        passport.authenticate('local', (err: any, user: IUser, info: IVerifyOptions) => {
            if (err) { return next(err) }
            if (!user) {
                return res.sendStatus(401)
            }
            req.logIn(user, (err) => {
                if (err) { return next(err) }
                return res.send(user.toJSON())
            })
        })(req, res, next)
    }

    @Post('/logout')
    logout(@Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        req.logout()

        return res.sendStatus(200)
    }

    @Post('/activate')
    async activate(@Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let validToken = await Token.validateIssuedToken(req.body.token)

        if (validToken) {
            let inactiveUser = await User.findById(validToken.user.toString()).exec()

            inactiveUser.isActive = true
            inactiveUser.expireAt = undefined
            await inactiveUser.save()
            await validToken.remove()

            return res.sendStatus(200)
        }

        return res.sendStatus(401)
    }

    @Post('/resetPassword')
    async resetPassword(@Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let existingUser = await User.findOne({ email: req.body.email }).exec()

        if (existingUser) {
            let passwordResetToken = await Token.issueToken(existingUser.id, domain.ETokenKind.PASSWORD_RESET)

            await sendResetPasswordEmail(existingUser.email, passwordResetToken)
                .catch(async () => {
                    let tokenToDelete = await Token.validateIssuedToken(passwordResetToken)

                    if (tokenToDelete) {
                        tokenToDelete.remove()
                    }

                    throw Error('Не удается отправить email на указанную почту')
                })

            return res.sendStatus(200)
        } else {
            return res.sendStatus(404)
        }
    }

    @Post('/updatePassword')
    async updatePassword(@Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let validToken = await Token.validateIssuedToken(req.body.token)
        let password = req.body.password
        let confirmPassword = req.body.confirmPassword

        if (validToken) {
            if (password && confirmPassword && password === confirmPassword) {
                let existingUser = await User.findById(validToken.user.toString()).exec()

                if (existingUser) {
                    existingUser.password = password
                    await existingUser.save()
                    await validToken.remove()

                    return res.sendStatus(200)
                }

                return res.sendStatus(404)
            }

            return res.sendStatus(400)
        }

        return res.sendStatus(401)
    }

    @Get('/userInfo')
    @Use(permitAuth())
    async userInfo(@Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        return res.json(req.user.toJSON())
    }
}
