import _ from 'lodash'
import mongoose from 'mongoose'
import { Controller, Post, Get, Delete, Req, Res, Next, Use } from '@tsed/common'
import { Request, Response, NextFunction } from 'express'
import permitRoles from '@/src/middlewares/permitRoles'
import { Category } from '@/src/models/Category'
import * as domain from '@core/domain'
import { Feature } from '@/src/models/Feature'
import { Value } from '@/src/models/Value'
import makeTree from '@/src/utils/makeTree'

@Controller('/feature')
export default class FeatureController {
    // #region CRUD

    @Post('/edit')
    @Use(permitRoles(['Admin']))
    async upsert(@Req() req: Request, @Res() res: Response) {
        if (req.body.id) {
            let existingFeature = await Feature.findById(req.body.id).exec()

            existingFeature = Object.assign(existingFeature, req.body)
            await existingFeature.save()
        } else {
            let newFeature = new Feature(req.body)

            await newFeature.save()
        }

        return res.sendStatus(200)
    }

    @Get('/edit/:id')
    @Use(permitRoles(['Admin']))
    async getForEditById(@Req() req: Request, @Res() res: Response) {
        return await Feature.findById(req.params.id).exec()
    }

    @Delete('/edit/:id')
    @Use(permitRoles(['Admin']))
    async deleteById(@Req() req: Request, @Res() res: Response) {
        /*
        *   Удалить фичу.
        *       1. Удалить все значения этой фичи.
        *       2. Отвязать все дочерние фичи, если это была группа.
        */

        let featureCursor = Feature.findById(req.params.id).cursor()

        await featureCursor.eachAsync(async (featureToDelete) => {
            featureToDelete.remove()

            let valueCursor = Value.find({ feature: featureToDelete.id }).cursor()

            valueCursor.eachAsync((valueToDelete) => {
                valueToDelete.remove()
            })

            if (featureToDelete.kind == domain.EFeatureKind.GROUP) {
                await Feature.updateMany({ parentFeature: featureToDelete.id }, { $set: { parentFeature: null } }).exec()
            }
        })

        return res.sendStatus(200)
    }

    // #endregion

    @Get('/tree')
    async getTreeByCategoryId(@Req() req: Request, @Res() res: Response) {
        let existingCategory = await Category.findById(req.query.categoryId).exec()

        if (!existingCategory) {
            return res.sendStatus(500)
        }

        let features = await Feature.find({ category: req.query.categoryId }).exec()
        let treeSource = features.map((feature) => {
            let featureJSON = feature.toJSON()
            // TODO: Надо сделать, чтобы ObjectId кастились в строку во время toJSON.
            if (featureJSON.parentFeature && featureJSON.parentFeature.toString) {
                featureJSON.parentFeature = featureJSON.parentFeature.toString()
            }

            return featureJSON
        })

        return makeTree(treeSource, null, 'parentFeature')
    }

    @Get('/groups')
    async getFeatureGroups(@Req() req: Request, @Res() res: Response) {
        let featurGroups = Feature.find({ category: mongoose.Types.ObjectId(req.query.categoryId), kind: domain.EFeatureKind.GROUP }).exec()

        return featurGroups
    }
}
