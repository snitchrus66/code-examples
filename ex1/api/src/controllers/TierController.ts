import _ from 'lodash'
import { Controller, Post, Get, Delete, Req, Res, Next, Use } from '@tsed/common'
import { Request, Response, NextFunction } from 'express'
import permitRoles from '@/src/middlewares/permitRoles'
import { Tier } from '@/src/models/Tier'

@Controller('/tier')
export default class TierController {
    // #region CRUD

    @Post('/edit')
    @Use(permitRoles(['Admin']))
    async upsert(@Req() req: Request, @Res() res: Response) {
        if (req.body.id) {
            let existingTier = await Tier.findById(req.body.id).exec()

            existingTier = Object.assign(existingTier, req.body)
            await existingTier.save()
        } else {
            let newTier = new Tier(req.body)

            await newTier.save()
        }

        return res.sendStatus(200)
    }

    @Get('/edit/:id')
    @Use(permitRoles(['Admin']))
    async getForEditById(@Req() req: Request, @Res() res: Response) {
        return await Tier.findById(req.params.id).exec()
    }

    @Delete('/edit/:id')
    @Use(permitRoles(['Admin']))
    async deleteById(@Req() req: Request, @Res() res: Response) {
        /*
        *   Удалить тир.
        */

        let tierCursor = Tier.findById(req.params.id).cursor()

        await tierCursor.eachAsync((tierToDelete) => {
            tierToDelete.remove()
        })

        return res.sendStatus(200)
    }

    // #endregion

    @Get('/all')
    async getAll(@Req() req: Request, @Res() res: Response) {
        return await Tier.find({}).exec()
    }
}
