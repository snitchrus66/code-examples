import mongoose from 'mongoose'
import { Controller, Use, Get, Post, Req, Res, Next } from '@tsed/common'
import { Request, Response, NextFunction } from 'express'
import { Category, Category1C } from '@/src/models/Category'
import makeTree from '@/src/utils/makeTree'

@Controller('/menu')
export default class MenuController {
    @Get('/products')
    async getMenuProducts() {
        let non1CCategories = await Category.find({ '__t': { $ne: 'Category1C' } }).select('id name parentCategory').exec()
        let treeSource = non1CCategories.map((category) => {
            // TODO: Надо сделать, чтобы ObjectId кастились в строку во время toJSON.
            let categoryJSON = category.toJSON()

            if (categoryJSON.parentCategory && categoryJSON.parentCategory.toString) {
                categoryJSON.parentCategory = categoryJSON.parentCategory.toString()
            }
            categoryJSON.active = false

            return categoryJSON
        })

        return makeTree(treeSource, null, 'parentCategory')
    }

    @Get('/spares')
    async getMenuSpares(@Req() req: Request, @Res() res: Response) {
        let query: any = {}

        if (req.query.parentCategory) {
            query.parentCategory = mongoose.Types.ObjectId(req.query.parentCategory)
        } else {
            query = {
                $or: [
                    { parentCategory: { $exists: false } },
                    { parentCategory: null }
                ]
            }
        }

        let categories1C = await Category1C.find(query).select('id name parentCategory').exec()
        let treeSource = categories1C.map((category1C) => {
            // TODO: Надо сделать, чтобы ObjectId кастились в строку во время toJSON.
            let category1CJSON = category1C.toJSON()

            if (category1CJSON.parentCategory && category1CJSON.parentCategory.toString) {
                category1CJSON.parentCategory = category1CJSON.parentCategory.toString()
            }
            category1CJSON.active = false

            return category1CJSON
        })

        return makeTree(treeSource, req.query.parentCategory ? req.query.parentCategory : null, 'parentCategory')
    }
}
