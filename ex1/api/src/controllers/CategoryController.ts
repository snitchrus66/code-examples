import _ from 'lodash'
import mongoose from 'mongoose'
import { Controller, Post, Get, Delete, Req, Res, Next, Use } from '@tsed/common'
import { Request, Response, NextFunction } from 'express'
import * as api from '@core/api'
import * as domain from '@core/domain'
import permitRoles from '@/src/middlewares/permitRoles'
import { Category } from '@/src/models/Category'
import { Product } from '@/src/models/Product'
import { Feature, IFeature } from '@/src/models/Feature'
import { Value, ValueString, ValueNumber, IValueString, IValueNumber } from '@/src/models/Value'
import { Tier } from '@/src/models/Tier'
import makeTree from '@/src/utils/makeTree'
import asyncForEach from '@/src/utils/asyncForEach'

@Controller('/category')
export default class CategoryController {
    // #region CRUD

    @Post('/edit')
    @Use(permitRoles(['Admin']))
    async upsert(@Req() req: Request, @Res() res: Response) {
        if (req.body.id) {
            let existingCategory = await Category.findById(req.body.id).exec()

            existingCategory = Object.assign(existingCategory, req.body)
            await existingCategory.save()
        } else {
            let newCategory = new Category(req.body)

            await newCategory.save()
        }

        return res.sendStatus(200)
    }

    @Get('/edit/:id')
    @Use(permitRoles(['Admin']))
    async getForEditById(@Req() req: Request, @Res() res: Response) {
        return await Category.findById(req.params.id).exec()
    }

    @Delete('/edit/:id')
    @Use(permitRoles(['Admin']))
    async deleteById(@Req() req: Request, @Res() res: Response) {
        /*
        *   Удалить категорию.
        *       1. Удалить товары этой категории.
        *           1а. Удалить из тиров удаленные товары.
        **           1б. Удалить картинки товаров.
        *       2. Удалить фичи этой категории.
        *           2а. Удалить значения этих фич.
        *       3. Отписать дочерние категории от этой категории.
        */

        let categoryCursor = Category.findById(req.params.id).cursor()

        await categoryCursor.eachAsync(async (categoryToDelete) => {
            categoryToDelete.remove()

            let productCursor = Product.find({ category: categoryToDelete.id }).cursor()

            productCursor.eachAsync((productToDelete) => {
                productToDelete.remove()

                let tierCursor = Tier.find({ products: productToDelete.id }).cursor()

                tierCursor.eachAsync((tierToModify) => {
                    _.remove(<mongoose.Types.ObjectId[]>(tierToModify.products), x => x.toHexString() == productToDelete.id)
                    tierToModify.save()
                })
            })

            let featureCursor = Feature.find({ category: categoryToDelete.id }).cursor()

            featureCursor.eachAsync((featureToDelete) => {
                featureToDelete.remove()

                let valueCursor = Value.find({ feature: featureToDelete.id }).cursor()

                valueCursor.eachAsync((valueToDelete) => {
                    valueToDelete.remove()
                })
            })

            await Category.updateMany({ parentCategory: categoryToDelete.id }, { $set: { parentCategory: null } }).exec()
        })

        return res.sendStatus(200)
    }

    // #endregion

    @Get('/tree')
    async getTree(@Req() req: Request, @Res() res: Response) {
        let allCategories = await Category.find({}).exec()
        let treeSource = allCategories.map((category) => {
            let categoryJSON = category.toJSON()
            // TODO: Надо сделать, чтобы ObjectId кастились в строку во время toJSON.
            if (categoryJSON.parentCategory && categoryJSON.parentCategory.toString) {
                categoryJSON.parentCategory = categoryJSON.parentCategory.toString()
            }

            return categoryJSON
        })

        return makeTree(treeSource, null, 'parentCategory')
    }

    @Get('/allNone1C')
    async getAllNone1C(@Req() req: Request, @Res() res: Response) {
        return await Category.find({ '__t': { $ne: 'Category1C' } }).exec()
    }

    @Get('/:id')
    async getById(@Req() req: Request, @Res() res: Response) {
        return await Category.findById(req.params.id).exec()
    }

    @Get('/:id/filter')
    async getFilterForCategory(@Req() req: Request, @Res() res: Response) {
        let result: api.FilterForCategory[] = [];
        let categoryNonGroupFeatures = await Feature
            .find({
                category: req.params.id,
                kind: {
                    $ne: domain.EFeatureKind.GROUP
                }
            })
            .select('id name kind')
            .exec()

        if (categoryNonGroupFeatures.length) {
            await asyncForEach(categoryNonGroupFeatures, async (feature) => {
                if (feature.kind == domain.EFeatureKind.CHECK
                    || feature.kind == domain.EFeatureKind.RADIO) {
                    let featureValues = await ValueString
                        .find({ feature: feature.id })
                        .sort('val')
                        .collation({
                            locale: 'en_US',
                            numericOrdering: true
                        })
                        .select('id val')
                        .exec()

                    result.push({
                        id: feature.id,
                        name: feature.name,
                        kind: feature.kind,
                        values: featureValues.map(featureValue => {
                            let newFeatureValue = _.omit(featureValue.toObject(), '__t')

                            newFeatureValue.id = newFeatureValue._id
                            delete newFeatureValue._id

                            return newFeatureValue
                        })
                    })
                }

                if (feature.kind == domain.EFeatureKind.NUMBER) {
                    let minVal = await ValueNumber.findOne().sort('val').exec()
                    let maxVal = await ValueNumber.findOne().sort('-val').exec()

                    result.push({
                        id: feature.id,
                        name: feature.name,
                        kind: feature.kind,
                        min: minVal?.val,
                        max: maxVal?.val
                    })
                }
            })
        }

        return result
    }
}
