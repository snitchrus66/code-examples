import _ from 'lodash'
import mongoose from 'mongoose'
import { Controller, Post, Get, Delete, Req, Res, Next, Use } from '@tsed/common'
import {MultipartFile, MulterOptions} from '@tsed/multipartfiles'
import { Request, Response, NextFunction } from 'express'
import permitRoles from '@/src/middlewares/permitRoles'
import { Category, Category1C } from '@/src/models/Category'
import { Product, Product1C, IProduct } from '@/src/models/Product'
import * as domain from '@core/domain'
import { Feature } from '@/src/models/Feature'
import { Value, ValueNumber, ValueString, IValue } from '@/src/models/Value'
import { Tier } from '@/src/models/Tier'
import asyncForEach from '@/src/utils/asyncForEach'
import { rootDir } from '@/src/app'
import rimraf from 'rimraf'
import path from 'path'
import fs from 'fs'
import sharp from 'sharp'
import chardet from 'chardet'
import iconvlite from 'iconv-lite'
import csvParse from 'csv-parse'

@Controller('/product')
export default class ProductController {
    // #region CRUD

    private async checkValueIdsUsageAndDeleteUnused(valueIds: string[]) {
        await asyncForEach(valueIds, async (valueId) => {
            let productsCountWithSameValue = await Product.countDocuments({ values: valueId }).exec()

            if (productsCountWithSameValue == 0) {
                await Value.findByIdAndDelete(valueId).exec()
            }
        })
    }

    private async resizeImageAndMove(file: Express.Multer.File, movePath: string) {
        let fileExtension = path.extname(file.originalname).toLowerCase()

        if (fileExtension == '.jpg' || fileExtension == '.jpeg') {
            await sharp(file.path)
                .resize(1600, 900, {
                    fit: 'inside',
                    background: { r: 0, g: 0, b: 0, alpha: 0 },
                    withoutEnlargement: true
                })
                .jpeg({ quality: 85 })
                .toFile(movePath)
        }

        if (fileExtension == '.png') {
            await sharp(file.path)
                .resize(1600, 900, {
                    fit: 'inside',
                    background: { r: 0, g: 0, b: 0, alpha: 0 },
                    withoutEnlargement: true
                })
                .png({ quality: 85 })
                .toFile(movePath)
        }

        fs.unlinkSync(file.path)
    }

    private processCoverImageFile(product: IProduct, coverImageFile: Express.Multer.File) {
        let staticDirRelativePath = '/static'
        let productDirRelativePath = `/Images/Products/${product.id}`
        let coverImageDirRelativePath = `${productDirRelativePath}/CoverImage`
        let coverImageDirAbsolutPath = `${rootDir}${staticDirRelativePath}${coverImageDirRelativePath}`

        if (!product.coverImagePath) {
            if (fs.existsSync(coverImageDirAbsolutPath)) {
                rimraf.sync(coverImageDirAbsolutPath)
            }
            this.deleteEmptyProductDir(`${rootDir}${staticDirRelativePath}${productDirRelativePath}`)
        }
        if (coverImageFile) {
            if (fs.existsSync(coverImageDirAbsolutPath)) {
                let filesToDelete = fs.readdirSync(coverImageDirAbsolutPath)

                filesToDelete.forEach((file) => {
                    fs.unlinkSync(file)
                })
            }
            fs.mkdirSync(coverImageDirAbsolutPath, { recursive: true })

            let coverImageFileExtension = path.extname(coverImageFile.originalname).toLowerCase()
            let newCoverImageFileName = `${mongoose.Types.ObjectId().toHexString()}${coverImageFileExtension}`

            product.coverImagePath = `${coverImageDirRelativePath}/${newCoverImageFileName}`
            this.resizeImageAndMove(coverImageFile, `${coverImageDirAbsolutPath}/${newCoverImageFileName}`)
        }
    }

    private processImageFiles(product: IProduct, imageFiles: Express.Multer.File[]) {
        let staticDirRelativePath = '/static'
        let productDirRelativePath = `/Images/Products/${product.id}`
        let imagesDirRelativePath = `${productDirRelativePath}/Images`
        let imagesDirAbsolutPath = `${rootDir}${staticDirRelativePath}${imagesDirRelativePath}`

        if (!product.imagePaths.length) {
            if (fs.existsSync(imagesDirAbsolutPath)) {
                rimraf.sync(imagesDirAbsolutPath)
            }
            this.deleteEmptyProductDir(`${rootDir}${staticDirRelativePath}${productDirRelativePath}`)
        } else {
            if (!fs.existsSync(imagesDirAbsolutPath)) {
                fs.mkdirSync(imagesDirAbsolutPath, { recursive: true })
            }

            let serverImageFilePaths = fs.readdirSync(imagesDirAbsolutPath)
            let serverImageFileNames = serverImageFilePaths.map(x => path.parse(x).name)
            let clientImageFileNames = product.imagePaths.filter(x => x).map(x => path.parse(x).name)
            let fileNamesToDelete = serverImageFileNames.filter(x => !clientImageFileNames.includes(x))

            fileNamesToDelete.forEach((fileName) => {
                let fullFilePath = serverImageFilePaths.find(x => x.includes(fileName))

                fs.unlinkSync(`${imagesDirAbsolutPath}/${fullFilePath}`)
            })
        }
        if (imageFiles && imageFiles.length) {
            fs.mkdirSync(imagesDirAbsolutPath, { recursive: true })
            imageFiles.forEach((imageFile) => {
                let imageFileName = imageFile.originalname
                let imageFileExtension = path.extname(imageFileName).toLowerCase()
                let newImageFileName = `${mongoose.Types.ObjectId().toHexString()}${imageFileExtension}`
                let emptyImagePathIndex = product.imagePaths.indexOf('')

                if (emptyImagePathIndex > -1) {
                    product.imagePaths[emptyImagePathIndex] = `${imagesDirRelativePath}/${newImageFileName}`
                } else {
                    product.imagePaths.push(`${imagesDirRelativePath}/${newImageFileName}`)
                }
                this.resizeImageAndMove(imageFile, `${imagesDirAbsolutPath}/${newImageFileName}`)
            })
        }
    }

    private deleteEmptyProductDir(path: string) {
        if (fs.existsSync(path)) {
            let dirList = fs.readdirSync(path)

            if (!dirList.length) {
                rimraf.sync(path)
            }
        }
    }

    @Post('/edit')
    @Use(permitRoles(['Admin']))
    async upsert(@Req() req: Request, @Res() res: Response,
        @MultipartFile('coverImageFile') coverImageFile: Express.Multer.File,
        @MultipartFile('imageFiles') imageFiles: Express.Multer.File[]) {
        if (!req.body.imagePaths) { req.body.imagePaths = [] }
        if (!req.body.videoIds) { req.body.videoIds = [] }
        if (!req.body.price) { req.body.price = '' }
        if (!req.body.discountPrice) { req.body.discountPrice = '' }

        let valueIds: string[] = []

        await asyncForEach(req.body.values, async (value: IValue) => {
            let existingFeature = await Feature.findById(value.feature).exec()

            if (value.id) {
                let existingValue = await Value.findById(value.id).exec()

                if (existingFeature.kind == domain.EFeatureKind.CHECK || existingFeature.kind == domain.EFeatureKind.RADIO) {
                    existingValue.val = value.val
                } else
                if (existingFeature.kind == domain.EFeatureKind.NUMBER) {
                    existingValue.val = Number(value.val)
                }
                await existingValue.save()
                valueIds.push(existingValue.id)
            } else {
                let existingValue = await Value.findOne({ feature: value.feature, val: typeof value.val == 'string' ? value.val.trim() : value.val }).exec()

                if (existingValue) {
                    valueIds.push(existingValue.id)
                } else {
                    let newValue

                    if (existingFeature.kind == domain.EFeatureKind.CHECK || existingFeature.kind == domain.EFeatureKind.RADIO) {
                        newValue = new ValueString(value)
                        await newValue.save()
                        valueIds.push(newValue.id)
                    } else
                    if (existingFeature.kind == domain.EFeatureKind.NUMBER) {
                        newValue = new ValueNumber(value)
                        await newValue.save()
                        valueIds.push(newValue.id)
                    }
                }
            }
        })
        req.body.values = valueIds

        let savedProduct: IProduct

        if (req.body.id) {
            let existingProduct = await Product.findById(req.body.id).exec()
            let existingProductValues = existingProduct.values as mongoose.Types.ObjectId[]
            let existingProductValuesAsStringArray = existingProductValues.map(x => x.toHexString())

            existingProduct = Object.assign(existingProduct, req.body)
            if (req.files && (<any>req.files).coverImageFile && (<any>req.files).coverImageFile.length) {
                this.processCoverImageFile(existingProduct, (<any>req.files).coverImageFile[0])
            } else {
                this.processCoverImageFile(existingProduct, null)
            }
            if (req.files && (<any>req.files).imageFiles && (<any>req.files).imageFiles.length) {
                this.processImageFiles(existingProduct, (<any>req.files).imageFiles)
            } else {
                this.processImageFiles(existingProduct, null)
            }
            savedProduct = await existingProduct.save()

            let savedProductValues = savedProduct.values as mongoose.Types.ObjectId[]
            let savedProductValuesAsStringArray = savedProductValues.map(x => x.toHexString())
            let valueIdsToCheckForDelete = existingProductValuesAsStringArray.filter(x => !savedProductValuesAsStringArray.includes(x))

            this.checkValueIdsUsageAndDeleteUnused(valueIdsToCheckForDelete)
        } else {
            let newProduct = new Product(req.body)

            if (req.files && (<any>req.files).coverImageFile && (<any>req.files).coverImageFile.length) {
                this.processCoverImageFile(newProduct, (<any>req.files).coverImageFile[0])
            } else {
                this.processCoverImageFile(newProduct, null)
            }
            if (req.files && (<any>req.files).imageFiles && (<any>req.files).imageFiles.length) {
                this.processImageFiles(newProduct, (<any>req.files).imageFiles)
            } else {
                this.processImageFiles(newProduct, null)
            }
            savedProduct = await newProduct.save()
        }

        let tierCursor = Tier.find().cursor()

        await tierCursor.eachAsync(async (tier) => {
            let tierProducts = tier.products as mongoose.Types.ObjectId[]
            let tierProductsAsStringArray = tierProducts.map(x => x.toHexString())

            if (req.body.tiers && req.body.tiers.includes(tier.id)) {
                if (!tierProductsAsStringArray.includes(savedProduct.id)) {
                    tier.products = [...tierProductsAsStringArray, savedProduct.id]
                }
            } else {
                if (tierProductsAsStringArray.includes(savedProduct.id)) {
                    _.pull(tierProductsAsStringArray, savedProduct.id)
                    tier.products = tierProductsAsStringArray as any
                }
            }
            await tier.save()
        })

        return res.sendStatus(200)
    }

    @Get('/edit/:id')
    @Use(permitRoles(['Admin']))
    async getForEditById(@Req() req: Request, @Res() res: Response) {
        let model: any = {}
        let existingProduct = await Product.findById(req.params.id).populate('values').exec()
        let tiersWithThisProduct = await Tier.find({ products: req.params.id }).exec()

        model = existingProduct.toJSON()
        model.tiers = []
        tiersWithThisProduct.forEach((tier) => { model.tiers.push(tier.id) })

        return model
    }

    @Delete('/edit/:id')
    @Use(permitRoles(['Admin']))
    async deleteById(@Req() req: Request, @Res() res: Response) {
        /*
        *   Удалить товар.
        *       1. Удалить из тиров удаленный товар.
        *       2. Удалить картинки товара.
        *       3. Проверить каждое значение товара: использует ли
        *          его хоть один другой товар. Если нет, то удалить его.
        */

        let productCursor = Product.findById(req.params.id).cursor()

        await productCursor.eachAsync(async (productToDelete) => {
            productToDelete.remove()

            let productPicturesDirAbsolutPath = `${rootDir}/static/Images/Products/${productToDelete.id}`

            rimraf.sync(productPicturesDirAbsolutPath)

            let tierCursor = Tier.find({ products: productToDelete.id }).cursor()

            tierCursor.eachAsync((tierToModify) => {
                _.remove(<mongoose.Types.ObjectId[]>(tierToModify.products), x => x.toHexString() == productToDelete.id)
                tierToModify.save()
            })

            let productToDeleteValues = productToDelete.values as mongoose.Types.ObjectId[]

            if (productToDeleteValues && productToDeleteValues.length) {
                await this.checkValueIdsUsageAndDeleteUnused(productToDeleteValues.map(x => x.toHexString()))
            }
        })

        return res.sendStatus(200)
    }

    // #endregion

    @Post('/sparesImport')
    @Use(permitRoles(['Admin']))
    async postSparesImport(@Req() req: Request, @Res() res: Response,
        @MultipartFile('sparesFile') sparesFile: Express.Multer.File) {
        await new Promise((resolve, reject) => {
            let fileContent = fs.readFileSync(sparesFile.path)
            let originalEncoding = chardet.detect(fileContent).toString()
            let fileContentAsStringInUTF8 = iconvlite.decode(fileContent, originalEncoding)
            let parsedCSV = csvParse(fileContentAsStringInUTF8, {
                delimiter: ';',
                quote: null
            }, async (err, output: string[][]) => {
                if (err) { reject(err) }

                await asyncForEach(output, async (csvRow) => {
                    if (!csvRow[0].length || !csvRow[1].length || !csvRow[2].length) { return }

                    let csvRowIsCategory = csvRow[4] == 'true'

                    if (csvRowIsCategory) {
                        let existingCategory1C = await Category1C.findOne({ id1C: csvRow[1] }).exec()

                        if (existingCategory1C) {
                            existingCategory1C.name = csvRow[0]
                            existingCategory1C.parentId1C = csvRow[2]
                            await existingCategory1C.save()
                        } else {
                            let newCategory1C = new Category1C({})

                            newCategory1C.name = csvRow[0]
                            newCategory1C.id1C = csvRow[1]
                            newCategory1C.parentId1C = csvRow[2]
                            await newCategory1C.save()
                        }
                    }
                })

                let category1CCursor = Category1C.find({}).cursor()

                await category1CCursor.eachAsync(async (category) => {
                    if (category.parentId1C) {
                        let parentCategory1C = await Category1C.findOne({ id1C: category.parentId1C }).exec()

                        if (parentCategory1C) {
                            category.parentCategory = parentCategory1C.id
                            await category.save()
                        }
                    }
                })

                await asyncForEach(output, async (csvRow) => {
                    if (!csvRow[0].length || !csvRow[1].length || !csvRow[2].length) { return }

                    let csvRowIsProduct = csvRow[4] == 'false'

                    if (csvRowIsProduct) {
                        let existingProduct1C = await Product1C.findOne({ id1C: csvRow[1] }).exec()
                        let productCategory1C = await Category1C.findOne({ id1C: csvRow[2] }).exec()

                        if (existingProduct1C) {
                            if (productCategory1C) {
                                existingProduct1C.name = csvRow[0]
                                existingProduct1C.parentId1C = csvRow[2]
                                existingProduct1C.price = Number.isNaN(Number(csvRow[3])) ? null : Number(csvRow[3])
                                existingProduct1C.category = productCategory1C.id
                                await existingProduct1C.save()
                            }
                        } else {
                            if (productCategory1C) {
                                let newProduct1C = new Product1C({})

                                newProduct1C.name = csvRow[0]
                                newProduct1C.id1C = csvRow[1]
                                newProduct1C.parentId1C = csvRow[2]
                                newProduct1C.price = Number.isNaN(Number(csvRow[3])) ? null : Number(csvRow[3])
                                newProduct1C.category = productCategory1C.id
                                await newProduct1C.save()
                            }
                        }
                    }
                })

                resolve(res.sendStatus(200))
            })
        })
    }

    @Get('/filter')
    async getByFilter(@Req() req: Request, @Res() res: Response) {
        let query: any = {}

        if (req.query.categoryId) {
            let existingCategory = await Category.findById(req.query.categoryId).exec()

            if (!existingCategory) {
                return res.sendStatus(500)
            }

            query.category = mongoose.Types.ObjectId(req.query.categoryId)
        }

        return await Product.find(query).exec()
    }

    @Post('/catalog')
    async postCatalog(@Req() req: Request, @Res() res: Response) {
        let query: any = {
            categoryId: req.body.categoryId
        }

        if (req.body.page && _.isNumber(Number(req.body.page))) {
            query.page = Number(req.body.page)
        }

        let aggregation = await Product
            .aggregate([
                {
                    $facet: {
                        paginatedResults: [
                            { $match: { category: mongoose.Types.ObjectId(query.categoryId) } },
                            { $project:
                                {
                                    _id: false,
                                    id: '$_id',
                                    coverImagePath: true,
                                    name: true,
                                    price: true,
                                    discountPrice: true
                                }
                            },
                            { $skip: query.page ? (query.page - 1) * 12 : 0 },
                            { $limit: 12 }
                        ],
                        totalCount: [
                            { $match: { category: mongoose.Types.ObjectId(query.categoryId) } },
                            { $count: 'count' }
                        ]
                    }
                },
                {
                    $project: {
                        products: '$paginatedResults',
                        total: { '$arrayElemAt': ['$totalCount.count', 0] }
                    }
                }
            ])
            .exec()

        if (aggregation && aggregation.length) {
            return aggregation[0]
        } else {
            return {
                products: [],
                total: null
            }
        }
    }

    @Get('/:id')
    async getById(@Req() req: Request, @Res() res: Response) {
        return await Product.findById(req.params.id).populate('values').exec()
    }
}
