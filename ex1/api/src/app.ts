import dotenv from 'dotenv'

if (process.env.NODE_ENV
    && process.env.NODE_ENV.trim
    && process.env.NODE_ENV.trim() == 'production') {
    dotenv.config({ path: '.env.prod' })
} else {
    dotenv.config({ path: '.env.dev' })
}

import mongoose from 'mongoose'
import meanieMongooseToJson from '@meanie/mongoose-to-json'
import mongooseBeautifulUniqueValidation from 'mongoose-beautiful-unique-validation'

mongoose.plugin(meanieMongooseToJson)
mongoose.plugin(mongooseBeautifulUniqueValidation, {
    defaultMessage: 'Поле `{PATH}` ({VALUE}) не уникально'
})

import { ServerLoader, ServerSettings } from '@tsed/common'
import '@tsed/multipartfiles'
import path from 'path'
import upath from 'upath'
import compression from 'compression'
import cookieParser from 'cookie-parser'
import cookieSession from 'cookie-session'
import bodyParser from 'body-parser'
import lusca from 'lusca'
import passport from 'passport'
import errorhandler from 'errorhandler'
import preFlight from '@/src/middlewares/preFlight'
import notFound from '@/src/middlewares/notFound'
import serverError from '@/src/middlewares/serverError'
import logger from '@/src/utils/logger'
import '@/src/config/passport'

export const rootDir = upath.normalize(path.resolve(__dirname))

@ServerSettings({
    httpPort: `${process.env.HTTP_HOST}` + process.env.HTTP_PORT ? `:${process.env.HTTP_PORT}` : '',
    httpsPort: false,
    rootDir,
    mount: { '/': `${rootDir}/controllers/**/*.(j|t)s` },
    uploadDir: `${rootDir}/upload`,
    statics: { '/': `${rootDir}/static` },
    multer: { //https://github.com/mscdex/busboy/#busboy-methods
        limits: {
            fieldNameSize: 1024,
            fieldSize: 25600,
            fields: Infinity,
            fileSize: Infinity,
            files: Infinity,
            parts: Infinity,
            headerPairs: 2000
        },
        preservePath: false
    }
})
class Server extends ServerLoader {
    $beforeInit() {
        logger.debug('Initializing server...')
        this.expressApp.disable('x-powered-by')
        mongoose
            .connect(
                process.env.MONGODB_URI,
                {
                    useCreateIndex: true,
                    useUnifiedTopology: true,
                    useNewUrlParser: true
                }
            )
            .then(() => {
                /* The `mongoose.connect()` promise resolves to undefined. */
                logger.debug('MongoDB connected...')
            })
            .catch((err) => {
                logger.error('MongoDB connection error. Make sure MongoDB is running. ' + err)
                process.exit()
            })
    }
    $beforeRoutesInit() {
        logger.debug('Mounting middlewares...')

        this
            .use(compression())
            .use(preFlight)
            .use(cookieParser())
            .use(bodyParser.json())
            .use(bodyParser.urlencoded({ extended: true }))
            /*
            * Либо по старому с точкой в начале ".example.com"
            * Либо по новому без точки в начале "example.com"
            * Либо ОБЯЗАТЕЛЬНО БЕЗ ТОЧКИ, если это "localhost"
            */
            .use(cookieSession({
                name: 's',
                domain: process.env.COOKIE_DOMAIN,
                secret: process.env.SESSION_SECRET,
                maxAge: Number(process.env.SESSION_MAX_AGE_MILLISECONDS)
            }))
            .use(passport.initialize())
            .use(passport.session())
            .use(lusca({
                xframe: 'SAMEORIGIN',
                xssProtection: true
            }))
            /**
             * Error Handler. Provides full stack - remove for production
             */
            /**
             * TODO:
             * Запилить тернарник на process.env.NODE_ENV === 'production' и "dummy middleware"
             * Либо забить и удалять стактрейс в последней мидлваре
             */
            .use(errorhandler())
    }
    $afterRoutesInit() {
        logger.debug('Routes has been initialized...')
        this.use(notFound)
            .use(serverError)
    }
    $onReady() {
        logger.debug('Server is ready...')
    }
    $onServerInitError(err: any){
        logger.error('ServerInitError:', err)
    }
}

new Server().start()
