import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App.vue'

import '~/common/fullscreenPolyfill'
import '~/plugins'
import '~/components'

Vue.config.productionTip = false

const root = new App({
  el: '#app',
  i18n,
  store,
  router
})

window['ROOT'] = root
