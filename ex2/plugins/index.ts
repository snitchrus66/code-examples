import './axios'
import './fontawesome'
import './bootstrapvue'
import './inputmask'
import './vuelayers'
import './vuetouchevents'
import './ismobile'
import './clipboard'
