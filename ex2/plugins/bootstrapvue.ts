import Vue from 'vue'
import { BootstrapVue/*, IconsPlugin*/ } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue, {
  BFormFile: {
    browseText: 'Обзор',
    placeholder: 'Файл не выбран',
    dropPlaceholder: 'Перетащите файлы сюда'
  },
  BCalendar: {
    labelPrevDecade: 'Предыдущая декада',
    labelPrevYear: 'Предыдущий год',
    labelPrevMonth: 'Предыдущий месяц',
    labelCurrentMonth: 'Текущий месяц',
    labelNextMonth: 'Следущий месяц',
    labelNextYear: 'Следующий год',
    labelNextDecade: 'Следующая декада',
    labelToday: 'Сегодня',
    labelSelected: 'Выбранная дата',
    labelNoDateSelected: 'Дата не выбрана',
    labelCalendar: 'Календарь',
    labelNav: 'Навигация по календарю',
    labelHelp: 'Используйте кнопки со стрелками для навигации по датам'
  },
  BFormDatepicker: {
    labelPrevDecade: null,
    labelPrevYear: null,
    labelPrevMonth: null,
    labelCurrentMonth: null,
    labelNextMonth: null,
    labelNextYear: null,
    labelNextDecade: null,
    labelToday: null,
    labelSelected: null,
    labelNoDateSelected: null,
    labelCalendar: null,
    labelNav: null,
    labelHelp: null,
    labelTodayButton: 'Выбрать сегодня',
    labelResetButton: 'Сбросить',
    labelCloseButton: 'Закрыть'
  }
})
// Optionally install the BootstrapVue icon components plugin
// Vue.use(IconsPlugin)
