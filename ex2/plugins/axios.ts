import axios from 'axios'
import { useStore } from 'vuex-simple'
import vuexStore, { RootModule } from '~/store'
import router from '~/router'

const store = useStore<RootModule>(vuexStore)

// Request interceptor
axios.interceptors.request.use((request) => {
  const token = store.auth.token

  if (token) {
    request.headers.common['Authorization'] = `Bearer ${token}`
  }

  return request
})

// Response interceptor
axios.interceptors.response.use(
  // Any status code that lie within the range of 2xx cause this function to trigger
  (response) => { return response },
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  (error) => {
    const { status } = error.response

    /**
     * No need to handle:
     * 1. Validation error - status 422.
     * 2. Session expiration error when auth already droped in browser - status 401 with !store.auth.check.
     */
    if (/*status === 422 || */(status === 401 && !store.auth.check)) {
      return Promise.reject(error)
    }

    let title = 'Ошибка'
    let message = 'Произошла неизвестная ошибка.'
    let variant = 'danger'
    let callback: (() => void) | null = null

    if (status >= 500) {
      title = 'Упс...'
      message = 'Что-то пошло не так! Пожалуйста, попробуйте ещё раз.'
      variant = 'danger'
    } else
    if (status === 401 && store.auth.check) {
      title = 'Сессия истекла!'
      message = 'Пожалуйста, войдите снова, чтобы продолжить.'
      variant = 'warning'
      callback = () => {
        store.auth.logout({ withoutRequest: true })
        router.push({ name: 'login' })
      }
    } else
    if (error.response.data.message) {
      message = error.response.data.message
    } else
    if (error.response.data.errors) {
      if (Array.isArray(error.response.data.errors)) {
        message = ''
        error.response.data.errors.forEach((text: string, index: number) => {
          message += `\r\n\r\n${text}`
          if (index === 0) { message.replace('\r\n\r\n', '') }
        })
      } else {
        message = ''
        Object.keys(error.response.data.errors).forEach((errorKey: string, index) => {
          message += `\r\n\r\n${error.response.data.errors[errorKey]}`
          if (index === 0) { message.replace('\r\n\r\n', '') }
        })
      }
    }

    // window.ROOT.$bvModal.msgBoxOk(
    //   message,
    //   {
    //     title,
    //     okVariant: variant,
    //     centered: true
    //   }
    // ).then(callback)

    window.ROOT.$bvToast.toast(
      message,
      {
        title,
        variant,
        solid: true

      }
    )
    if (callback) {
      callback()
    }

    return Promise.reject(error)
  })
