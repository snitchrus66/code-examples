import Vue from 'vue'
import Child from './Child.vue'
import { AlertError, AlertSuccess, HasError } from 'vform'
import Autocomplete from './Autocomplete.vue'
import Gallery from './Gallery.vue'
import Tree from './Tree.vue'
import TreeItem from './TreeItem.vue'
import CheckBoxDropdownList from './CheckBoxDropdownList.vue'
import CheckBoxFiltered from './CheckBoxFiltered.vue'

// Components that are registered globaly.
Vue.component('child', Child)
Vue.component('alert-error', AlertError)
Vue.component('alert-success', AlertSuccess)
Vue.component('has-error', HasError)
Vue.component('odtn-gallery', Gallery)
Vue.component('odtn-autocomplete', Autocomplete)
Vue.component('odtn-tree', Tree)
Vue.component('odtn-tree-item', TreeItem)
Vue.component('odtn-checkbox-dropdown', CheckBoxDropdownList)
Vue.component('odtn-filtered-checkbox', CheckBoxFiltered)
