import { debounce } from 'lodash-es'

type DebounceParameters = Parameters<typeof debounce>

export default function (milliseconds: number = 0, options: DebounceParameters[2] = {}) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const map = new WeakMap()
    const originalMethod = descriptor.value

    descriptor.value = function (...params: any) {
      let debounced = map.get(this)

      if (!debounced) {
        debounced = debounce(originalMethod, milliseconds, options).bind(this)
        map.set(this, debounced)
      }
      debounced(...params)
    }

    return descriptor
  }
}
