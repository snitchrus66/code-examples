import { throttle } from 'lodash-es'

type ThrottleParameters = Parameters<typeof throttle>

export default function (milliseconds: number = 0, options: ThrottleParameters[2] = {}) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const map = new WeakMap()
    const originalMethod = descriptor.value

    descriptor.value = function (...params: any) {
      let throttled = map.get(this)

      if (!throttled) {
        throttled = throttle(originalMethod, milliseconds, options).bind(this)
        map.set(this, throttled)
      }
      throttled(...params)
    }

    return descriptor
  }
}
