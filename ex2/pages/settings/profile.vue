<template>
  <b-card class="mb-4">
    <template v-slot:header>
      Ваш профиль
    </template>
    <form
      @submit.prevent="update"
      @keydown="userForm.onKeydown($event)"
    >
      <alert-success
        :form="userForm"
        :message="$t('info_updated')"
      />
      <div class="form-group row text-center">
        <div class="col">
          <img
            :src="user.photo_url.width_512"
            class="rounded-circle profile-photo mr-1"
            style="max-width: 200px; max-height: 200px;"
          >
        </div>
      </div>

      <!-- avatar -->
      <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right">
          Аватар
        </label>
        <div class="col-md-7">
          <b-form-file
            :state="avatarForm.errors.has('avatar') ? false : null"
            name="avatar"
            @change="updateAvatar"
          />
          <has-error
            :form="avatarForm"
            field="avatar"
          />
        </div>
      </div>

      <!-- Last name -->
      <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right">{{ $t('last_name') }}</label>
        <div class="col-md-7">
          <b-form-input
            v-model="userForm.last_name"
            :state="userForm.errors.has('last_name') ? false : null"
            name="last_name"
          />
          <has-error
            :form="userForm"
            field="last_name"
          />
        </div>
      </div>

      <!-- First name -->
      <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right">{{ $t('first_name') }}</label>
        <div class="col-md-7">
          <b-form-input
            v-model="userForm.first_name"
            :state="userForm.errors.has('first_name') ? false : null"
            name="first_name"
          />
          <has-error
            :form="userForm"
            field="first_name"
          />
        </div>
      </div>

      <!-- Middle name -->
      <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right">{{ $t('middle_name') }}</label>
        <div class="col-md-7">
          <b-form-input
            v-model="userForm.middle_name"
            :state="userForm.errors.has('middle_name') ? false : null"
            name="middle_name"
          />
          <has-error
            :form="userForm"
            field="middle_name"
          />
        </div>
      </div>

      <!-- Email -->
      <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right">{{ $t('email') }}</label>
        <div class="col-md-7">
          <b-form-input
            v-model="userForm.email"
            :state="userForm.errors.has('email') ? false : null"
            name="email"
            type="email"
          />
          <has-error
            :form="userForm"
            field="email"
          />
        </div>
      </div>

      <!-- Submit Button -->
      <div class="form-group row">
        <div class="col-md-9 ml-md-auto">
          <b-button
            :class="[{'btn-loading': userForm.busy}]"
            variant="success"
            type="submit"
          >
            {{ $t('update') }}
          </b-button>
        </div>
      </div>
    </form>
  </b-card>
</template>

<script lang="ts">
import { Vue, Component } from 'vue-property-decorator'
import Form from 'vform'
import { useStore } from 'vuex-simple'
import { RootModule } from '~/store'
import { serialize as objectToFormData } from 'object-to-formdata'

@Component({
  middleware: 'auth',
  metaInfo() {
    return { title: this.$t('settings') }
  }
})
export default class Profile extends Vue {
  store = useStore<RootModule>(this.$store)
  avatarForm = new Form({
    avatar: ''
  })
  userForm = new Form({
    last_name: '',
    first_name: '',
    middle_name: '',
    email: ''
  })

  get user() {
    return this.store.auth.user
  }

  created() {
    this.getUser()
  }

  async update() {
    const { data } = await this.userForm.patch('/api/v1/settings/profile')

    this.store.auth.updateUser({ user: data })
  }

  async updateAvatar(event: Event) {
    const files = (event.target as HTMLInputElement).files

    if (files?.length) {
      // @ts-ignore
      this.avatarForm.avatar = files[0]

      const response = await this.avatarForm.post('/api/v1/settings/avatar', { transformRequest: [(data: any) => objectToFormData(data)] })

      // @ts-ignore
      if (response.status === 201) {
        this.getUser()
      }
    }
  }

  async getUser() {
    await this.store.auth.fetchUser()
    // @ts-ignore
    this.userForm.keys().forEach(key => {
      // @ts-ignore
      this.userForm[key] = this.user[key]
    })
  }
}
</script>
