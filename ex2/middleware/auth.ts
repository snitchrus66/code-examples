import { Route, NavigationGuardNext } from 'vue-router'
import { useStore } from 'vuex-simple'
import vuexStore, { RootModule } from '~/store'

export default async (to: Route, from: Route, next: NavigationGuardNext) => {
  const store = useStore<RootModule>(vuexStore)

  if (!store.auth.check) {
    next({ name: 'login' })
  } else {
    next()
  }
}
