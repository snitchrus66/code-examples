import { Route, NavigationGuardNext } from 'vue-router'
import { useStore } from 'vuex-simple'
import vuexStore, { RootModule } from '~/store'
import { loadMessages } from '~/plugins/i18n'

export default async (to: Route, from: Route, next: NavigationGuardNext) => {
  const store = useStore<RootModule>(vuexStore)

  await loadMessages(store.lang.locale)
  next()
}
