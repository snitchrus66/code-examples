import { Route, NavigationGuardNext } from 'vue-router'
import { useStore } from 'vuex-simple'
import vuexStore, { RootModule } from '~/store'

export default (to: Route, from: Route, next: NavigationGuardNext) => {
  const store = useStore<RootModule>(vuexStore)

  if (store.auth.user && !store.auth.user.roles.includes('admin')) {
    next({ name: 'home' })
  } else {
    next()
  }
}
