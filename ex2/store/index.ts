import Vue from 'vue'
import Vuex from 'vuex'
import { Module, State, Getter, Mutation, createVuexStore, Action } from 'vuex-simple'
import AuthModule from '~/store/modules/auth'
import ContactsModule from '~/store/modules/contacts'
import DbObjectTypesModule from '~/store/modules/dbObjectTypes'
import FilesModule from '~/store/modules/files'
import LangModule from '~/store/modules/lang'
import MessengerModule from '~/store/modules/messenger'
import RolesModule from '~/store/modules/roles'
import UsersModule from '~/store/modules/users'
import MapModule from '~/store/modules/map'
import CounterpartiesModule from '~/store/modules/counterparties'
import DepartmentsModule from '~/store/modules/departments'
import EmployeePositionsModule from '~/store/modules/employeePositions'
import ProcessingModulesModule from '~/store/modules/processingModules'
import StaffModule from '~/store/modules/staff'
import MetricControllersModule from '~/store/modules/metricControllers'
import AllVidecamsModule from '~/store/modules/cams'
import S3EndpointsModule from '~/store/modules/s3Endpoints'
import * as client from '~/typings/client'

Vue.use(Vuex)

export class RootModule {
  @Module()
  auth = new AuthModule()

  @Module()
  contacts = new ContactsModule()

  @Module()
  dbObjectTypes = new DbObjectTypesModule()

  @Module()
  files = new FilesModule()

  @Module()
  lang = new LangModule()

  @Module()
  messenger = new MessengerModule()

  @Module()
  roles = new RolesModule()

  @Module()
  users = new UsersModule()

  @Module()
  map = new MapModule()

  @Module()
  counterparties = new CounterpartiesModule()

  @Module()
  departments = new DepartmentsModule()

  @Module()
  employeePositions = new EmployeePositionsModule()

  @Module()
  staff = new StaffModule()

  @Module()
  metricControllers = new MetricControllersModule()

  @Module()
  videcams = new AllVidecamsModule()

  @Module()
  processingModules = new ProcessingModulesModule()

  @Module()
  s3Endpoints = new S3EndpointsModule()

  @State()
  navbarIsVisible = true

  @Getter()
  get mainMenu() {
    const menu: client.MainMenuItem[] = []

    if (this.auth.user?.can) {
      if (this.auth.user.can['read map objects']) {
        menu.push({
          icon: 'map-marked-alt',
          name: 'Карта',
          route: 'map'
        })
        menu.push({
          icon: 'map-marker-alt',
          name: 'Наши объекты',
          route: 'map-objects'
        })
      }
      if (this.auth.user.can['read user medialib items']) {
        menu.push({
          icon: 'folder',
          name: 'Файлы',
          route: 'settings.documents'
        })
      }
      if (this.auth.user.can['read counterparties']) {
        menu.push({
          icon: 'building',
          name: 'Контрагенты',
          route: 'counterparties'
        })
      }
      if (this.auth.user.can['read users']) {
        menu.push({
          icon: 'users',
          name: 'Пользователи',
          route: 'users'
        })
      }
      if (this.auth.user.can['read roles']) {
        menu.push({
          icon: 'users-cog',
          name: 'Роли',
          route: 'roles'
        })
      }
      if (this.auth.user.can['read staff schedules']) {
        menu.push({
          icon: ['fab', 'black-tie'],
          name: 'Штатные расписания',
          route: 'staff-schedules'
        })
      }
    }

    return menu
  }

  @Getter()
  get directoryMenu() {
    const menuGroups: client.MainMenuItemsGroup[] = []
    let mainMenuGroup: client.MainMenuItem[] = []

    if (this.auth.user?.can) {
      if (this.auth.user.can['read map object types']) {
        mainMenuGroup.push({
          icon: 'map',
          name: 'Типы объектов',
          route: 'map-object-types'
        })
      }
      if (this.auth.user.can['read contact types']) {
        mainMenuGroup.push({
          icon: 'address-book',
          name: 'Типы контактов',
          route: 'contact-types'
        })
      }
      if (this.auth.user.can['read departments']) {
        mainMenuGroup.push({
          icon: 'sitemap',
          name: 'Структурные подразделения',
          route: 'departments'
        })
      }
      if (this.auth.user.can['read employee positions']) {
        mainMenuGroup.push({
          icon: 'user-tie',
          name: 'Должности',
          route: 'employee-positions'
        })
      }
      if (this.auth.user.can['read s3 endpoints']) {
        mainMenuGroup.push({
          icon: 'hdd',
          name: 'Эндпоинты S3',
          route: 's3-endpoints'
        })
      }
      if (this.auth.user.can['read db object type']) {
        mainMenuGroup.push({
          icon: 'hdd',
          name: 'Типы объектов БД',
          route: 'db-object-types'
        })
      }

      if (mainMenuGroup.length) {
        menuGroups.push({
          icon: '',
          name: 'Структуры',
          items: [...mainMenuGroup]
        })
      }

      mainMenuGroup = []

      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'hdd',
          name: 'Модели контроллеров',
          route: 'metrics.metric-controller-models'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'hdd',
          name: 'Контроллеры метрик',
          route: 'metrics.metric-controllers'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'hdd',
          name: 'Типы сетей',
          route: 'metrics.network-types'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'hdd',
          name: 'Сети контроллеров',
          route: 'metrics.controller-networks'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'hdd',
          name: 'Модели модулей контроллеров',
          route: 'metrics.rs485-device-models'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'hdd',
          name: 'Модули контроллеров',
          route: 'metrics.rs485-devices'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'cubes',
          name: 'Типы параметров',
          route: 'metrics.unit-measurements'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'cubes',
          name: 'TS Серверы БД',
          route: 'metrics.ts-db-servers'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'cubes',
          name: 'TS Базы данных',
          route: 'metrics.ts-databases'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'cubes',
          name: 'Каналы датчиков',
          route: 'metrics.metric-sensor-channels'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'file-alt',
          name: 'Протоколы передачи данных',
          route: 'metrics.data-transfer-protocols'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'fax',
          name: 'Модели датчиков',
          route: 'metrics.metric-sensor-models'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'tachometer-alt',
          name: 'Датчики',
          route: 'metrics.metric-sensors'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'tachometer-alt',
          name: 'Mqtt-серверы',
          route: 'metrics.mqtt-servers'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'tachometer-alt',
          name: 'Графики',
          route: 'metrics.graphs'
        })
      }
      if (this.auth.user.can['read metric controller']) {
        mainMenuGroup.push({
          icon: 'tachometer-alt',
          name: 'Графики канала датчиков',
          route: 'metrics.sensor-channel-graphs'
        })
      }

      if (mainMenuGroup.length) {
        menuGroups.push({
          icon: '',
          name: 'Метрики',
          items: [...mainMenuGroup]
        })
      }

      mainMenuGroup = []

      if (this.auth.user.can['read videcams']) {
        mainMenuGroup.push({
          icon: 'fax',
          name: 'Видеокамеры',
          route: 'videcams.videcams'
        })
      }
      if (this.auth.user.can['read videcams']) {
        mainMenuGroup.push({
          icon: 'tachometer-alt',
          name: 'Модели видеокамер',
          route: 'videcams.videcam-models'
        })
      }

      if (mainMenuGroup.length) {
        menuGroups.push({
          icon: '',
          name: 'Видеокамеры',
          items: [...mainMenuGroup]
        })
      }

      mainMenuGroup = []

      if (this.auth.user.can['read messenger']) {
        mainMenuGroup.push({
          icon: 'fax',
          name: 'Сообщения',
          route: 'messenger.messages'
        })
      }
      if (this.auth.user.can['read messenger']) {
        mainMenuGroup.push({
          icon: 'tachometer-alt',
          name: 'Ветки сообщений',
          route: 'messenger.message-threads'
        })
      }
      if (this.auth.user.can['read messenger']) {
        mainMenuGroup.push({
          icon: 'tachometer-alt',
          name: 'Вложения',
          route: 'messenger.attachments'
        })
      }
      if (this.auth.user.can['read messenger']) {
        mainMenuGroup.push({
          icon: 'tachometer-alt',
          name: 'Связка типов об. БД и веток',
          route: 'messenger.db-object-types-message-threads'
        })
      }

      if (mainMenuGroup.length) {
        menuGroups.push({
          icon: '',
          name: 'Мессенджер',
          items: [...mainMenuGroup]
        })
      }

      mainMenuGroup = []

      if (this.auth.user.can['read processing module']) {
        mainMenuGroup.push({
          icon: 'fax',
          name: 'Модули',
          route: 'processing-modules.modules'
        })
      }
      if (this.auth.user.can['read processing module']) {
        mainMenuGroup.push({
          icon: 'list',
          name: 'Модули - Объекты БД',
          route: 'processing-modules.db-object-type-modules'
        })
      }

      if (mainMenuGroup.length) {
        menuGroups.push({
          icon: '',
          name: 'Модули обработки',
          items: [...mainMenuGroup]
        })
      }

      mainMenuGroup = []

      if (this.auth.user.can['read medialib-file-extensions']) {
        mainMenuGroup.push({
          icon: 'list',
          name: 'Расширения файлов',
          route: 'medialib-file-extensions'
        })
      }
      if (this.auth.user.can['read medialib-file-types']) {
        mainMenuGroup.push({
          icon: 'list',
          name: 'Типы файлов',
          route: 'medialib-file-types'
        })
      }

      if (mainMenuGroup.length) {
        menuGroups.push({
          icon: '',
          name: 'Файлы',
          items: [...mainMenuGroup]
        })
      }

    }

    return menuGroups

  }

  @Mutation()
  TOGGLE_NAVBAR() {
    this.navbarIsVisible = !this.navbarIsVisible
  }

  @Action()
  toggleNavbar() {
    this.TOGGLE_NAVBAR()
  }
}

export default createVuexStore(new RootModule(), { strict: true })
