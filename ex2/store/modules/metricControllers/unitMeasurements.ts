
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class UnitMeasurementsModule {
  @State()
  unitMeasurements: api.Controllers.UnitMeasurements.UnitMeasurementsListResponse | null = null

  @State()
  unitMeasurement: api.Controllers.UnitMeasurements.UnitMeasurementReadResponse | null = null

  @Mutation()
  SAVE_PARAM_TYPES(payload: api.Controllers.UnitMeasurements.UnitMeasurementsListResponse) {
    this.unitMeasurements = payload
  }

  @Mutation()
  SAVE_PARAM_TYPE(payload: api.Controllers.UnitMeasurements.UnitMeasurementReadResponse) {
    this.unitMeasurement = payload
  }

  @Action()
  async fetchUnitMeasurements(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.UnitMeasurements.UnitMeasurementsListResponse>(`/api/v1/metrics/unit-measurements`, { params: payload })

    this.SAVE_PARAM_TYPES(response.data)

    return response
  }

  @Action()
  async fetchUnitMeasurement(payload: api.Controllers.UnitMeasurements.UnitMeasurementReadRequest) {
    const response = await axios.get<api.Controllers.UnitMeasurements.UnitMeasurementReadResponse>(`/api/v1/metrics/unit-measurements/${payload.id}`)

    this.SAVE_PARAM_TYPE(response.data)

    return response
  }

  @Action()
  async deleteUnitMeasurement(payload: api.Controllers.UnitMeasurements.UnitMeasurementDeleteRequest) {
    const response = await axios.delete<api.Controllers.UnitMeasurements.UnitMeasurementDeleteResponse>(`/api/v1/metrics/unit-measurements/${payload.id}`)

    return response
  }

}
