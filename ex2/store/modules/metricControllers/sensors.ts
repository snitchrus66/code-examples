
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class SensorsModule {
  @State()
  sensors: api.Controllers.Sensors.SensorsListResponse | null = null

  @State()
  sensor: api.Controllers.Sensors.SensorReadResponse | null = null

  @Mutation()
  SAVE_SENSORS(payload: api.Controllers.Sensors.SensorsListResponse) {
    this.sensors = payload
  }

  @Mutation()
  SAVE_SENSOR(payload: api.Controllers.Sensors.SensorReadResponse) {
    this.sensor = payload
  }

  @Action()
  async fetchSensors(payload?: api.ListOptions) {
    const response = await axios.get<api.Controllers.Sensors.SensorsListResponse>(`/api/v1/metrics/sensors`, { params: payload })

    this.SAVE_SENSORS(response.data)

    return response
  }

  @Action()
  async fetchSensor(payload: api.Controllers.Sensors.SensorReadRequest) {
    const response = await axios.get<api.Controllers.Sensors.SensorReadResponse>(`/api/v1/metrics/sensors/${payload.id}`)

    this.SAVE_SENSOR(response.data)

    return response
  }

  @Action()
  async deleteSensor(payload: api.Controllers.Sensors.SensorDeleteRequest) {
    const response = await axios.delete<api.Controllers.Sensors.SensorDeleteResponse>(`/api/v1/metrics/sensors/${payload.id}`)

    return response
  }

}
