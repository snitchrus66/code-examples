import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class GraphsModule {
  @State()
  graphs: any

  @State()
  graph: any

  @Mutation()
  SAVE_GRAPHS(payload: any) {
    this.graphs = payload
  }

  @Mutation()
  SAVE_SENSOR_CHANNEL_GRAPH(payload: any) {
    this.graph = payload
  }

  @Action()
  async fetchGraphs(payload: api.ListOptions = {}) {
    const response = await axios.get(`/api/v1/metrics/graffics`, { params: payload })

    this.SAVE_GRAPHS(response.data)

    return response
  }

  @Action()
  async fetchGraph(payload: any) {
    const response = await axios.get(`/api/v1/metrics/graffics/${payload.id}`)

    this.SAVE_SENSOR_CHANNEL_GRAPH(response.data)

    return response
  }

  @Action()
  async deleteGraph(payload: any) {
    const response = await axios.delete(`/api/v1/metrics/graffics/${payload.id}`)

    return response
  }

}
