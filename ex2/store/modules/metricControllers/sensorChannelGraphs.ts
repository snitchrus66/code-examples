import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class SensorChannelGraphsModule {
  @State()
  sensorChannelGraphs: any

  @State()
  sensorChannelGraph: any

  @Mutation()
  SAVE_SENSOR_CHANNEL_GRAPHS(payload: any) {
    this.sensorChannelGraphs = payload
  }

  @Mutation()
  SAVE_SENSOR_CHANNEL_GRAPH(payload: any) {
    this.sensorChannelGraph = payload
  }

  @Action()
  async fetchSensorChannelGraphs(payload: api.ListOptions = {}) {
    const response = await axios.get(`/api/v1/metrics/sensor-channel-graffics`, { params: payload })

    this.SAVE_SENSOR_CHANNEL_GRAPHS(response.data)

    return response
  }

  @Action()
  async fetchSensorChannelGraph(payload: any) {
    const response = await axios.get(`/api/v1/metrics/sensor-channel-graffics/${payload.id}`)

    this.SAVE_SENSOR_CHANNEL_GRAPH(response.data)

    return response
  }

  @Action()
  async deleteSensorChannelGraph(payload: any) {
    const response = await axios.delete(`/api/v1/metrics/sensor-channel-graffics/${payload.id}`)

    return response
  }

}
