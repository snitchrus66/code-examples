
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class SensorChannelsModule {
  @State()
  sensorChannels: api.Controllers.SensorChannels.SensorChannelsListResponse | null = null

  @State()
  sensorChannel: api.Controllers.SensorChannels.SensorChannelReadResponse | null = null

  @Mutation()
  SAVE_SENSOR_CHANNELS(payload: api.Controllers.SensorChannels.SensorChannelsListResponse) {
    this.sensorChannels = payload
  }

  @Mutation()
  SAVE_SENSOR_CHANNEL(payload: api.Controllers.SensorChannels.SensorChannelReadResponse) {
    this.sensorChannel = payload
  }

  @Action()
  async fetchSensorChannels(payload: api.Controllers.SensorChannels.SensorChannelsListRequest = {}) {
    const response = await axios.get<api.Controllers.SensorChannels.SensorChannelsListResponse>(`/api/v1/metrics/sensor-channels`, { params: payload })

    this.SAVE_SENSOR_CHANNELS(response.data)

    return response
  }

  @Action()
  async fetchSensorChannel(payload: api.Controllers.SensorChannels.SensorChannelReadRequest) {
    const response = await axios.get<api.Controllers.SensorChannels.SensorChannelReadResponse>(`/api/v1/metrics/sensor-channels/${payload.id}`)

    this.SAVE_SENSOR_CHANNEL(response.data)

    return response
  }

  @Action()
  async deleteSensorChannel(payload: api.Controllers.SensorChannels.SensorChannelDeleteRequest) {
    const response = await axios.delete<api.Controllers.SensorChannels.SensorChannelDeleteResponse>(`/api/v1/metrics/sensor-channels/${payload.id}`)

    return response
  }

}
