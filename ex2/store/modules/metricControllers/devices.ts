
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class DevicesModule {
  @State()
  devices: api.Controllers.Devices.DevicesListResponse | null = null

  @State()
  device: api.Controllers.Devices.DeviceReadResponse | null = null

  @Mutation()
  SAVE_DEVICES(payload: api.Controllers.Devices.DevicesListResponse) {
    this.devices = payload
  }

  @Mutation()
  SAVE_DEVICE(payload: api.Controllers.Devices.DeviceReadResponse) {
    this.device = payload
  }

  @Action()
  async fetchDevices(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.Devices.DevicesListResponse>(`/api/v1/metrics/rs485-devices`, { params: payload })

    this.SAVE_DEVICES(response.data)

    return response
  }

  @Action()
  async fetchDevice(payload: api.Controllers.Devices.DeviceReadRequest) {
    const response = await axios.get<api.Controllers.Devices.DeviceReadResponse>(`/api/v1/metrics/rs485-devices/${payload.id}`)

    this.SAVE_DEVICE(response.data)

    return response
  }

  @Action()
  async deleteDevice(payload: api.Controllers.Devices.DeviceDeleteRequest) {
    const response = await axios.delete<api.Controllers.Devices.DeviceDeleteResponse>(`/api/v1/metrics/rs485-devices/${payload.id}`)

    return response
  }

}
