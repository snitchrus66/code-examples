
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class TsDatabasesModule {
  @State()
  tsDatabases: api.Controllers.TsDatabases.TsDatabasesListResponse | null = null

  @State()
  tsDatabase: api.Controllers.TsDatabases.TsDatabaseReadResponse | null = null

  @Mutation()
  SAVE_TS_DATABASES(payload: api.Controllers.TsDatabases.TsDatabasesListResponse) {
    this.tsDatabases = payload
  }

  @Mutation()
  SAVE_TS_DATABASE(payload: api.Controllers.TsDatabases.TsDatabaseReadResponse) {
    this.tsDatabase = payload
  }

  @Action()
  async fetchTsDatabases(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.TsDatabases.TsDatabasesListResponse>(`/api/v1/metrics/ts-databases`, { params: payload })

    this.SAVE_TS_DATABASES(response.data)

    return response
  }

  @Action()
  async fetchTsDatabase(payload: api.Controllers.TsDatabases.TsDatabaseReadRequest) {
    const response = await axios.get<api.Controllers.TsDatabases.TsDatabaseReadResponse>(`/api/v1/metrics/ts-databases/${payload.id}`)

    this.SAVE_TS_DATABASE(response.data)

    return response
  }

  @Action()
  async deleteTsDatabase(payload: api.Controllers.TsDatabases.TsDatabaseDeleteRequest) {
    const response = await axios.delete<api.Controllers.TsDatabases.TsDatabaseDeleteResponse>(`/api/v1/metrics/ts-databases/${payload.id}`)

    return response
  }

}
