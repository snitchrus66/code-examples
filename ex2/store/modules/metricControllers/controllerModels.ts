
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class ControllerModelsModule {
  @State()
  controllerModels: api.Controllers.ControllerModels.ControllerModelsListResponse | null = null

  @State()
  controllerModel: api.Controllers.ControllerModels.ControllerModelReadResponse | null = null

  @Mutation()
  SAVE_CONTROLLER_MODELS(payload: api.Controllers.ControllerModels.ControllerModelsListResponse) {
    this.controllerModels = payload
  }

  @Mutation()
  SAVE_CONTROLLER_MODEL(payload: api.Controllers.ControllerModels.ControllerModelReadResponse) {
    this.controllerModel = payload
  }

  @Action()
  async fetchControllerModels(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.ControllerModels.ControllerModelsListResponse>(`/api/v1/metrics/controller-models`, { params: payload })

    this.SAVE_CONTROLLER_MODELS(response.data)

    return response
  }

  @Action()
  async fetchControllerModel(payload: api.Controllers.ControllerModels.ControllerModelReadRequest) {
    const response = await axios.get<api.Controllers.ControllerModels.ControllerModelReadResponse>(`/api/v1/metrics/controller-models/${payload.id}`)

    this.SAVE_CONTROLLER_MODEL(response.data)

    return response
  }

  @Action()
  async deleteControllerModel(payload: api.Controllers.ControllerModels.ControllerModelDeleteRequest) {
    const response = await axios.delete<api.Controllers.ControllerModels.ControllerModelDeleteResponse>(`/api/v1/metrics/controller-models/${payload.id}`)

    return response
  }

}
