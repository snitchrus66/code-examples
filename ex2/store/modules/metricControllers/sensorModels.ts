
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class SensorModelsModule {
  @State()
  sensorModels: api.Controllers.SensorModels.SensorModelsListResponse | null = null

  @State()
  sensorModel: api.Controllers.SensorModels.SensorModelReadResponse | null = null

  @Mutation()
  SAVE_SENSOR_MODELS(payload: api.Controllers.SensorModels.SensorModelsListResponse) {
    this.sensorModels = payload
  }

  @Mutation()
  SAVE_SENSOR_MODEL(payload: api.Controllers.SensorModels.SensorModelReadResponse) {
    this.sensorModel = payload
  }

  @Action()
  async fetchSensorModels(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.SensorModels.SensorModelsListResponse>(`/api/v1/metrics/sensor-models`, { params: payload })

    this.SAVE_SENSOR_MODELS(response.data)

    return response
  }

  @Action()
  async fetchSensorModel(payload: api.Controllers.SensorModels.SensorModelReadRequest) {
    const response = await axios.get<api.Controllers.SensorModels.SensorModelReadResponse>(`/api/v1/metrics/sensor-models/${payload.id}`)

    this.SAVE_SENSOR_MODEL(response.data)

    return response
  }

  @Action()
  async deleteSensorModel(payload: api.Controllers.SensorModels.SensorModelDeleteRequest) {
    const response = await axios.delete<api.Controllers.SensorModels.SensorModelDeleteResponse>(`/api/v1/metrics/sensor-models/${payload.id}`)

    return response
  }

}
