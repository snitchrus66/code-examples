
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class NetworkTypesModule {
  @State()
  networkTypes: api.Controllers.NetworkTypes.NetworkTypesListResponse | null = null

  @State()
  networkType: api.Controllers.NetworkTypes.NetworkTypeReadResponse | null = null

  @Mutation()
  SAVE_SENSOR_MODELS(payload: api.Controllers.NetworkTypes.NetworkTypesListResponse) {
    this.networkTypes = payload
  }

  @Mutation()
  SAVE_SENSOR_MODEL(payload: api.Controllers.NetworkTypes.NetworkTypeReadResponse) {
    this.networkType = payload
  }

  @Action()
  async fetchNetworkTypes(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.NetworkTypes.NetworkTypesListResponse>(`/api/v1/metrics/network-types`, { params: payload })

    this.SAVE_SENSOR_MODELS(response.data)

    return response
  }

  @Action()
  async fetchNetworkType(payload: api.Controllers.NetworkTypes.NetworkTypeReadRequest) {
    const response = await axios.get<api.Controllers.NetworkTypes.NetworkTypeReadResponse>(`/api/v1/metrics/network-types/${payload.id}`)

    this.SAVE_SENSOR_MODEL(response.data)

    return response
  }

  @Action()
  async deleteNetworkType(payload: api.Controllers.NetworkTypes.NetworkTypeDeleteRequest) {
    const response = await axios.delete<api.Controllers.NetworkTypes.NetworkTypeDeleteResponse>(`/api/v1/metrics/network-types/${payload.id}`)

    return response
  }

}
