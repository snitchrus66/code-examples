import { Module } from 'vuex-simple'

import ControllerModelsModule from '~/store/modules/metricControllers/controllerModels'
import UnitMeasurementsModule from '~/store/modules/metricControllers/unitMeasurements'
import SensorModelsModule from '~/store/modules/metricControllers/sensorModels'
import TransferProtocolsModule from '~/store/modules/metricControllers/transferProtocols'
import TsDbServersModule from '~/store/modules/metricControllers/tsDbServers'
import TsDatabasesModule from '~/store/modules/metricControllers/tsDatabases'
import ControllersModule from '~/store/modules/metricControllers/controllers'
import NetworkTypesModule from '~/store/modules/metricControllers/networkTypes'
import ControllerNetworksModule from '~/store/modules/metricControllers/controllerNetworks'
import DeviceModelsModule from '~/store/modules/metricControllers/deviceModels'
import DevicesModule from '~/store/modules/metricControllers/devices'
import SensorChannelsModule from '~/store/modules/metricControllers/sensorChannels'
import SensorsModule from '~/store/modules/metricControllers/sensors'
import MqttServersModule from '~/store/modules/metricControllers/mqttServers'
import SensorChannelGraphsModule from '~/store/modules/metricControllers/sensorChannelGraphs'
import GraphsModule from '~/store/modules/metricControllers/graphs'

export default class MetricControllersModule {

  @Module()
  controllerModels = new ControllerModelsModule()

  @Module()
  unitMeasurements = new UnitMeasurementsModule()

  @Module()
  sensorModels = new SensorModelsModule()

  @Module()
  transferProtocols = new TransferProtocolsModule()

  @Module()
  tsDbServers = new TsDbServersModule()

  @Module()
  tsDatabases = new TsDatabasesModule()

  @Module()
  controllers = new ControllersModule()

  @Module()
  networkTypes = new NetworkTypesModule()

  @Module()
  controllerNetworks = new ControllerNetworksModule()

  @Module()
  deviceModels = new DeviceModelsModule()

  @Module()
  devices = new DevicesModule()

  @Module()
  sensorChannels = new SensorChannelsModule()

  @Module()
  sensors = new SensorsModule()

  @Module()
  mqttServers = new MqttServersModule()

  @Module()
  sensorChannelGraphs = new SensorChannelGraphsModule()

  @Module()
  graphs = new GraphsModule()

}