
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class ControllerNetworksModule {
  @State()
  controllerNetworks: api.Controllers.ControllerNetworks.СontrollerNetworksListResponse | null = null

  @State()
  controllerNetwork: api.Controllers.ControllerNetworks.СontrollerNetworkReadResponse | null = null

  @Mutation()
  SAVE_CONTROLLER_NETWORKS(payload: api.Controllers.ControllerNetworks.СontrollerNetworksListResponse) {
    this.controllerNetworks = payload
  }

  @Mutation()
  SAVE_CONTROLLER_NETWORK(payload: api.Controllers.ControllerNetworks.СontrollerNetworkReadResponse) {
    this.controllerNetwork = payload
  }

  @Action()
  async fetchControllerNetworks(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.ControllerNetworks.СontrollerNetworksListResponse>(`/api/v1/metrics/controller-networks`, { params: payload })

    this.SAVE_CONTROLLER_NETWORKS(response.data)

    return response
  }

  @Action()
  async fetchControllerNetwork(payload: api.Controllers.ControllerNetworks.СontrollerNetworkReadRequest) {
    const response = await axios.get<api.Controllers.ControllerNetworks.СontrollerNetworkReadResponse>(`/api/v1/metrics/controller-networks/${payload.id}`)

    this.SAVE_CONTROLLER_NETWORK(response.data)

    return response
  }

  @Action()
  async deleteControllerNetwork(payload: api.Controllers.ControllerNetworks.СontrollerNetworkDeleteRequest) {
    const response = await axios.delete<api.Controllers.ControllerNetworks.СontrollerNetworkDeleteResponse>(`/api/v1/metrics/controller-networks/${payload.id}`)

    return response
  }

}
