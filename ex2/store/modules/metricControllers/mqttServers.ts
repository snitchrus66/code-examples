
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class MqttServersModule {
  @State()
  mqttServers: api.Controllers.MqttServers.MqttServersListResponse | null = null

  @State()
  mqttServer: api.Controllers.MqttServers.MqttServerReadResponse | null = null

  @Mutation()
  SAVE_MQTT_SERVERS(payload: api.Controllers.MqttServers.MqttServersListResponse) {
    this.mqttServers = payload
  }

  @Mutation()
  SAVE_MQTT_SERVER(payload: api.Controllers.MqttServers.MqttServerReadResponse) {
    this.mqttServer = payload
  }

  @Action()
  async fetchMqttServers(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.MqttServers.MqttServersListResponse>(`/api/v1/metrics/mqtt-servers`, { params: payload })

    this.SAVE_MQTT_SERVERS(response.data)

    return response
  }

  @Action()
  async fetchMqttServer(payload: api.Controllers.MqttServers.MqttServerReadRequest) {
    const response = await axios.get<api.Controllers.MqttServers.MqttServerReadResponse>(`/api/v1/metrics/mqtt-servers/${payload.id}`)

    this.SAVE_MQTT_SERVER(response.data)

    return response
  }

  @Action()
  async deleteMqttServer(payload: api.Controllers.MqttServers.MqttServerDeleteRequest) {
    const response = await axios.delete<api.Controllers.MqttServers.MqttServerDeleteResponse>(`/api/v1/metrics/mqtt-servers/${payload.id}`)

    return response
  }

}
