
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class TsDbServersModule {
  @State()
  tsDbServers: api.Controllers.TsDbServers.TsDbServersListResponse | null = null

  @State()
  tsDbServer: api.Controllers.TsDbServers.TsDbServerReadResponse | null = null

  @Mutation()
  SAVE_TS_DB_SERVERS(payload: api.Controllers.TsDbServers.TsDbServersListResponse) {
    this.tsDbServers = payload
  }

  @Mutation()
  SAVE_TS_DB_SERVER(payload: api.Controllers.TsDbServers.TsDbServerReadResponse) {
    this.tsDbServer = payload
  }

  @Action()
  async fetchTsDbServers(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.TsDbServers.TsDbServersListResponse>(`/api/v1/metrics/ts-db-servers`, { params: payload })

    this.SAVE_TS_DB_SERVERS(response.data)

    return response
  }

  @Action()
  async fetchTsDbServer(payload: api.Controllers.TsDbServers.TsDbServerReadRequest) {
    const response = await axios.get<api.Controllers.TsDbServers.TsDbServerReadResponse>(`/api/v1/metrics/ts-db-servers/${payload.id}`)

    this.SAVE_TS_DB_SERVER(response.data)

    return response
  }

  @Action()
  async deleteTsDbServer(payload: api.Controllers.TsDbServers.TsDbServerDeleteRequest) {
    const response = await axios.delete<api.Controllers.TsDbServers.TsDbServerDeleteResponse>(`/api/v1/metrics/ts-db-servers/${payload.id}`)

    return response
  }

}
