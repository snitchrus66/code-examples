
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class ControllersModule {
  @State()
  controllers: api.Controllers.Controllers.ControllersListResponse | null = null

  @State()
  controller: api.Controllers.Controllers.ControllerReadResponse | null = null

  @Mutation()
  SAVE_CONTROLLERS(payload: api.Controllers.Controllers.ControllersListResponse) {
    this.controllers = payload
  }

  @Mutation()
  SAVE_CONTROLLER(payload: api.Controllers.Controllers.ControllerReadResponse) {
    this.controller = payload
  }

  @Action()
  async fetchControllers(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.Controllers.ControllersListResponse>(`/api/v1/metrics/controllers`, { params: payload })

    this.SAVE_CONTROLLERS(response.data)

    return response
  }

  @Action()
  async fetchController(payload: api.Controllers.Controllers.ControllerReadRequest) {
    const response = await axios.get<api.Controllers.Controllers.ControllerReadResponse>(`/api/v1/metrics/controllers/${payload.id}`)

    this.SAVE_CONTROLLER(response.data)

    return response
  }

  @Action()
  async deleteController(payload: api.Controllers.Controllers.ControllerDeleteRequest) {
    const response = await axios.delete<api.Controllers.Controllers.ControllerDeleteResponse>(`/api/v1/metrics/controllers/${payload.id}`)

    return response
  }

}
