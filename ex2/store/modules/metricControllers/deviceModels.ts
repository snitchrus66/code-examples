
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class DeviceModelsModule {
  @State()
  deviceModels: api.Controllers.DeviceModels.DeviceModelsListResponse | null = null

  @State()
  deviceModel: api.Controllers.DeviceModels.DeviceModelReadResponse | null = null

  @Mutation()
  SAVE_DEVICE_MODELS(payload: api.Controllers.DeviceModels.DeviceModelsListResponse) {
    this.deviceModels = payload
  }

  @Mutation()
  SAVE_DEVICE_MODEL(payload: api.Controllers.DeviceModels.DeviceModelReadResponse) {
    this.deviceModel = payload
  }

  @Action()
  async fetchDeviceModels(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.DeviceModels.DeviceModelsListResponse>(`/api/v1/metrics/rs485-dev-models`, { params: payload })

    this.SAVE_DEVICE_MODELS(response.data)

    return response
  }

  @Action()
  async fetchDeviceModel(payload: api.Controllers.DeviceModels.DeviceModelReadRequest) {
    const response = await axios.get<api.Controllers.DeviceModels.DeviceModelReadResponse>(`/api/v1/metrics/rs485-dev-models/${payload.id}`)

    this.SAVE_DEVICE_MODEL(response.data)

    return response
  }

  @Action()
  async deleteDeviceModel(payload: api.Controllers.DeviceModels.DeviceModelDeleteRequest) {
    const response = await axios.delete<api.Controllers.DeviceModels.DeviceModelDeleteResponse>(`/api/v1/metrics/rs485-dev-models/${payload.id}`)

    return response
  }

}
