
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class TransferProtocolsModule {
  @State()
  transferProtocols: api.Controllers.TransferProtocols.TransferProtocolsListResponse | null = null

  @State()
  transferProtocol: api.Controllers.TransferProtocols.TransferProtocolReadResponse | null = null

  @Mutation()
  SAVE_TRANSFER_PROTOCOLS(payload: api.Controllers.TransferProtocols.TransferProtocolsListResponse) {
    this.transferProtocols = payload
  }

  @Mutation()
  SAVE_TRANSFER_PROTOCOL(payload: api.Controllers.TransferProtocols.TransferProtocolReadResponse) {
    this.transferProtocol = payload
  }

  @Action()
  async fetchTransferProtocols(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Controllers.TransferProtocols.TransferProtocolsListResponse>(`/api/v1/metrics/data-transfer-protocols`, { params: payload })

    this.SAVE_TRANSFER_PROTOCOLS(response.data)

    return response
  }

  @Action()
  async fetchTransferProtocol(payload: api.Controllers.TransferProtocols.TransferProtocolReadRequest) {
    const response = await axios.get<api.Controllers.TransferProtocols.TransferProtocolReadResponse>(`/api/v1/metrics/data-transfer-protocols/${payload.id}`)

    this.SAVE_TRANSFER_PROTOCOL(response.data)

    return response
  }

  @Action()
  async deleteTransferProtocol(payload: api.Controllers.TransferProtocols.TransferProtocolDeleteRequest) {
    const response = await axios.delete<api.Controllers.TransferProtocols.TransferProtocolDeleteResponse>(`/api/v1/metrics/data-transfer-protocols/${payload.id}`)

    return response
  }

}
