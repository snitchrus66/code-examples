import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class RolesModule {
  @State()
  roles: any[] = []

  @State()
  allRoles: any[] = []

  @State()
  role: any[] = []

  @State()
  permissions: any[] = []

  @Mutation()
  SAVE_ROLES(payload: { roles: any[] }) {
    this.roles = payload.roles
  }

  @Mutation()
  SAVE_ALL_ROLES(payload: { allRoles: any[] }) {
    this.allRoles = payload.allRoles
  }

  @Mutation()
  SAVE_ROLE(payload: { role: any[] }) {
    this.role = payload.role
  }

  @Mutation()
  SAVE_PERMISSIONS(payload: { permissions: any[] }) {
    this.permissions = payload.permissions
  }

  @Action()
  async fetchRoles(payload: api.ListOptions = {}) {
    const response = await axios.get('/api/v1/roles', { params: payload })

    this.SAVE_ROLES({ roles: response.data })

    return response
  }

  @Action()
  async fetchAllRoles() {
    const { data } = await axios.get('/api/v1/roles/all')

    this.SAVE_ALL_ROLES({ allRoles: data })
  }

  @Action()
  async fetchRole(payload: { id: number }) {
    const { data } = await axios.get('/api/v1/roles/' + payload.id)

    this.SAVE_ROLE({ role: data })
  }

  @Action()
  async fetchPermissions() {
    const { data } = await axios.get('/api/v1/permissions')

    this.SAVE_PERMISSIONS({ permissions: data })
  }

  @Action()
  async deleteRole(payload: { id: number }) {
    const response = await axios.delete(`/api/v1/roles/${payload.id}`)

    return response
  }
}
