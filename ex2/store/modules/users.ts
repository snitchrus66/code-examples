import { State, Mutation, Action, Getter } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class UsersModule {
  @State()
  users: any[] = []

  @State()
  user: any = null

  @State()
  userPhotos: api.Medialib.Files.FilesListResponse | null = null

  @State()
  userRemovedPhotos: api.Medialib.Files.FilesListResponse | null = null

  @State()
  userFiles: api.Medialib.Files.FilesListResponse | null = null

  @State()
  userRemovedFiles: api.Medialib.Files.FilesListResponse | null = null

  @Mutation()
  SAVE_USERS(payload: { users: any[] }) {
    this.users = payload.users
  }

  @Mutation()
  SAVE_USER(payload: { user: any }) {
    this.user = payload.user
  }

  @Mutation()
  SAVE_USER_PHOTOS(payload: api.Medialib.Files.FilesListResponse) {
    this.userPhotos = payload
  }

  @Mutation()
  SAVE_USER_REMOVED_PHOTOS(payload: api.Medialib.Files.FilesListResponse) {
    this.userRemovedPhotos = payload
  }

  @Mutation()
  SAVE_USER_FILES(payload: api.Medialib.Files.FilesListResponse) {
    this.userFiles = payload
  }

  @Mutation()
  SAVE_USER_REMOVED_FILES(payload: api.Medialib.Files.FilesListResponse) {
    this.userRemovedFiles = payload
  }

  @Action()
  async fetchUsers(payload: api.ListOptions) {
    const params = {
      search: payload.search,
      orderby: payload.orderby,
      direction: payload.direction,
      page: payload.page
    }
    const response = await axios.get('/api/v1/users', { params })

    this.SAVE_USERS({ users: response.data })

    return response
  }

  @Action()
  async fetchUser(payload: { id: number }) {
    const response = await axios.get(`/api/v1/users/${payload.id}`)

    response.data.qr = response.data.qr + `?random=${new Date().getTime()}`
    this.SAVE_USER({ user: response.data })

    return response
  }

  @Action()
  async deleteUser(payload: { id: number }) {
    const response = await axios.delete(`/api/v1/users/${payload.id}`)

    return response
  }

  @Action()
  async fetchUserPhotos(userId: number) {
    const request: api.Medialib.Files.FilesListRequest = {
      entity: api.Medialib.Entities.User,
      entity_id: userId,
      group: api.Medialib.Groups.Photos
    }
    const response = await axios.get<api.Medialib.Files.FilesListResponse>(`/api/v1/medialib-module/get-list`, { params: request })

    this.SAVE_USER_PHOTOS(response.data)

    return response
  }

  @Action()
  async fetchUserRemovedPhotos(userId: number) {
    const request: api.Medialib.Files.FilesListRequest = {
      entity: api.Medialib.Entities.User,
      entity_id: userId,
      group: api.Medialib.Groups.Photos,
      show: 'removed'
    }
    const response = await axios.get<api.Medialib.Files.FilesListResponse>(`/api/v1/medialib-module/get-list`, { params: request })

    this.SAVE_USER_REMOVED_PHOTOS(response.data)

    return response
  }

  @Action()
  async fetchUserFiles(userId: number) {
    const request: api.Medialib.Files.FilesListRequest = {
      entity: api.Medialib.Entities.User,
      entity_id: userId,
      group: api.Medialib.Groups.Files
    }
    const response = await axios.get<api.Medialib.Files.FilesListResponse>(`/api/v1/medialib-module/get-list`, { params: request })

    this.SAVE_USER_FILES(response.data)

    return response
  }

  @Action()
  async fetchUserRemovedFiles(userId: number) {
    const request: api.Medialib.Files.FilesListRequest = {
      entity: api.Medialib.Entities.User,
      entity_id: userId,
      group: api.Medialib.Groups.Files,
      show: 'removed'
    }
    const response = await axios.get<api.Medialib.Files.FilesListResponse>(`/api/v1/medialib-module/get-list`, { params: request })

    this.SAVE_USER_REMOVED_FILES(response.data)

    return response
  }

  @Action()
  async deleteUserPhoto(payload: Pick<api.Medialib.Files.FileDeleteSoftRequest, 'id'>) {
    const response = await axios.delete<api.Medialib.Files.FileDeleteSoftResponse>(
      `/api/v1/medialib-module/${payload.id}/soft`,
      {
        params: {
          group: api.Medialib.Groups.Photos
        }
      }
    )

    return response
  }

  @Action()
  async deleteUserFile(payload: Pick<api.Medialib.Files.FileDeleteSoftRequest, 'id'>) {
    const response = await axios.delete<api.Medialib.Files.FileDeleteSoftResponse>(
      `/api/v1/medialib-module/${payload.id}/soft`,
      {
        params: {
          group: api.Medialib.Groups.Files
        }
      }
    )

    return response
  }
}
