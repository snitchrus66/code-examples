import { State, Mutation, Action } from 'vuex-simple'
import Cookies from 'js-cookie'
import moment from 'moment'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const { locale, locales }: { locale: string, locales: any } = (window as any).config

export default class LangModule {
  @State()
  locale = 'ru' //Cookies.get('locale') ?? locale

  @State()
  locales = locales

  @Mutation()
  SET_LOCALE(payload: { locale: string }) {
    this.locale = payload.locale
  }

  @Action()
  setLocale(payload: { locale: string }) {
    moment.locale(payload.locale)
    this.SET_LOCALE({ locale: payload.locale })
    Cookies.set('locale', payload.locale, { expires: 365 })
  }
}
