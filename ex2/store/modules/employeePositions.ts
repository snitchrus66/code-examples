
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class EmployeePositionsModule {
  @State()
  employeePositions: api.EmployeePositions.EmployeePositionsListResponse | null = null

  @State()
  employeePosition: api.EmployeePositions.EmployeePositionReadResponse | null = null

  @Mutation()
  SAVE_EMPLOYEE_POSITIONS(payload: api.EmployeePositions.EmployeePositionsListResponse) {
    this.employeePositions = payload
  }

  @Mutation()
  SAVE_EMPLOYEE_POSITION(payload: api.EmployeePositions.EmployeePositionReadResponse) {
    this.employeePosition = payload
  }

  @Action()
  async fetchEmployeePositions(payload: api.EmployeePositions.EmployeePositionsListRequest = {}) {
    const response = await axios.get<api.EmployeePositions.EmployeePositionsListResponse>(`/api/v1/employee-positions`, { params: payload })

    this.SAVE_EMPLOYEE_POSITIONS(response.data)

    return response
  }

  @Action()
  async fetchEmployeePosition(payload: api.EmployeePositions.EmployeePositionReadRequest) {
    const response = await axios.get<api.EmployeePositions.EmployeePositionReadResponse>(`/api/v1/employee-positions/${payload.id}`)

    this.SAVE_EMPLOYEE_POSITION(response.data)

    return response
  }

  @Action()
  async deleteEmployeePosition(payload: api.EmployeePositions.EmployeePositionDeleteRequest) {
    const response = await axios.delete<api.EmployeePositions.EmployeePositionDeleteResponse>(`/api/v1/employee-positions/${payload.id}`)

    return response
  }
}
