import { State, Getter, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class CounterpartiesModule {
  @State()
  counterparty: api.Counterparties.CounterpartyReadResponse | null = null

  @State()
  counterparties: api.Counterparties.CounterpartiesListResponse | null = null

  @State()
  counterpartyTypes: api.Counterparties.CounterpartyTypesListResponse | null = null

  @Getter()
  get labelOfCounterparty() {
    return (counterparty: {
      rus_name?: string
      en_name?: string
      person_name?: string
    }) => {
      return counterparty.rus_name
        ? counterparty.rus_name
        : counterparty.en_name
          ? counterparty.en_name
          : counterparty.person_name
            ? counterparty.person_name
            : 'Название или ФИО отсутствует'
    }
  }

  @Mutation()
  SAVE_COUNTERPARTY(payload: api.Counterparties.CounterpartyReadResponse) {
    this.counterparty = payload
  }

  @Mutation()
  SAVE_COUNTERPARTIES(payload: api.Counterparties.CounterpartiesListResponse) {
    this.counterparties = payload
  }

  @Mutation()
  SAVE_COUNTERPARTY_TYPES(payload: api.Counterparties.CounterpartyTypesListResponse) {
    this.counterpartyTypes = payload
  }

  @Action()
  async fetchCounterparty(payload: api.Counterparties.CounterpartyReadRequest) {
    const response = await axios.get<api.Counterparties.CounterpartyReadResponse>(`/api/v1/counterparties/${payload.id}`)

    this.SAVE_COUNTERPARTY(response.data)

    return response
  }

  @Action()
  async fetchCounterparties(payload: api.Counterparties.CounterpartiesListRequest) {
    const response = await axios.get<api.Counterparties.CounterpartiesListResponse>(`/api/v1/counterparties`, { params: payload })

    this.SAVE_COUNTERPARTIES(response.data)

    return response
  }

  @Action()
  async fetchCounterpartyTypes() {
    const response = await axios.get<api.Counterparties.CounterpartyTypesListResponse>(`/api/v1/counterparties/types`)

    this.SAVE_COUNTERPARTY_TYPES(response.data)

    return response
  }

  @Action()
  async deleteCounterparty(payload: api.Counterparties.CounterpartyDeleteRequest) {
    const response = await axios.delete<api.Counterparties.CounterpartyDeleteResponse>(`/api/v1/counterparties/${payload.id}`)

    return response
  }
}
