
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class VidecamModelsModule {
  @State()
  videcamModels: api.Videcams.VidecamModels.VidecamModelsListResponse | null = null

  @State()
  videcamModel: api.Videcams.VidecamModels.VidecamModelReadResponse | null = null

  @Mutation()
  SAVE_VIDECAM_MODELS(payload: api.Videcams.VidecamModels.VidecamModelsListResponse) {
    this.videcamModels = payload
  }

  @Mutation()
  SAVE_VIDECAM_MODEL(payload: api.Videcams.VidecamModels.VidecamModelReadResponse) {
    this.videcamModel = payload
  }

  @Action()
  async fetchVidecamModels(payload: api.ListOptions = {}) {
    const response = await axios.get<api.Videcams.VidecamModels.VidecamModelsListResponse>(`/api/v1/videcam-models`, { params: payload })

    this.SAVE_VIDECAM_MODELS(response.data)

    return response
  }

  @Action()
  async fetchVidecamModel(payload: api.Videcams.VidecamModels.VidecamModelReadRequest) {
    const response = await axios.get<api.Videcams.VidecamModels.VidecamModelReadResponse>(`/api/v1/videcam-models/${payload.id}`)

    this.SAVE_VIDECAM_MODEL(response.data)

    return response
  }

  @Action()
  async deleteVidecamModel(payload: api.Videcams.VidecamModels.VidecamModelDeleteRequest) {
    const response = await axios.delete<api.Videcams.VidecamModels.VidecamModelDeleteResponse>(`/api/v1/videcam-models/${payload.id}`)

    return response
  }

}
