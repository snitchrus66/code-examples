import { Module } from 'vuex-simple'

import VidecamModelsModule from '~/store/modules/cams/videcamModels'
import VidecamsModule from '~/store/modules/cams/videcams'

export default class AllVidecamsModule {

  @Module()
  videcams = new VidecamsModule()

  @Module()
  videcamModels = new VidecamModelsModule()

}