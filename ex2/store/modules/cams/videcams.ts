
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class VidecamsModule {
  @State()
  videcams: api.Videcams.Videcams.VidecamsListResponse | null = null

  @State()
  videcam: api.Videcams.Videcams.VidecamReadResponse | null = null

  @Mutation()
  SAVE_VIDECAMS(payload: api.Videcams.Videcams.VidecamsListResponse) {
    this.videcams = payload
  }

  @Mutation()
  SAVE_VIDECAM(payload: api.Videcams.Videcams.VidecamReadResponse) {
    this.videcam = payload
  }

  @Action()
  async fetchVidecams(payload: api.Videcams.Videcams.VidecamsListRequest = {}) {
    const response = await axios.get<api.Videcams.Videcams.VidecamsListResponse>(`/api/v1/videcams`, { params: payload })

    this.SAVE_VIDECAMS(response.data)

    return response
  }

  @Action()
  async fetchVidecam(payload: api.Videcams.Videcams.VidecamReadRequest) {
    const response = await axios.get<api.Videcams.Videcams.VidecamReadResponse>(`/api/v1/videcams/${payload.id}`)

    this.SAVE_VIDECAM(response.data)

    return response
  }

  @Action()
  async deleteVidecam(payload: api.Videcams.Videcams.VidecamDeleteRequest) {
    const response = await axios.delete<api.Videcams.Videcams.VidecamDeleteResponse>(`/api/v1/videcams/${payload.id}`)

    return response
  }

}
