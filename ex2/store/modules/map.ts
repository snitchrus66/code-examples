import { State, Getter, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class MapModule {
  @State()
  mapObject: api.MapObjects.MapObjectReadResponse | null = null

  @State()
  mapObjects: api.MapObjects.MapObjectsListResponse | null = null

  @State()
  mapObjectType: api.MapObjectTypes.MapObjectTypeReadResponse | null = null

  @State()
  mapObjectTypes: api.MapObjectTypes.MapObjectTypesListResponse | null = null

  @State()
  mapObjectPhotos: api.Medialib.Files.FilesListResponse | null = null

  @State()
  mapObjectRemovedPhotos: api.Medialib.Files.FilesListResponse | null = null

  @State()
  mapObjectFiles: api.Medialib.Files.FilesListResponse | null = null

  @State()
  mapObjectRemovedFiles: api.Medialib.Files.FilesListResponse | null = null

  @Getter()
  get mapObjectsOfType() {
    return (mapObjectTypeId: number) => {
      return this.mapObjects?.data.filter(mapObject => mapObject.type === mapObjectTypeId) ?? []
    }
  }

  @Getter()
  get mapObjectWithId() {
    return (id: number) => {
      return this.mapObjects?.data.find(mapObject => mapObject.id === id) ?? null
    }
  }

  @Mutation()
  SAVE_MAP_OBJECT(payload: api.MapObjects.MapObjectReadResponse) {
    this.mapObject = payload
  }

  @Mutation()
  SAVE_MAP_OBJECTS(payload: api.MapObjects.MapObjectsListResponse) {
    this.mapObjects = payload
  }

  @Mutation()
  SAVE_MAP_OBJECT_TYPE(payload: api.MapObjectTypes.MapObjectTypeReadResponse) {
    this.mapObjectType = payload
  }

  @Mutation()
  SAVE_MAP_OBJECT_TYPES(payload: api.MapObjectTypes.MapObjectTypesListResponse) {
    this.mapObjectTypes = payload
  }

  @Mutation()
  SAVE_MAP_OBJECT_PHOTOS(payload: api.Medialib.Files.FilesListResponse) {
    this.mapObjectPhotos = payload
  }

  @Mutation()
  SAVE_MAP_OBJECT_REMOVED_PHOTOS(payload: api.Medialib.Files.FilesListResponse) {
    this.mapObjectRemovedPhotos = payload
  }

  @Mutation()
  SAVE_MAP_OBJECT_FILES(payload: api.Medialib.Files.FilesListResponse) {
    this.mapObjectFiles = payload
  }

  @Mutation()
  SAVE_MAP_OBJECT_REMOVED_FILES(payload: api.Medialib.Files.FilesListResponse) {
    this.mapObjectRemovedFiles = payload
  }

  @Action()
  async fetchMapObject(payload: api.MapObjects.MapObjectReadRequest) {
    const response = await axios.get<api.MapObjects.MapObjectReadResponse>(`/api/v1/map-objects/${payload.id}`)

    this.SAVE_MAP_OBJECT(response.data)

    return response
  }

  @Action()
  async fetchMapObjects(payload: api.MapObjects.MapObjectsListRequest = {}) {
    const response = await axios.get<api.MapObjects.MapObjectsListResponse>(`/api/v1/map-objects`, { params: payload })

    this.SAVE_MAP_OBJECTS(response.data)

    return response
  }

  @Action()
  async fetchMapObjectType(payload: api.MapObjectTypes.MapObjectTypeReadRequest) {
    const response = await axios.get<api.MapObjectTypes.MapObjectTypeReadResponse>(`/api/v1/map-object-types/${payload.id}`)

    this.SAVE_MAP_OBJECT_TYPE(response.data)

    return response
  }

  @Action()
  async fetchMapObjectTypes(payload: api.MapObjectTypes.MapObjectTypesListRequest) {
    const response = await axios.get<api.MapObjectTypes.MapObjectTypesListResponse>(`/api/v1/map-object-types`, { params: payload })

    this.SAVE_MAP_OBJECT_TYPES(response.data)

    return response
  }

  @Action()
  async deleteMapObject(payload: api.MapObjects.MapObjectDeleteRequest) {
    const response = await axios.delete<api.MapObjects.MapObjectDeleteResponse>(`/api/v1/map-objects/${payload.id}`)

    return response
  }

  @Action()
  async deleteMapObjectType(payload: api.MapObjectTypes.MapObjectTypeDeleteRequest) {
    const response = await axios.delete<api.MapObjectTypes.MapObjectTypeDeleteResponse>(`/api/v1/map-object-types/${payload.id}`)

    return response
  }

  @Action()
  async fetchMapObjectPhotos(mapObjectId: number) {
    const request: api.Medialib.Files.FilesListRequest = {
      entity: api.Medialib.Entities.MapObject,
      entity_id: mapObjectId,
      group: api.Medialib.Groups.Photos
    }
    const response = await axios.get<api.Medialib.Files.FilesListResponse>(`/api/v1/medialib-module/get-list`, { params: request })

    this.SAVE_MAP_OBJECT_PHOTOS(response.data)

    return response
  }

  @Action()
  async fetchMapObjectRemovedPhotos(mapObjectId: number) {
    const request: api.Medialib.Files.FilesListRequest = {
      entity: api.Medialib.Entities.MapObject,
      entity_id: mapObjectId,
      group: api.Medialib.Groups.Photos,
      show: 'removed'
    }
    const response = await axios.get<api.Medialib.Files.FilesListResponse>(`/api/v1/medialib-module/get-list`, { params: request })

    this.SAVE_MAP_OBJECT_REMOVED_PHOTOS(response.data)

    return response
  }

  @Action()
  async fetchMapObjectFiles(mapObjectId: number) {
    const request: api.Medialib.Files.FilesListRequest = {
      entity: api.Medialib.Entities.MapObject,
      entity_id: mapObjectId,
      group: api.Medialib.Groups.Files
    }
    const response = await axios.get<api.Medialib.Files.FilesListResponse>(`/api/v1/medialib-module/get-list`, { params: request })

    this.SAVE_MAP_OBJECT_FILES(response.data)

    return response
  }

  @Action()
  async fetchMapObjectRemovedFiles(mapObjectId: number) {
    const request: api.Medialib.Files.FilesListRequest = {
      entity: api.Medialib.Entities.MapObject,
      entity_id: mapObjectId,
      group: api.Medialib.Groups.Files,
      show: 'removed'
    }
    const response = await axios.get<api.Medialib.Files.FilesListResponse>(`/api/v1/medialib-module/get-list`, { params: request })

    this.SAVE_MAP_OBJECT_REMOVED_FILES(response.data)

    return response
  }

  @Action()
  async deleteMapObjectPhoto(payload: Pick<api.Medialib.Files.FileDeleteSoftRequest, 'id'>) {
    const response = await axios.delete<api.Medialib.Files.FileDeleteSoftResponse>(
      `/api/v1/medialib-module/${payload.id}/soft`,
      {
        params: {
          group: api.Medialib.Groups.Photos
        }
      }
    )

    return response
  }

  @Action()
  async deleteMapObjectFile(payload: Pick<api.Medialib.Files.FileDeleteSoftRequest, 'id'>) {
    const response = await axios.delete<api.Medialib.Files.FileDeleteSoftResponse>(
      `/api/v1/medialib-module/${payload.id}/soft`,
      {
        params: {
          group: api.Medialib.Groups.Files
        }
      }
    )

    return response
  }
}
