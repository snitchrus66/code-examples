import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class DbObjectTypesModule {
  @State()
  dbObjectTypes: any

  @State()
  dbObjectType: any

  @Mutation()
  SAVE_DB_OBJECT_TYPES(payload: any) {
    this.dbObjectTypes = payload
  }

  @Mutation()
  SAVE_DB_OBJECT_TYPE(payload: any) {
    this.dbObjectType = payload
  }

  @Action()
  async fetchDbObjectTypes(payload: api.ListOptions = {}) {
    const response = await axios.get(`/api/v1/db-object-types`, { params: payload })

    this.SAVE_DB_OBJECT_TYPES(response.data)

    return response
  }

  @Action()
  async fetchDbObjectType(payload: any) {
    const response = await axios.get(`/api/v1/db-object-types/${payload.id}`)

    this.SAVE_DB_OBJECT_TYPE(response.data)

    return response
  }

  @Action()
  async deleteDbObjectType(payload: any) {
    const response = await axios.delete(`/api/v1/db-object-types/${payload.id}`)

    return response
  }

}
