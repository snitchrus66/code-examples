
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class FilesModule {
  @State()
  fileExtensions: api.Medialib.FileExtensions.FileExtensionsListResponse | null = null

  @State()
  fileExtension: api.Medialib.FileExtensions.FileExtensionReadResponse | null = null

  @State()
  fileTypes: api.Medialib.FileTypes.FileTypesListResponse | null = null

  @State()
  fileType: api.Medialib.FileTypes.FileTypeReadResponse | null = null

  @Mutation()
  SAVE_FILE_EXTENSIONS(payload: api.Medialib.FileExtensions.FileExtensionsListResponse) {
    this.fileExtensions = payload
  }

  @Mutation()
  SAVE_FILE_EXTENSION(payload: api.Medialib.FileExtensions.FileExtensionReadResponse) {
    this.fileExtension = payload
  }

  @Mutation()
  SAVE_FILE_TYPES(payload: api.Medialib.FileTypes.FileTypesListResponse) {
    this.fileTypes = payload
  }

  @Mutation()
  SAVE_FILE_TYPE(payload: api.Medialib.FileTypes.FileTypeReadResponse) {
    this.fileType = payload
  }

  @Action()
  async fetchFileExtensions(payload: api.Medialib.FileExtensions.FileExtensionsListRequest) {
    const response = await axios.get<api.Medialib.FileExtensions.FileExtensionsListResponse>(`/api/v1/medialib-file-extensions`, { params: payload })

    this.SAVE_FILE_EXTENSIONS(response.data)

    return response
  }

  @Action()
  async fetchFileExtension(payload: api.Medialib.FileExtensions.FileExtensionReadRequest) {
    const response = await axios.get<api.Medialib.FileExtensions.FileExtensionReadResponse>(`/api/v1/medialib-file-extensions/${payload.id}`)

    this.SAVE_FILE_EXTENSION(response.data)

    return response
  }

  @Action()
  async deleteFileExtension(payload: api.Medialib.FileExtensions.FileExtensionDeleteRequest) {
    const response = await axios.delete<api.Medialib.FileExtensions.FileExtensionDeleteResponse>(`/api/v1/medialib-file-extensions/${payload.id}`)

    return response
  }

  @Action()
  async fetchFileTypes(payload?: api.Medialib.FileTypes.FileTypesListRequest) {
    const response = await axios.get<api.Medialib.FileTypes.FileTypesListResponse>(`/api/v1/medialib-file-types`, { params: payload })

    this.SAVE_FILE_TYPES(response.data)

    return response
  }

  @Action()
  async fetchFileType(payload: api.Medialib.FileTypes.FileTypeReadRequest) {
    const response = await axios.get<api.Medialib.FileTypes.FileTypeReadResponse>(`/api/v1/medialib-file-types/${payload.id}`)

    this.SAVE_FILE_TYPE(response.data)

    return response
  }

  @Action()
  async deleteFileType(payload: api.Medialib.FileTypes.FileTypeDeleteRequest) {
    const response = await axios.delete<api.Medialib.FileTypes.FileTypeDeleteResponse>(`/api/v1/medialib-file-types/${payload.id}`)

    return response
  }
}
