
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class DepartmentsModule {
  @State()
  departments: api.Departments.DepartmentsListResponse | null = null

  @State()
  department: api.Departments.DepartmentReadResponse | null = null

  @Mutation()
  SAVE_DEPARTMENTS(payload: api.Departments.DepartmentsListResponse) {
    this.departments = payload
  }

  @Mutation()
  SAVE_DEPARTMENT(payload: api.Departments.DepartmentReadResponse) {
    this.department = payload
  }

  @Action()
  async fetchDepartments() {
    const response = await axios.get<api.Departments.DepartmentsListResponse>(`/api/v1/departments`)

    this.SAVE_DEPARTMENTS(response.data)

    return response
  }

  @Action()
  async fetchDepartment(payload: api.Departments.DepartmentReadRequest) {
    const response = await axios.get<api.Departments.DepartmentReadResponse>(`/api/v1/departments/${payload.id}`)

    this.SAVE_DEPARTMENT(response.data)

    return response
  }

  @Action()
  async deleteDepartment(payload: api.Departments.DepartmentDeleteRequest) {
    const response = await axios.delete<api.Departments.DepartmentDeleteResponse>(`/api/v1/departments/${payload.id}`)

    return response
  }
}
