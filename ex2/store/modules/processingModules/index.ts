import { Module } from 'vuex-simple'

import DbObjectTypeModulesModule from '~/store/modules/processingModules/dbObjectTypeModules'
import ModulesModule from '~/store/modules/processingModules/modules'


export default class ProcessingModulesModule {

  @Module()
  dbObjectTypeModules = new DbObjectTypeModulesModule()

  @Module()
  modules = new ModulesModule()

}