import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class ModulesModule {
  @State()
  modules: any

  @State()
  module: any

  @Mutation()
  SAVE_MODULES(payload: any) {
    this.modules = payload
  }

  @Mutation()
  SAVE_MODULE(payload: any) {
    this.module = payload
  }

  @Action()
  async fetchModules(payload: api.ListOptions = {}) {
    const response = await axios.get(`/api/v1/processing-modules/modules`, { params: payload })

    this.SAVE_MODULES(response.data)

    return response
  }

  @Action()
  async fetchModule(payload: any) {
    const response = await axios.get(`/api/v1/processing-modules/modules/${payload.id}`)

    this.SAVE_MODULE(response.data)

    return response
  }

  @Action()
  async deleteModule(payload: any) {
    const response = await axios.delete(`/api/v1/processing-modules/modules/${payload.id}`)

    return response
  }

}
