import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class DbObjectTypeModulesModule {
  @State()
  dbObjectTypeModules: any

  @State()
  dbObjectTypeModule: any

  @Mutation()
  SAVE_DB_OBJECT_TYPE_MODULES(payload: any) {
    this.dbObjectTypeModules = payload
  }

  @Mutation()
  SAVE_DB_OBJECT_TYPE_MODULE(payload: any) {
    this.dbObjectTypeModule = payload
  }

  @Action()
  async fetchDbObjectTypeModules(payload: api.ListOptions = {}) {
    const response = await axios.get(`/api/v1/processing-modules/db-object-type-modules`, { params: payload })

    this.SAVE_DB_OBJECT_TYPE_MODULES(response.data)

    return response
  }

  @Action()
  async fetchDbObjectTypeModule(payload: any) {
    const response = await axios.get(`/api/v1/processing-modules/db-object-type-modules/${payload.id}`)

    this.SAVE_DB_OBJECT_TYPE_MODULE(response.data)

    return response
  }

  @Action()
  async deleteDbObjectTypeModule(payload: any) {
    const response = await axios.delete(`/api/v1/processing-modules/db-object-type-modules/${payload.id}`)

    return response
  }

}
