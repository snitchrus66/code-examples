import { State, Mutation, Action, Getter } from 'vuex-simple'
import * as api from '~/typings/api'
import * as client from '~/typings/client'
import axios from 'axios'
import Cookies from 'js-cookie'

export default class AuthModule {
  @State()
  user: api.Auth.User | null = null

  @State()
  token: string | null = Cookies.get('token') ?? null

  @Getter()
  get check() {
    return this.user !== null
  }

  @Getter()
  get userConfigValue() {
    return (key: string) => {
      const config = JSON.parse(this.user?.config ?? '{}')

      if (config) {
        return config[key] as client.UserConfigValue
      } else {
        return undefined
      }
    }
  }

  @Mutation()
  SAVE_TOKEN(payload: { token: string, remember?: boolean }) {
    this.token = payload.token
    Cookies.set('token', payload.token, { expires: payload.remember ? 365 : undefined })
  }

  @Mutation()
  FETCH_USER_SUCCESS(payload: { user: api.Auth.User }) {
    this.user = payload.user
  }

  @Mutation()
  FETCH_USER_FAILURE() {
    this.token = null
    Cookies.remove('token')
  }

  @Mutation()
  LOGOUT() {
    this.user = null
    this.token = null
    Cookies.remove('token')
  }

  @Mutation()
  UPDATE_USER(payload: { user: api.Auth.User }) {
    if (this.user === null) {
      this.user = payload.user
    } else {
      this.user.first_name = payload.user.first_name
      this.user.last_name = payload.user.last_name
      this.user.middle_name = payload.user.middle_name
      this.user.email = payload.user.email
    }
  }

  @Mutation()
  UPDATE_USER_CONFIG(payload: { config: string}) {
    if (this.user) {
      this.user.config = payload.config
    }
  }

  @Action()
  saveToken(payload: { token: string, remember?: boolean }) {
    this.SAVE_TOKEN(payload)
  }

  @Action()
  async fetchUser() {
    try {
      const { data } = await axios.get('/api/v1/user')

      this.FETCH_USER_SUCCESS({ user: data })
    } catch (e) {
      this.FETCH_USER_FAILURE()
    }
  }

  @Action()
  updateUser(payload: { user: api.Auth.User }) {
    this.UPDATE_USER(payload)
  }

  @Action()
  async updateUserConfig(payload: {
    name: string
    value: client.UserConfigValue
  }) {
    if (this.user) {
      const config = JSON.parse(this.user.config ?? '{}')

      config[payload.name] = payload.value
      this.UPDATE_USER_CONFIG({ config: JSON.stringify(config)})
      return axios.put(`/api/v1/users/${this.user.id}?config_only=1`, {
        config: `${JSON.stringify(config)}`,
        email: this.user.email
      })
    }
  }

  @Action()
  async logout(payload?: { withoutRequest?: boolean }) {
    if (!payload?.withoutRequest) {
      await axios.post('/api/v1/logout')
    }
    this.LOGOUT()
  }

  @Action()
  async fetchOauthUrl(payload: { provider: string }) {
    const { data } = await axios.post(`/api/v1/oauth/${payload.provider}`)

    return data.url
  }
}
