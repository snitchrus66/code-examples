
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class S3EndpointsModule {
  @State()
  s3Endpoints: api.S3Endpoints.S3EndpointsListResponse | null = null

  @State()
  s3Endpoint: api.S3Endpoints.S3EndpointReadResponse | null = null

  @Mutation()
  SAVE_S3_EDNPOINTS(payload: api.S3Endpoints.S3EndpointsListResponse) {
    this.s3Endpoints = payload
  }

  @Mutation()
  SAVE_S3_EDNPOINT(payload: api.S3Endpoints.S3EndpointReadResponse) {
    this.s3Endpoint = payload
  }

  @Action()
  async fetchS3Endpoints(payload: api.ListOptions = {}) {
    const response = await axios.get<api.S3Endpoints.S3EndpointsListResponse>(`/api/v1/s3-endpoints`, { params: payload })

    this.SAVE_S3_EDNPOINTS(response.data)

    return response
  }

  @Action()
  async fetchS3Endpoint(payload: api.S3Endpoints.S3EndpointReadRequest) {
    const response = await axios.get<api.S3Endpoints.S3EndpointReadResponse>(`/api/v1/s3-endpoints/${payload.id}`)

    this.SAVE_S3_EDNPOINT(response.data)

    return response
  }

  @Action()
  async deleteS3Endpoint(payload: api.S3Endpoints.S3EndpointDeleteRequest) {
    const response = await axios.delete<api.S3Endpoints.S3EndpointDeleteResponse>(`/api/v1/s3-endpoints/${payload.id}`)

    return response
  }

}
