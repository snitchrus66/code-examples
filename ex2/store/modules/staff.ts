
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class StaffModule {
  @State()
  staffSchedules: api.StaffSchedules.StaffSchedulesListResponse | null = null

  @State()
  staffSchedule: api.StaffSchedules.StaffScheduleReadResponse | null = null

  @State()
  staffSchedulePositions: api.StaffSchedulePositions.StaffSchedulePositionsListResponse | null = null

  @State()
  staffSchedulePosition: api.StaffSchedulePositions.StaffSchedulePositionReadResponse | null = null

  @Mutation()
  SAVE_STAFF_SCHEDULES(payload: api.StaffSchedules.StaffSchedulesListResponse) {
    this.staffSchedules = payload
  }

  @Mutation()
  SAVE_STAFF_SCHEDULE(payload: api.StaffSchedules.StaffScheduleReadResponse) {
    this.staffSchedule = payload
  }

  @Mutation()
  SAVE_STAFF_SCHEDULE_POSITIONS(payload: api.StaffSchedulePositions.StaffSchedulePositionsListResponse) {
    this.staffSchedulePositions = payload
  }

  @Mutation()
  RESET_STAFF_SCHEDULE_POSITIONS() {
    this.staffSchedulePositions = null
  }

  @Mutation()
  SAVE_STAFF_SCHEDULE_POSITION(payload: api.StaffSchedulePositions.StaffSchedulePositionReadResponse) {
    this.staffSchedulePosition = payload
  }

  @Action()
  async fetchStaffSchedules() {
    const response = await axios.get<api.StaffSchedules.StaffSchedulesListResponse>(`/api/v1/staff-schedules`)

    this.SAVE_STAFF_SCHEDULES(response.data)

    return response
  }

  @Action()
  async fetchStaffSchedule(payload: api.StaffSchedules.StaffScheduleReadRequest) {
    const response = await axios.get<api.StaffSchedules.StaffScheduleReadResponse>(`/api/v1/staff-schedules/${payload.id}`)

    this.SAVE_STAFF_SCHEDULE(response.data)

    return response
  }

  @Action()
  async deleteStaffSchedule(payload: api.StaffSchedules.StaffScheduleDeleteRequest) {
    const response = await axios.delete<api.StaffSchedules.StaffScheduleDeleteResponse>(`/api/v1/staff-schedules/${payload.id}`)

    return response
  }

  @Action()
  async fetchStaffSchedulePositions(payload: api.StaffSchedulePositions.StaffSchedulePositionsListRequest = {}) {
    const response = await axios.get<api.StaffSchedulePositions.StaffSchedulePositionsListResponse>(`/api/v1/staff-schedule-positions`, { params: payload })

    this.SAVE_STAFF_SCHEDULE_POSITIONS(response.data)

    return response
  }

  @Action()
  resetStaffSchedulePositions() {
    this.RESET_STAFF_SCHEDULE_POSITIONS()
  }

  @Action()
  async fetchStaffSchedulePosition(payload: api.StaffSchedulePositions.StaffSchedulePositionReadRequest) {
    const response = await axios.get<api.StaffSchedulePositions.StaffSchedulePositionReadResponse>(`/api/v1/staff-schedule-positions/${payload.id}`)

    this.SAVE_STAFF_SCHEDULE_POSITION(response.data)

    return response
  }

  @Action()
  async deleteStaffSchedulePosition(payload: api.StaffSchedulePositions.StaffSchedulePositionDeleteRequest) {
    const response = await axios.delete<api.StaffSchedulePositions.StaffSchedulePositionDeleteResponse>(`/api/v1/staff-schedule-positions/${payload.id}`)

    return response
  }
}
