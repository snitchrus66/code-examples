import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class DbObjectTypeMessageThreadsModule {
  @State()
  dbObjectTypesMessageThreads: any

  @State()
  dbObjectTypesMessageThread: any

  @Mutation()
  SAVE_DB_OBJECT_TYPES_MESSAGE_THREADS(payload: any) {
    this.dbObjectTypesMessageThreads = payload
  }

  @Mutation()
  SAVE_DB_OBJECT_TYPES_MESSAGE_THREAD(payload: any) {
    this.dbObjectTypesMessageThread = payload
  }

  @Action()
  async fetchDbObjectTypesMessageThreads(payload: api.ListOptions = {}) {
    const response = await axios.get(`/api/v1/messenger/db-object-types-message-threads`, { params: payload })

    this.SAVE_DB_OBJECT_TYPES_MESSAGE_THREADS(response.data)

    return response
  }

  @Action()
  async fetchDbObjectTypesMessageThread(payload: any) {
    const response = await axios.get(`/api/v1/messenger/db-object-types-message-threads/${payload.id}`)

    this.SAVE_DB_OBJECT_TYPES_MESSAGE_THREAD(response.data)

    return response
  }

  @Action()
  async deleteDbObjectTypesMessageThread(payload: any) {
    const response = await axios.delete(`/api/v1/messenger/db-object-types-message-threads/${payload.id}`)

    return response
  }

}
