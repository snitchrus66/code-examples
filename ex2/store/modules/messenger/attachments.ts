import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class AttachmentsModule {
  @State()
  attachments: any

  @State()
  attachment: any

  @Mutation()
  SAVE_ATTACHMENTS(payload: any) {
    this.attachments = payload
  }

  @Mutation()
  SAVE_ATTACHMENT(payload: any) {
    this.attachment = payload
  }

  @Action()
  async fetchAttachments(payload: api.ListOptions = {}) {
    const response = await axios.get(`/api/v1/messenger/attachments`, { params: payload })

    this.SAVE_ATTACHMENTS(response.data)

    return response
  }

  @Action()
  async fetchAttachment(payload: any) {
    const response = await axios.get(`/api/v1/messenger/attachments/${payload.id}`)

    this.SAVE_ATTACHMENT(response.data)

    return response
  }

  @Action()
  async deleteAttachment(payload: any) {
    const response = await axios.delete(`/api/v1/messenger/attachments/${payload.id}`)

    return response
  }

}
