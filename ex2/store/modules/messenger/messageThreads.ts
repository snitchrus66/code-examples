import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class MessageThreadsModule {
  @State()
  messageThreads: any

  @State()
  messageThread: any

  @Mutation()
  SAVE_MESSAGE_THREADS(payload: any) {
    this.messageThreads = payload
  }

  @Mutation()
  SAVE_MESSAGE_THREAD(payload: any) {
    this.messageThread = payload
  }

  @Action()
  async fetchMessageThreads(payload: api.ListOptions = {}) {
    const response = await axios.get(`/api/v1/messenger/message-threads`, { params: payload })

    this.SAVE_MESSAGE_THREADS(response.data)

    return response
  }

  @Action()
  async fetchMessageThread(payload: any) {
    const response = await axios.get(`/api/v1/messenger/message-threads/${payload.id}`)

    this.SAVE_MESSAGE_THREAD(response.data)

    return response
  }

  @Action()
  async deleteMessageThread(payload: any) {
    const response = await axios.delete(`/api/v1/messenger/message-threads/${payload.id}`)

    return response
  }

}
