import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

export default class MessagesModule {
  @State()
  messages: any

  @State()
  message: any

  @Mutation()
  SAVE_MESSAGES(payload: any) {
    this.messages = payload
  }

  @Mutation()
  SAVE_MESSAGE(payload: any) {
    this.message = payload
  }

  @Action()
  async fetchMessages(payload: api.ListOptions = {}) {
    let params = {}
    if (payload) {
      params = {
        search: payload.search,
        orderby: payload.orderby,
        direction: payload.direction,
        page: payload.page
      }
    }

    const response = await axios.get(`/api/v1/messenger/messages`, { params: payload })

    this.SAVE_MESSAGES(response.data)

    return response
  }

  @Action()
  async fetchMessage(payload: any) {
    const response = await axios.get(`/api/v1/messenger/messages/${payload.id}`)

    this.SAVE_MESSAGE(response.data)

    return response
  }

  @Action()
  async deleteMessage(payload: any) {
    const response = await axios.delete(`/api/v1/messenger/messages/${payload.id}`)

    return response
  }

}
