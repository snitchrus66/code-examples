import { Module } from 'vuex-simple'

import DbObjectTypeMessageThreadsModule from '~/store/modules/messenger/dbObjectTypeMessageThreads'
import AttachmentsModule from '~/store/modules/messenger/attachments'
import MessagesModule from '~/store/modules/messenger/messages'
import MessageThreadsModule from '~/store/modules/messenger/messageThreads'


export default class MessengerModule {

  @Module()
  dbObjectTypesMessageThreads = new DbObjectTypeMessageThreadsModule()

  @Module()
  attachments = new AttachmentsModule()

  @Module()
  messages = new MessagesModule()

  @Module()
  messageThreads = new MessageThreadsModule()

}