
import { State, Mutation, Action } from 'vuex-simple'
import axios from 'axios'
import * as api from '~/typings/api'

type StoredContactType = (api.ContactType & { contact_type: string }) | null

export default class ContactsModule {
  @State()
  contacts: any[] = []

  @State()
  contactType: StoredContactType = null

  @State()
  contactTypes: { [contactTypeKey: string]: api.ContactType } = {}

  @Mutation()
  SAVE_CONTACTS(payload: { contacts: any[] }) {
    this.contacts = payload.contacts
  }

  @Mutation()
  SAVE_CONTACT_TYPES(payload: { contactTypes: { [contactTypeKey: string]: api.ContactType } }) {
    this.contactTypes = payload.contactTypes
  }

  @Mutation()
  SAVE_CONTACT_TYPE(payload: StoredContactType) {
    this.contactType = payload
  }

  @Action()
  async fetchContacts(payload: { id: number }) {
    const { data } = await axios.get(`/api/v1/users/${payload.id}/contacts`)

    this.SAVE_CONTACTS({ contacts: data })
  }

  @Action()
  async fetchUserContacts() {
    const { data } = await axios.get(`/api/v1/settings/contacts`)

    this.SAVE_CONTACTS({ contacts: data })
  }

  @Action()
  async fetchContactTypes() {
    const { data } = await axios.get<{ [contactTypeKey: string]: api.ContactType }>(`/api/v1/contact-types`)

    this.SAVE_CONTACT_TYPES({ contactTypes: data })
  }

  @Action()
  async fetchContactType(payload: { id: number }) {
    const { data } = await axios.get<{ [contactTypeKey: string]: api.ContactType }>(`/api/v1/contact-types`)

    Object.keys(data).some((contactTypeKey) => {
      if (data[contactTypeKey].id === payload.id) {
        this.SAVE_CONTACT_TYPE({
          contact_type: contactTypeKey,
          id: data[contactTypeKey].id,
          name: data[contactTypeKey].name,
          icon: data[contactTypeKey].icon
        })

        return true
      }

      return false
    })
  }

  @Action()
  async deleteUserContact(payload: { id: number }) {
    const response = await axios.delete(`/api/v1/settings/contacts/${payload.id}`)

    return response
  }

  @Action()
  async deleteContact(payload: { userId: number, id: number }) {
    const response = await axios.delete(`/api/v1/users/${payload.userId}/contacts/${payload.id}`)

    return response
  }

  @Action()
  async deleteContactType(payload: { id: number }) {
    const response = await axios.delete(`/api/v1/contact-type/${payload.id}`)

    return response
  }
}
