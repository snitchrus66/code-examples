/* eslint-disable @typescript-eslint/no-use-before-define */
import Vue from 'vue'
import store from '~/store'
import Meta from 'vue-meta'
import routes from './routes'
import Router, { Route, NavigationGuardNext } from 'vue-router'
import Cookies from 'js-cookie'
import { sync } from 'vuex-router-sync'
import VueMoment from 'vue-moment'
import moment from 'moment'
import 'moment/locale/ru'

Vue.use(Meta)
Vue.use(Router)
Vue.use(VueMoment, { moment })
moment.locale(Cookies.get('locale'))

// The middleware for every page of the application.
const globalMiddleware = ['locale', 'check-auth']

// Load middleware modules dynamically.
const routeMiddleware = resolveMiddleware(
  require.context('~/middleware', false, /.*\.ts$/)
)

const router = createRouter()

sync(store, router)

export default router

/**
 * Create a new router instance.
 *
 * @return {Router}
 */
function createRouter() {
  const router = new Router({
    scrollBehavior,
    mode: 'history',
    routes
  })

  router.beforeEach(beforeEach)
  router.afterEach(afterEach)

  return router
}

/**
 * Global router guard.
 *
 * @param {Route} to
 * @param {Route} from
 * @param {Function} next
 */
async function beforeEach(to: Route, from: Route, next: NavigationGuardNext) {
  let components: any[] = []

  try {
    // Get the matched components and resolve them.
    components = await resolveComponents(
      router.getMatchedComponents({ ...to })
    )
  } catch (error) {
    if (/^Loading( CSS)? chunk (\d)+ failed\./.test(error.message)) {
      window.location.reload(true)

      return
    }
  }

  if (components.length === 0) {
    return next()
  }

  const componentOptions = components[components.length - 1]?.options ?? components[components.length - 1] ?? null
  // Start the loading bar.
  // @ts-ignore
  if (componentOptions.loading !== false) {
    router.app.$nextTick(() => {
      // @ts-ignore
      if (router.app?.$loading?.start) {
        // @ts-ignore
        router.app.$loading.start()
      }
    })
  }

  // Get the middleware for all the matched components.
  const middleware = getMiddleware(components)

  // Call each middleware.
  callMiddleware(middleware, to, from, (...args: any) => {
    // Set the application layout only if "next()" was called with no args.
    if (args.length === 0) {
      (router.app as any).setLayout(components[0]?.options?.layout ?? '')
    }

    next(...args)
  })
}

/**
 * Global after hook.
 *
 * @param {Route} to
 * @param {Route} from
 * @param {Function} next
 */
async function afterEach(to: Route, from: Route) {
  let components: any[] = []

  try {
    // Get the matched components and resolve them.
    components = await resolveComponents(
      router.getMatchedComponents({ ...to })
    )
  } catch (error) {
    if (/^Loading( CSS)? chunk (\d)+ failed\./.test(error.message)) {
      window.location.reload(true)

      return
    }
  }

  const componentOptions = components[components.length - 1]?.options ?? components[components.length - 1] ?? null

  // @ts-ignore
  await router.app.$nextTick()
  if (componentOptions.loading !== true) {
    (router.app as any).$loading.finish()
  }
}

/**
 * Call each middleware.
 *
 * @param {Array} middleware
 * @param {Route} to
 * @param {Route} from
 * @param {Function} next
 */
function callMiddleware(
  middleware: (Function | string)[],
  to: Route,
  from: Route,
  next: NavigationGuardNext
) {
  const stack = middleware.reverse()
  const _next = (...args: any) => {
    // Stop if "_next" was called with an argument or the stack is empty.
    if (args.length > 0 || stack.length === 0) {
      if (args.length > 0) {
        (router.app as any).$loading.finish()
      }

      return next(...args)
    }

    const middleware = stack.pop()

    if (middleware) {
      if (typeof middleware === 'function') {
        middleware(to, from, _next)
      } else if (routeMiddleware[middleware]) {
        routeMiddleware[middleware](to, from, _next)
      } else {
        throw Error(`Undefined middleware [${middleware}]`)
      }
    }
  }

  _next()
}

/**
 * Resolve async components.
 *
 * @param  {Array} components
 * @return {Array}
 */
function resolveComponents(components: any[]) {
  return Promise.all(
    components.map(
      component =>
        typeof component === 'function'
            && !Object.keys(component).includes('component')
          ? component()
          : component
    )
  )
}

/**
 * Merge the the global middleware with the components middleware.
 *
 * @param  {Array} components
 * @return {Array}
 */
function getMiddleware(components: any[]) {
  const middlewares = [...globalMiddleware]

  components
    .filter((component) => {
      if (component.middleware || component.options?.middleware) {
        return true
      } else {
        return false
      }
    })
    .forEach((component) => {
      const middleware = component.middleware ?? component.options.middleware

      if (middleware && Array.isArray(middleware)) {
        middlewares.push(...middleware)
      } else {
        middlewares.push(middleware)
      }
    })

  return middlewares
}

type Position = { x: number, y: number }
type PositionResult = Position | { selector: string, offset?: Position } | void

/**
 * Scroll Behavior
 *
 * @link https://router.vuejs.org/en/advanced/scroll-behavior.html
 *
 * @param  {Route} to
 * @param  {Route} from
 * @param  {Object|undefined} savedPosition
 * @return {Object}
 */
function scrollBehavior(
  to: Route,
  from: Route,
  savedPosition: Position | void
): PositionResult | Promise<PositionResult> | undefined | null {
  if (savedPosition) {
    return savedPosition
  }

  if (to.hash) {
    return { selector: to.hash }
  }

  const [component] = router.getMatchedComponents({ ...to }).slice(-1)

  if (component && (component as any).scrollToTop === false) {
    return void 0
  }

  return { x: 0, y: 0 }
}

/**
 * @param  {Object} requireContext
 * @return {Object}
 */
function resolveMiddleware(requireContext: Function) {
  return (requireContext as any).keys()
    .map((file: string) =>
      [file.replace(/(^.\/)|(\.ts$)/g, ''), requireContext(file)]
    )
    .reduce((
      guards: { [guardName: string]: Function },
      [name, guard]: [string, { default: Function }]
    ) => (
      { ...guards, [name]: guard.default }
    ), {})
}
