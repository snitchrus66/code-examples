function page(path: string) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
  { path: '/', name: 'welcome', component: page('welcome.vue') },

  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },

  { path: '/home', name: 'home', component: page('home.vue') },

  { path: '/users', name: 'users', component: page('users.vue') },
  { path: '/user/new', name: 'user.new', component: page('user/new.vue') },
  { path: '/user/:id', name: 'user', component: page('user/read.vue') },
  { path: '/user/:id/edit', name: 'user.edit', component: page('user/edit.vue') },
  { path: '/user/:id/contacts', name: 'user.contacts', component: page('user/contacts.vue') },

  { path: '/roles', name: 'roles', component: page('roles.vue') },
  { path: '/role/new', name: 'role.new', component: page('role/new.vue') },
  { path: '/role/:id', name: 'role.edit', component: page('role/edit.vue') },

  { path: '/contact-types', name: 'contact-types', component: page('contact-types.vue') },
  { path: '/contact-type/new', name: 'contact-type.new', component: page('contact-type/new.vue') },
  { path: '/contact-type/:id/edit', name: 'contact-type.edit', component: page('contact-type/edit.vue') },

  { path: '/departments', name: 'departments', component: page('departments.vue') },
  { path: '/departments/new', name: 'department.new', component: page('department/new.vue') },
  { path: '/departments/:id/edit', name: 'department.edit', component: page('department/edit.vue') },

  { path: '/employee-positions', name: 'employee-positions', component: page('employee-positions.vue') },
  { path: '/employee-position/new', name: 'employee-position.new', component: page('employee-position/new.vue') },
  { path: '/employee-position/:id/edit', name: 'employee-position.edit', component: page('employee-position/edit.vue') },

  { path: '/medialib-file-extensions', name: 'medialib-file-extensions', component: page('medialib-file-extensions.vue') },
  { path: '/medialib-file-extensions/new', name: 'medialib-file-extension.new', component: page('medialib-file-extension/new.vue') },
  { path: '/medialib-file-extensions/:id/edit', name: 'medialib-file-extension.edit', component: page('medialib-file-extension/edit.vue') },

  { path: '/medialib-file-types', name: 'medialib-file-types', component: page('medialib-file-types.vue') },
  { path: '/medialib-file-types/new', name: 'medialib-file-type.new', component: page('medialib-file-type/new.vue') },
  { path: '/medialib-file-types/:id/edit', name: 'medialib-file-type.edit', component: page('medialib-file-type/edit.vue') },

  { path: '/map', name: 'map', component: page('map.vue') },

  { path: '/map-objects', name: 'map-objects', component: page('map-objects.vue') },
  { path: '/map-object/new', name: 'map-object.new', component: page('map-object/new.vue') },
  { path: '/map-object/:id', name: 'map-object.read', component: page('map-object/read.vue') },
  { path: '/map-object/:id/edit', name: 'map-object.edit', component: page('map-object/edit.vue') },

  { path: '/map-object-types', name: 'map-object-types', component: page('map-object-types.vue') },
  { path: '/map-object-type/new', name: 'map-object-type.new', component: page('map-object-type/new.vue') },
  { path: '/map-object-type/:id/edit', name: 'map-object-type.edit', component: page('map-object-type/edit.vue') },

  { path: '/counterparties', name: 'counterparties', component: page('counterparties.vue') },
  { path: '/counterparties/new', name: 'counterparty.new', component: page('counterparty/new.vue') },
  { path: '/counterparties/:id/edit', name: 'counterparty.edit', component: page('counterparty/edit.vue') },

  { path: '/staff-schedules', name: 'staff-schedules', component: page('staff-schedules.vue') },
  { path: '/staff-schedule/new', name: 'staff-schedule.new', component: page('staff-schedule/new.vue') },
  { path: '/staff-schedule/:id', name: 'staff-schedule.read', component: page('staff-schedule/read.vue') },
  { path: '/staff-schedule/:id/edit', name: 'staff-schedule.edit', component: page('staff-schedule/edit.vue') },

  { path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'documents', name: 'settings.documents', component: page('settings/documents.vue') },
      { path: 'contacts', name: 'settings.contacts', component: page('settings/contacts.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') }
    ] },

  { path: '/metrics',
    component: page('metrics/index.vue'),
    children: [
      { path: '', redirect: { name: 'metrics.metric-controllers' } },

      { path: 'metric-controller-models', name: 'metrics.metric-controller-models', component: page('metrics/metric-controller-models.vue')},
      { path: 'metric-controller-models/new', name: 'metrics.metric-controller-models.new', component: page('metrics/metric-controller-models/new.vue')},
      { path: 'metric-controller-models/:id/edit', name: 'metrics.metric-controller-models.edit', component: page('metrics/metric-controller-models/edit.vue')},

      { path: 'unit-measurements', name: 'metrics.unit-measurements', component: page('metrics/unit-measurements.vue')},
      { path: 'unit-measurements/new', name: 'metrics.unit-measurements.new', component: page('metrics/unit-measurements/new.vue')},
      { path: 'unit-measurements/:id/edit', name: 'metrics.unit-measurements.edit', component: page('metrics/unit-measurements/edit.vue')},

      { path: 'metric-sensor-models', name: 'metrics.metric-sensor-models', component: page('metrics/metric-sensor-models.vue')},
      { path: 'metric-sensor-models/new', name: 'metrics.metric-sensor-models.new', component: page('metrics/metric-sensor-models/new.vue')},
      { path: 'metric-sensor-models/:id/edit', name: 'metrics.metric-sensor-models.edit', component: page('metrics/metric-sensor-models/edit.vue')},

      { path: 'metric-controllers', name: 'metrics.metric-controllers', component: page('metrics/metric-controllers.vue')},
      { path: 'metric-controllers/new', name: 'metrics.metric-controllers.new', component: page('metrics/metric-controllers/new.vue')},
      { path: 'metric-controllers/:id/edit', name: 'metrics.metric-controllers.edit', component: page('metrics/metric-controllers/edit.vue')},

      { path: 'controller-networks', name: 'metrics.controller-networks', component: page('metrics/controller-networks.vue')},
      { path: 'controller-networks/new', name: 'metrics.controller-networks.new', component: page('metrics/controller-networks/new.vue')},
      { path: 'controller-networks/:id/edit', name: 'metrics.controller-networks.edit', component: page('metrics/controller-networks/edit.vue')},

      { path: 'network-types', name: 'metrics.network-types', component: page('metrics/network-types.vue')},
      { path: 'network-types/new', name: 'metrics.network-types.new', component: page('metrics/network-types/new.vue')},
      { path: 'network-types/:id/edit', name: 'metrics.network-types.edit', component: page('metrics/network-types/edit.vue')},

      { path: 'rs485-device-models', name: 'metrics.rs485-device-models', component: page('metrics/rs485-device-models.vue')},
      { path: 'rs485-device-models/new', name: 'metrics.rs485-device-models.new', component: page('metrics/rs485-device-models/new.vue')},
      { path: 'rs485-device-models/:id/edit', name: 'metrics.rs485-device-models.edit', component: page('metrics/rs485-device-models/edit.vue')},

      { path: 'rs485-devices', name: 'metrics.rs485-devices', component: page('metrics/rs485-devices.vue')},
      { path: 'rs485-devices/new', name: 'metrics.rs485-devices.new', component: page('metrics/rs485-devices/new.vue')},
      { path: 'rs485-devices/:id/edit', name: 'metrics.rs485-devices.edit', component: page('metrics/rs485-devices/edit.vue')},

      { path: 'ts-db-servers', name: 'metrics.ts-db-servers', component: page('metrics/ts-db-servers.vue')},
      { path: 'ts-db-servers/new', name: 'metrics.ts-db-servers.new', component: page('metrics/ts-db-servers/new.vue')},
      { path: 'ts-db-servers/:id/edit', name: 'metrics.ts-db-servers.edit', component: page('metrics/ts-db-servers/edit.vue')},

      { path: 'metric-sensor-channels', name: 'metrics.metric-sensor-channels', component: page('metrics/metric-sensor-channels.vue')},
      { path: 'metric-sensor-channels/new', name: 'metrics.metric-sensor-channels.new', component: page('metrics/metric-sensor-channels/new.vue')},
      { path: 'metric-sensor-channels/:id/edit', name: 'metrics.metric-sensor-channels.edit', component: page('metrics/metric-sensor-channels/edit.vue')},

      { path: 'ts-databases', name: 'metrics.ts-databases', component: page('metrics/ts-databases.vue')},
      { path: 'ts-databases/new', name: 'metrics.ts-databases.new', component: page('metrics/ts-databases/new.vue')},
      { path: 'ts-databases/:id/edit', name: 'metrics.ts-databases.edit', component: page('metrics/ts-databases/edit.vue')},

      { path: 'data-transfer-protocols', name: 'metrics.data-transfer-protocols', component: page('metrics/data-transfer-protocols.vue')},
      { path: 'data-transfer-protocols/new', name: 'metrics.data-transfer-protocols.new', component: page('metrics/data-transfer-protocols/new.vue')},
      { path: 'data-transfer-protocols/:id/edit', name: 'metrics.data-transfer-protocols.edit', component: page('metrics/data-transfer-protocols/edit.vue')},

      { path: 'metric-sensors', name: 'metrics.metric-sensors', component: page('metrics/metric-sensors.vue')},
      { path: 'metric-sensors/new', name: 'metrics.metric-sensors.new', component: page('metrics/metric-sensors/new.vue')},
      { path: 'metric-sensors/:id/edit', name: 'metrics.metric-sensors.edit', component: page('metrics/metric-sensors/edit.vue')},

      { path: 'mqtt-servers', name: 'metrics.mqtt-servers', component: page('metrics/mqtt-servers.vue')},
      { path: 'mqtt-servers/new', name: 'metrics.mqtt-servers.new', component: page('metrics/mqtt-servers/new.vue')},
      { path: 'mqtt-servers/:id/edit', name: 'metrics.mqtt-servers.edit', component: page('metrics/mqtt-servers/edit.vue')},

      { path: 'sensor-channel-graphs', name: 'metrics.sensor-channel-graphs', component: page('metrics/sensor-channel-graphs.vue')},
      { path: 'sensor-channel-graphs/new', name: 'metrics.sensor-channel-graphs.new', component: page('metrics/sensor-channel-graphs/new.vue')},
      { path: 'sensor-channel-graphs/:id/edit', name: 'metrics.sensor-channel-graphs.edit', component: page('metrics/sensor-channel-graphs/edit.vue')},

      { path: 'graphs', name: 'metrics.graphs', component: page('metrics/graphs.vue')},
      { path: 'graphs/new', name: 'metrics.graphs.new', component: page('metrics/graphs/new.vue')},
      { path: 'graphs/:id/edit', name: 'metrics.graphs.edit', component: page('metrics/graphs/edit.vue')}
    ] },

  { path: '/cams',
    component: page('cams/index.vue'),
    children: [
      { path: '', redirect: { name: 'videcams.videcams' } },

      { path: 'videcams', name: 'videcams.videcams', component: page('cams/videcams.vue')},
      { path: 'videcams/new', name: 'videcams.videcams.new', component: page('cams/videcams/new.vue')},
      { path: 'videcams/:id/edit', name: 'videcams.videcams.edit', component: page('cams/videcams/edit.vue')},

      { path: 'videcam-models', name: 'videcams.videcam-models', component: page('cams/videcam-models.vue')},
      { path: 'videcam-models/new', name: 'videcams.videcam-models.new', component: page('cams/videcam-models/new.vue')},
      { path: 'videcam-models/:id/edit', name: 'videcams.videcam-models.edit', component: page('cams/videcam-models/edit.vue')}
    ] },

  { path: '/messenger',
    component: page('messenger/index.vue'),
    children: [
      { path: '', redirect: { name: 'messenger.messages' } },

      { path: 'messages', name: 'messenger.messages', component: page('messenger/messages.vue')},
      { path: 'messages/new', name: 'messenger.messages.new', component: page('messenger/messages/new.vue')},
      { path: 'messages/:id/edit', name: 'messenger.messages.edit', component: page('messenger/messages/edit.vue')},

      { path: 'message-threads', name: 'messenger.message-threads', component: page('messenger/message-threads.vue')},
      { path: 'message-threads/new', name: 'messenger.message-threads.new', component: page('messenger/message-threads/new.vue')},
      { path: 'message-threads/:id/edit', name: 'messenger.message-threads.edit', component: page('messenger/message-threads/edit.vue')},

      { path: 'db-object-types-message-threads', name: 'messenger.db-object-types-message-threads', component: page('messenger/db-object-types-message-threads.vue')},
      { path: 'db-object-types-message-threads/new', name: 'messenger.db-object-types-message-threads.new', component: page('messenger/db-object-types-message-threads/new.vue')},
      { path: 'db-object-types-message-threads/:id/edit', name: 'messenger.db-object-types-message-threads.edit', component: page('messenger/db-object-types-message-threads/edit.vue')},

      { path: 'attachments', name: 'messenger.attachments', component: page('messenger/attachments.vue')},
      { path: 'attachments/new', name: 'messenger.attachments.new', component: page('messenger/attachments/new.vue')},
      { path: 'attachments/:id/edit', name: 'messenger.attachments.edit', component: page('messenger/attachments/edit.vue')}
    ] },

  { path: '/processing-modules',
    component: page('processing-modules/index.vue'),
    children: [
      { path: '', redirect: { name: 'processing-modules.modules' } },

      { path: 'modules', name: 'processing-modules.modules', component: page('processing-modules/modules.vue')},
      { path: 'modules/new', name: 'processing-modules.modules.new', component: page('processing-modules/modules/new.vue')},
      { path: 'modules/:id/edit', name: 'processing-modules.modules.edit', component: page('processing-modules/modules/edit.vue')},

      { path: 'db-object-type-modules', name: 'processing-modules.db-object-type-modules', component: page('processing-modules/db-object-type-modules.vue')},
      { path: 'db-object-type-modules/new', name: 'processing-modules.db-object-type-modules.new', component: page('processing-modules/db-object-type-modules/new.vue')},
      { path: 'db-object-type-modules/:id/edit', name: 'processing-modules.db-object-type-modules.edit', component: page('processing-modules/db-object-type-modules/edit.vue')}
    ] },

  { path: '/db-object-types', name: 'db-object-types', component: page('db-object-types.vue') },
  { path: '/db-object-types/new', name: 'db-object-types.new', component: page('db-object-types/new.vue') },
  { path: '/db-object-types/:id/edit', name: 'db-object-types.edit', component: page('db-object-types/edit.vue') },

  { path: '/s3-endpoints', name: 's3-endpoints', component: page('s3-endpoints.vue') },
  { path: '/s3-endpoints/new', name: 's3-endpoints.new', component: page('s3-endpoints/new.vue') },
  { path: '/s3-endpoints/:id/edit', name: 's3-endpoints.edit', component: page('s3-endpoints/edit.vue') },

  { path: '*', component: page('errors/404.vue') }
]
