declare var ROOT: import('vue/types/vue').CombinedVueInstance<Vue, object, object, object, Record<never, any>>

declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module 'vue-highlight-words'

declare module 'blueimp-gallery/js/blueimp-gallery.js'

declare module 'vform' {
  interface ErrorsDictionary {
    [key: string]: string
  }

  interface ErrorsHelpers {
    /**
     * Get all the errors.
     *
     * @return {Object}
     */
    all(): {}
    /**
     * Determine if there is an error for the given field.
     *
     * @param  {String} field
     * @return {Boolean}
     */
    has(field: string): boolean
    /**
     * Determine if there are any errors for the given fields.
     *
     * @param  {...String} fields
     * @return {Boolean}
     */
    hasAny(...args: string[]): boolean
    /**
     * Determine if there are any errors.
     *
     * @return {Boolean}
     */
    any(): boolean
    /**
     * Get the first error message for the given field.
     *
     * @param  {String} field
     * @return {String|undefined}
     */
    get(field: string): string
    /**
     * Get all the error messages for the given field.
     *
     * @param  {String} field
     * @return {Array}
     */
    getAll(field: string): string[]
    /**
     * Get the error message for the given fields.
     *
     * @param  {...String} fields
     * @return {Array}
     */
    only(...args: string[]): string[]
    /**
     * Get all the errors in a flat array.
     *
     * @return {Array}
     */
    flatten(): string[]
    /**
     * Clear one or all error fields.
     *
     * @param {String|undefined} field
     */
    clear(field?: string): void
    /**
     * Set the errors object.
     *
     * @param {Object}
     *
     * Set a specified error message.
     *
     * @param {String}
     * @param {String}
     */
    set(fieldOrErrors: string | object, message?: string): void
  }

  export default class Form {
    /**
     * Indicates if the form is sent to the server.
     *
     * @var {Boolean}
     */
    busy: boolean
    /**
     * Indicates if the response form the server was successful.
     *
     * @var {Boolean}
     */
    successful: boolean
    /**
     * Create a new form instance.
     *
     * @param {Object} data
     */
    constructor(data?: object)
    /**
     * Submit the from via a POST|PATCH|PUT|DELETE|GET request.
     *
     * @param  {String} url
     * @return {Promise}
     */
    post<T>(url: string, options?: any): Promise<T>
    patch<T>(url: string, options?: any): Promise<T>
    put<T>(url: string, options?: any): Promise<T>
    delete<T>(url: string, options?: any): Promise<T>
    get<T>(url: string, options?: any): Promise<T>
    /**
     * Clear the form errors.
     */
    clear(): void
    /**
     * Reset the form fields.
     */
    reset(): void
    /**
     * Fill form data.
     *
     * @param {Object} data
     */
    fill(data: object): void
    errors: ErrorsDictionary & ErrorsHelpers
  }

  const Errors: any
  const HasError: any
  const AlertError: any
  const AlertErrors: any
  const AlertSuccess: any
  export {
    Errors,
    HasError,
    AlertError,
    AlertErrors,
    AlertSuccess
  }
}

declare module 'v-mask' {
  const value: any
  export default value
}

declare module 'vue-mobile-detection'

declare module 'v-clipboard'
