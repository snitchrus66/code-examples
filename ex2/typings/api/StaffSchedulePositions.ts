/* eslint-disable @typescript-eslint/no-empty-interface */
export type StaffScheduleDepartmentPosition = {
  department_id: number | null
  department_name: string | null
}

export type StaffSchedultEmployeePositionPosition = {
  empl_position_id: number | null
  empl_position_name: string | null
  quantity: number | null
}

export type StaffSchedulePosition =
  & {
    id: number | null
    created_at: string | null
    description: string | null
    parent_id: number | null
    signed: 0 | 1
    staff_schedule_id: number | null
    supervisor_id: number | null
  }
  & StaffScheduleDepartmentPosition
  & StaffSchedultEmployeePositionPosition

/**
 * @method GET
 * @url /api/v1/staff-schedule-positions
 * @description Получение списка позиций штаных расписаний
 */
export interface StaffSchedulePositionsListRequest {
  direction?: 'asc' | 'desc'
  staff_schedule_id?: number
}

/**
 * @method GET
 * @url /api/v1/staff-schedule-positions
 * @description Получение списка позиций штаных расписаний
 */
export interface StaffSchedulePositionsListResponse {
  data: StaffSchedulePosition[]
}

/**
 * @method POST
 * @url /api/v1/staff-schedule-positions
 * @description Создание
 */
export interface StaffSchedulePositionCreateRequest {}

/**
 * @method POST
 * @url /api/v1/staff-schedule-positions
 * @description Создание
 */
export interface StaffSchedulePositionCreateResponse {}

/**
 * @method GET
 * @url /api/v1/staff-schedule-positions/{id}
 * @description Запрос одной позиции штаного расписания
 */
export interface StaffSchedulePositionReadRequest {
  id: number
}

/**
 * @method GET
 * @url /api/v1/staff-schedule-positions/{id}
 * @description Запрос одной позиции штаного расписания
 */
export interface StaffSchedulePositionReadResponse {
  data: StaffSchedulePosition
}

/**
 * @method DELETE
 * @url /api/v1/staff-schedule-positions/{id}
 * @description Удаление
 */
export interface StaffSchedulePositionDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/staff-schedule-positions/{id}
 * @description Удаление
 */
export interface StaffSchedulePositionDeleteResponse {

}

/**
 * @method PUT
 * @url /api/v1/staff-schedule-positions/{id}
 * @description Изменение
 */
export interface StaffSchedulePositionUpdateRequest {
  id: number
}

/**
 * @method PUT
 * @url /api/v1/staff-schedule-positions/{id}
 * @description Изменение
 */
export interface StaffSchedulePositionUpdateResponse {

}
