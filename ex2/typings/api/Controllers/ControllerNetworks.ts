/* eslint-disable @typescript-eslint/no-empty-interface */
export type СontrollerNetwork = {
  id: number
  metric_controller_id: number
  network_type_id: number
  topic_name: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/metrics/conroller-networks
  * @description Получение списка сетей контроллеров
  */
export interface СontrollerNetworksListRequest {

}

/**
  * @method GET
  * @url /api/v1/metrics/conroller-networks
  * @description Получение списка сетей контроллеров
  */
export interface СontrollerNetworksListResponse {
  data: СontrollerNetwork[]
}
  
/**
  * @method POST
  * @url /api/v1/metrics/conroller-networks
  * @description Создание сети контроллеров
  */
export interface СontrollerNetworkCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/metrics/conroller-networks
  * @description Создание сети контроллеров
  */
export interface СontrollerNetworkCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/metrics/conroller-networks/{id}
  * @description Запрос одной сети контроллеров
  */
export interface СontrollerNetworkReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/metrics/conroller-networks/{id}
  * @description Запрос одной сети контроллеров
  */
export interface СontrollerNetworkReadResponse {
  data: СontrollerNetwork
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/conroller-networks/{id}
  * @description Удаление сети контроллеров
  */
export interface СontrollerNetworkDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/conroller-networks/{id}
  * @description Удаление сети контроллеров
  */
export interface СontrollerNetworkDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/conroller-networks/{id}
  * @description Изменение сети контроллеров
  */
export interface СontrollerNetworkUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/conroller-networks/{id}
  * @description Изменение сети контроллеров
  */
export interface СontrollerNetworkUpdateResponse {
  
}
  