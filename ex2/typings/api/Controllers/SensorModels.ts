/* eslint-disable @typescript-eslint/no-empty-interface */
export type SensorModel = {
  id: number
  name: string
  description: string | null
  data_transfer_protocol_id: number
}
  
/**
  * @method GET
  * @url /api/v1/metric-sensor-models
  * @description Получение списка моделей датчиков метрик
  */
export interface SensorModelsListRequest {

}

/**
  * @method GET
  * @url /api/v1/metric-sensor-models
  * @description Получение списка моделей датчиков метрик
  */
export interface SensorModelsListResponse {
  data: SensorModel[]
}
  
/**
  * @method POST
  * @url /api/v1/metric-sensor-models
  * @description Создание модели датчика метрик
  */
export interface SensorModelCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/metric-sensor-models
  * @description Создание модели датчика метрик
  */
export interface SensorModelCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/metric-sensor-models/{id}
  * @description Запрос одной модели датчика метрик
  */
export interface SensorModelReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/metric-sensor-models/{id}
  * @description Запрос одной модели датчика метрик
  */
export interface SensorModelReadResponse {
  data: SensorModel
}
  
/**
  * @method DELETE
  * @url /api/v1/metric-sensor-models/{id}
  * @description Удаление модели датчика метрик
  */
export interface SensorModelDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/metric-sensor-models/{id}
  * @description Удаление модели датчика метрик
  */
export interface SensorModelDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/metric-sensor-models/{id}
  * @description Изменение модели датчика метрик
  */
export interface SensorModelUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/metric-sensor-models/{id}
  * @description Изменение модели датчика метрик
  */
export interface SensorModelUpdateResponse {
  
}
  