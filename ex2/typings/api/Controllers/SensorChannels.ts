/* eslint-disable @typescript-eslint/no-empty-interface */
import * as api from '~/typings/api'

export type SensorChannel = {
  id: number
  rs485_device_id: number
  unit_measurement_id: number
  topic_name: string
  status_flag: number
  ts_database_id: number
  min_value: number
  max_value: number
  min_warning_value: number
  max_warning_value: number
  min_alert_value: number
  max_alert_value: number
  description: string | null
  created_at: string
  updated_at: string
  deleted_at: string | null
  map_object_id: number
  object_name: string
  type_name: string
  type_id: number
  map_object_address: string
  metric_controller_id: number
  unit_measurement_name: string
}

/**
  * @method GET
  * @url /api/v1/metric-sensor-channels
  * @description Получение списка каналов датчиков метрик
  */
export interface SensorChannelsListRequest extends api.ListOptions {
  objects_type?: number[]
  objects_id?: number[]
}

/**
  * @method GET
  * @url /api/v1/metric-sensor-channels
  * @description Получение списка каналов датчиков метрик
  */
export interface SensorChannelsListResponse extends api.ListOfItemsResponse {
  data: SensorChannel[]
}

/**
  * @method POST
  * @url /api/v1/metric-sensor-channels
  * @description Создание канала датчика метрик
  */
export interface SensorChannelCreateRequest {}

/**
  * @method POST
  * @url /api/v1/metric-sensor-channels
  * @description Создание канала датчика метрик
  */
export interface SensorChannelCreateResponse {}

/**
  * @method GET
  * @url /api/v1/metric-sensor-channels/{id}
  * @description Запрос одного канала датчика метрик
  */
export interface SensorChannelReadRequest {
  id: number
}

/**
  * @method GET
  * @url /api/v1/metric-sensor-channels/{id}
  * @description Запрос одного канала датчика метрик
  */
export interface SensorChannelReadResponse {
  data: SensorChannel
}

/**
  * @method DELETE
  * @url /api/v1/metric-sensor-channels/{id}
  * @description Удаление канала датчика метрик
  */
export interface SensorChannelDeleteRequest {
  id: number
}

/**
  * @method DELETE
  * @url /api/v1/metric-sensor-channels/{id}
  * @description Удаление канала датчика метрик
  */
export interface SensorChannelDeleteResponse {

}

/**
  * @method PATCH
  * @url /api/v1/metric-sensor-channels/{id}
  * @description Изменение канала датчика метрик
  */
export interface SensorChannelUpdateRequest {
  id: number
}

/**
  * @method PATCH
  * @url /api/v1/metric-sensor-channels/{id}
  * @description Изменение канала датчика метрик
  */
export interface SensorChannelUpdateResponse {

}
