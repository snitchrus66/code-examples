/* eslint-disable @typescript-eslint/no-empty-interface */
export type NetworkType = {
  id: number
  name: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/network-types
  * @description Получение списка сетей передачи данных
  */
export interface NetworkTypesListRequest {

}

/**
  * @method GET
  * @url /api/v1/network-types
  * @description Получение списка сетей передачи данных
  */
export interface NetworkTypesListResponse {
  data: NetworkType[]
}
  
/**
  * @method POST
  * @url /api/v1/network-types
  * @description Создание типа сети передачи данных
  */
export interface NetworkTypeCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/network-types
  * @description Создание типа сети передачи данных
  */
export interface NetworkTypeCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/network-types/{id}
  * @description Запрос одного типа сети передачи данных
  */
export interface NetworkTypeReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/network-types/{id}
  * @description Запрос одного типа сети передачи данных
  */
export interface NetworkTypeReadResponse {
  data: NetworkType
}
  
/**
  * @method DELETE
  * @url /api/v1/network-types/{id}
  * @description Удаление типа сети передачи данных
  */
export interface NetworkTypeDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/network-types/{id}
  * @description Удаление типа сети передачи данных
  */
export interface NetworkTypeDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/network-types/{id}
  * @description Изменение типа сети передачи данных
  */
export interface NetworkTypeUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/network-types/{id}
  * @description Изменение типа сети передачи данных
  */
export interface NetworkTypeUpdateResponse {
  
}
  