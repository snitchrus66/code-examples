/* eslint-disable @typescript-eslint/no-empty-interface */
export type Сontroller = {
  id: number
  object_id: number
  metric_contr_model_id: number
  mqtt_server_id: number
  serial_number: string
  ip_address: string | null
  fqdn_name: string | null
  login: string | null
  password: string | null
  http_port: string | null
  ssh_port: string | null
  rsa_key: string | null
  status_flag: number
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/metrics/controllers
  * @description Получение списка контроллеров метрик
  */
export interface ControllersListRequest {

}

/**
  * @method GET
  * @url /api/v1/metrics/controllers
  * @description Получение списка контроллеров метрик
  */
export interface ControllersListResponse {
  data: Сontroller[]
}
  
/**
  * @method POST
  * @url /api/v1/metrics/controllers
  * @description Создание контроллера метрик
  */
export interface ControllerCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/metrics/controllers
  * @description Создание контроллера метрик
  */
export interface ControllerCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/metrics/controllers/{id}
  * @description Запрос одного контроллера метрик
  */
export interface ControllerReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/metrics/controllers/{id}
  * @description Запрос одного контроллера метрик
  */
export interface ControllerReadResponse {
  data: Сontroller
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/controllers/{id}
  * @description Удаление контроллера метрик
  */
export interface ControllerDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/controllers/{id}
  * @description Удаление контроллера метрик
  */
export interface ControllerDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/controllers/{id}
  * @description Изменение контроллера метрик
  */
export interface ControllerUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/controllers/{id}
  * @description Изменение контроллера метрик
  */
export interface ControllerUpdateResponse {
  
}
  