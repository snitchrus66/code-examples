/* eslint-disable @typescript-eslint/no-empty-interface */
export type Device = {
  id: number
  controller_network_id: number
  serial_number: string
  rs485_device_model_id: number
  rs485_address: string
  topic_name: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/rs485-devices
  * @description Получение списка устройств
  */
export interface DevicesListRequest {

}

/**
  * @method GET
  * @url /api/v1/rs485-devices
  * @description Получение списка устройств
  */
export interface DevicesListResponse {
  data: Device[]
}
  
/**
  * @method POST
  * @url /api/v1/rs485-devices
  * @description Создание устройства
  */
export interface DeviceCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/rs485-devices
  * @description Создание устройства
  */
export interface DeviceCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/rs485-devices/{id}
  * @description Запрос одного устройства
  */
export interface DeviceReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/rs485-devices/{id}
  * @description Запрос одного устройства
  */
export interface DeviceReadResponse {
  data: Device
}
  
/**
  * @method DELETE
  * @url /api/v1/rs485-devices/{id}
  * @description Удаление устройства
  */
export interface DeviceDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/rs485-devices/{id}
  * @description Удаление устройства
  */
export interface DeviceDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/rs485-devices/{id}
  * @description Изменение устройства
  */
export interface DeviceUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/rs485-devices/{id}
  * @description Изменение устройства
  */
export interface DeviceUpdateResponse {
  
}
  