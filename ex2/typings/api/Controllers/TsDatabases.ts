/* eslint-disable @typescript-eslint/no-empty-interface */
export type TsDatabase = {
  id: number
  name: string
  ts_db_server_id: number
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/metrics/ts-databases
  * @description Получение списка Time Serial баз данных
  */
export interface TsDatabasesListRequest {

}

/**
  * @method GET
  * @url /api/v1/metrics/ts-databases
  * @description Получение списка Time Serial баз данных
  */
export interface TsDatabasesListResponse {
  data: TsDatabase[]
}
  
/**
  * @method POST
  * @url /api/v1/metrics/ts-databases
  * @description Создание Time Serial базы данных
  */
export interface TsDatabaseCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/metrics/ts-databases
  * @description Создание Time Serial базы данных
  */
export interface TsDatabaseCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/metrics/ts-databases/{id}
  * @description Запрос одной Time Serial базы данных
  */
export interface TsDatabaseReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/metrics/ts-databases/{id}
  * @description Запрос одной Time Serial базы данных
  */
export interface TsDatabaseReadResponse {
  data: TsDatabase
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/ts-databases/{id}
  * @description Удаление Time Serial базы данных
  */
export interface TsDatabaseDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/ts-databases/{id}
  * @description Удаление Time Serial базы данных
  */
export interface TsDatabaseDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/ts-databases/{id}
  * @description Изменение Time Serial базы данных
  */
export interface TsDatabaseUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/ts-databases/{id}
  * @description Изменение Time Serial базы данных
  */
export interface TsDatabaseUpdateResponse {
  
}
  