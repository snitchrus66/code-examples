/* eslint-disable @typescript-eslint/no-empty-interface */
export type MqttServer = {
  id: number
  ip_address: string
  login: string | null
  password: string | null
  ssl_certificate: string | null
  web_socket_address: string | null
  http_url: string | null
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/rs485-MqttServers
  * @description Получение mqqt серверов
  */
export interface MqttServersListRequest {

}

/**
  * @method GET
  * @url /api/v1/rs485-MqttServers
  * @description Получение mqqt серверов
  */
export interface MqttServersListResponse {
  data: MqttServer[]
}
  
/**
  * @method POST
  * @url /api/v1/rs485-MqttServers
  * @description Создание mqtt сервера
  */
export interface MqttServerCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/rs485-MqttServers
  * @description Создание mqtt сервера
  */
export interface MqttServerCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/rs485-MqttServers/{id}
  * @description Запрос одного mqtt сервера
  */
export interface MqttServerReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/rs485-MqttServers/{id}
  * @description Запрос одного mqtt сервера
  */
export interface MqttServerReadResponse {
  data: MqttServer
}
  
/**
  * @method DELETE
  * @url /api/v1/rs485-MqttServers/{id}
  * @description Удаление mqtt сервера
  */
export interface MqttServerDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/rs485-MqttServers/{id}
  * @description Удаление mqtt сервера
  */
export interface MqttServerDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/rs485-MqttServers/{id}
  * @description Изменение mqtt сервера
  */
export interface MqttServerUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/rs485-MqttServers/{id}
  * @description Изменение mqtt сервера
  */
export interface MqttServerUpdateResponse {
  
}
  