/* eslint-disable @typescript-eslint/no-empty-interface */
export type СontrollerModel = {
  id: number
  name: string
  topic_name: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/metric-controller-models
  * @description Получение списка моделей контроллеров метрик
  */
export interface ControllerModelsListRequest {

}

/**
  * @method GET
  * @url /api/v1/metric-controller-models
  * @description Получение списка моделей контроллеров метрик
  */
export interface ControllerModelsListResponse {
  data: СontrollerModel[]
}
  
/**
  * @method POST
  * @url /api/v1/metric-controller-models
  * @description Создание модели контроллера метрик
  */
export interface ControllerModelCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/metric-controller-models
  * @description Создание модели контроллера метрик
  */
export interface ControllerModelCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/metric-controller-models/{id}
  * @description Запрос одного модели контроллера метрик
  */
export interface ControllerModelReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/metric-controller-models/{id}
  * @description Запрос одного модели контроллера метрик
  */
export interface ControllerModelReadResponse {
  data: СontrollerModel
}
  
/**
  * @method DELETE
  * @url /api/v1/metric-controller-models/{id}
  * @description Удаление модели контроллера метрик
  */
export interface ControllerModelDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/metric-controller-models/{id}
  * @description Удаление модели контроллера метрик
  */
export interface ControllerModelDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/metric-controller-models/{id}
  * @description Изменение модели контроллера метрик
  */
export interface ControllerModelUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/metric-controller-models/{id}
  * @description Изменение модели контроллера метрик
  */
export interface ControllerModelUpdateResponse {
  
}
  