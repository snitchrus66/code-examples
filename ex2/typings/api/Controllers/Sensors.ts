/* eslint-disable @typescript-eslint/no-empty-interface */
export type Sensor = {
  id: number
  metric_sensor_channel_id: number
  metric_sensor_model_id: number
  serial_number: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/metrics/sensors
  * @description Получение списка датчиков
  */
export interface SensorsListRequest {

}

/**
  * @method GET
  * @url /api/v1/metrics/sensors
  * @description Получение списка датчиков
  */
export interface SensorsListResponse {
  data: Sensor[]
}
  
/**
  * @method POST
  * @url /api/v1/metrics/sensors
  * @description Создание датчика
  */
export interface SensorCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/metrics/sensors
  * @description Создание датчика
  */
export interface SensorCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/metrics/sensors/{id}
  * @description Запрос одного датчика
  */
export interface SensorReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/metrics/sensors/{id}
  * @description Запрос одного датчика
  */
export interface SensorReadResponse {
  data: Sensor
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/sensors/{id}
  * @description Удаление датчика
  */
export interface SensorDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/sensors/{id}
  * @description Удаление датчика
  */
export interface SensorDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/sensors/{id}
  * @description Изменение датчика
  */
export interface SensorUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/sensors/{id}
  * @description Изменение датчика
  */
export interface SensorUpdateResponse {
  
}
  