/* eslint-disable @typescript-eslint/no-empty-interface */
export type TsDbServer = {
  id: number
  name: string
  host: string 
  port: number
  http_api_port: number
  login: string
  password: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/metrics/ts-db-servers
  * @description Получение списка Time Serial БД серверов
  */
export interface TsDbServersListRequest {

}

/**
  * @method GET
  * @url /api/v1/metrics/ts-db-servers
  * @description Получение списка Time Serial БД серверов
  */
export interface TsDbServersListResponse {
  data: TsDbServer[]
}
  
/**
  * @method POST
  * @url /api/v1/metrics/ts-db-servers
  * @description Создание Time Serial БД сервера
  */
export interface TsDbServerCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/metrics/ts-db-servers
  * @description Создание Time Serial БД сервера
  */
export interface TsDbServerCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/metrics/ts-db-servers/{id}
  * @description Запрос одного Time Serial БД сервера
  */
export interface TsDbServerReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/metrics/ts-db-servers/{id}
  * @description Запрос одного Time Serial БД сервера
  */
export interface TsDbServerReadResponse {
  data: TsDbServer
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/ts-db-servers/{id}
  * @description Удаление Time Serial БД сервера
  */
export interface TsDbServerDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/ts-db-servers/{id}
  * @description Удаление Time Serial БД сервера
  */
export interface TsDbServerDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/ts-db-servers/{id}
  * @description Изменение Time Serial БД сервера
  */
export interface TsDbServerUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/ts-db-servers/{id}
  * @description Изменение Time Serial БД сервера
  */
export interface TsDbServerUpdateResponse {
  
}
  