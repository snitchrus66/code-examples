/* eslint-disable @typescript-eslint/no-empty-interface */
export type UnitMeasurements = {
  id: number
  name: string
  unit_measurement: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/unit-measurements
  * @description Получение списка типов параметров метрик
  */
export interface UnitMeasurementsListRequest {

}

/**
  * @method GET
  * @url /api/v1/unit-measurements
  * @description Получение списка типов параметров метрик
  */
export interface UnitMeasurementsListResponse {
  data: UnitMeasurements[]
}
  
/**
  * @method POST
  * @url /api/v1/unit-measurements
  * @description Создание типа параметра метрик
  */
export interface UnitMeasurementCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/unit-measurements
  * @description Создание типа параметра метрик
  */
export interface UnitMeasurementCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/unit-measurements/{id}
  * @description Запрос одного типа параметра метрик
  */
export interface UnitMeasurementReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/unit-measurements/{id}
  * @description Запрос одного типа параметра метрик
  */
export interface UnitMeasurementReadResponse {
  data: UnitMeasurements
}
  
/**
  * @method DELETE
  * @url /api/v1/unit-measurements/{id}
  * @description Удаление типа параметра метрик
  */
export interface UnitMeasurementDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/unit-measurements/{id}
  * @description Удаление типа параметра метрик
  */
export interface UnitMeasurementDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/unit-measurements/{id}
  * @description Изменение типа параметра метрик
  */
export interface UnitMeasurementUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/unit-measurements/{id}
  * @description Изменение типа параметра метрик
  */
export interface UnitMeasurementsUpdateResponse {
  
}
  