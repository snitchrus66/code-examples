/* eslint-disable @typescript-eslint/no-empty-interface */
export type TransferProtocol = {
  id: number
  name: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/metrics/data-transfer-protocols
  * @description Получение списка протоколов передачи данных
  */
export interface TransferProtocolsListRequest {

}

/**
  * @method GET
  * @url /api/v1/metrics/data-transfer-protocols
  * @description Получение списка протоколов передачи данных
  */
export interface TransferProtocolsListResponse {
  data: TransferProtocol[]
}
  
/**
  * @method POST
  * @url /api/v1/metrics/data-transfer-protocols
  * @description Создание протокола передачи данных
  */
export interface TransferProtocolCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/metrics/data-transfer-protocols
  * @description Создание протокола передачи данных
  */
export interface TransferProtocolCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/metrics/data-transfer-protocols/{id}
  * @description Запрос одного протокола передачи данных
  */
export interface TransferProtocolReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/metrics/data-transfer-protocols/{id}
  * @description Запрос одного протокола передачи данных
  */
export interface TransferProtocolReadResponse {
  data: TransferProtocol
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/data-transfer-protocols/{id}
  * @description Удаление протокола передачи данных
  */
export interface TransferProtocolDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/data-transfer-protocols/{id}
  * @description Удаление протокола передачи данных
  */
export interface TransferProtocolDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/data-transfer-protocols/{id}
  * @description Изменение протокола передачи данных
  */
export interface TransferProtocolUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/data-transfer-protocols/{id}
  * @description Изменение протокола передачи данных
  */
export interface TransferProtocolUpdateResponse {
  
}
  