/* eslint-disable @typescript-eslint/no-empty-interface */
export type DeviceModel = {
  id: number
  name: string
  number_of_channels: number
  topic_name: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/metrics/rs485-dev-models
  * @description Получение списка моделей модулей контроллеров
  */
export interface DeviceModelsListRequest {

}

/**
  * @method GET
  * @url /api/v1/metrics/rs485-dev-models
  * @description Получение списка моделей модулей контроллеров
  */
export interface DeviceModelsListResponse {
  data: DeviceModel[]
}
  
/**
  * @method POST
  * @url /api/v1/metrics/rs485-dev-models
  * @description Создание модели модуля контроллера
  */
export interface DeviceModelCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/metrics/rs485-dev-models
  * @description Создание модели модуля контроллера
  */
export interface DeviceModelCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/metrics/rs485-dev-models/{id}
  * @description Запрос одного модели модуля контроллера
  */
export interface DeviceModelReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/metrics/rs485-dev-models/{id}
  * @description Запрос одного модели модуля контроллера
  */
export interface DeviceModelReadResponse {
  data: DeviceModel
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/rs485-dev-models/{id}
  * @description Удаление модели модуля контроллера
  */
export interface DeviceModelDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/metrics/rs485-dev-models/{id}
  * @description Удаление модели модуля контроллера
  */
export interface DeviceModelDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/rs485-dev-models/{id}
  * @description Изменение модели модуля контроллера
  */
export interface DeviceModelUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/metrics/rs485-dev-models/{id}
  * @description Изменение модели модуля контроллера
  */
export interface DeviceModelUpdateResponse {
  
}
  