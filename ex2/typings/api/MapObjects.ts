/* eslint-disable @typescript-eslint/no-empty-interface */
import * as api from '~/typings/api'

export type MapObject = {
  id: number
  type: number | null
  name: string
  description: string
  address: string
  latitude: string
  longitude: string
  staff_sched_position_id: number | null
  department_name: string
}

/**
 * @method GET
 * @url /api/v1/map-objects
 * @description Получение списка объектов
 */
export interface MapObjectsListRequest extends api.ListOptions {
  items?: 'all'
  types?: number[] // ids of map object types
}

/**
 * @method GET
 * @url /api/v1/map-objects
 * @description Получение списка объектов
 */
export interface MapObjectsListResponse extends api.ListOfItemsResponse {
  data: MapObject[]
}

/**
 * @method POST
 * @url /api/v1/map-objects
 * @description Добавление
 */
export interface MapObjectCreateRequest {}

/**
 * @method POST
 * @url /api/v1/map-objects
 * @description Добавление
 */
export interface MapObjectCreateResponse {}

/**
 * @method GET
 * @url /api/v1/map-objects/{id}
 * @description Запрос одного объекта
 */
export interface MapObjectReadRequest {
  id: number
}

/**
 * @method GET
 * @url /api/v1/map-objects/{id}
 * @description Запрос одного объекта
 */
export interface MapObjectReadResponse {
  data: MapObject
}

/**
 * @method DELETE
 * @url /api/v1/map-objects/{id}
 * @description Удаление
 */
export interface MapObjectDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/map-objects/{id}
 * @description Удаление
 */
export interface MapObjectDeleteResponse {

}

/**
 * @method PATCH
 * @url /api/v1/map-objects/{id}
 * @description Изменение
 */
export interface MapObjectUpdateRequest {

}

/**
 * @method PATCH
 * @url /api/v1/map-objects/{id}
 * @description Изменение
 */
export interface MapObjectUpdateResponse {

}
