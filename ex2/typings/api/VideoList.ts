/* eslint-disable @typescript-eslint/no-empty-interface */
export type VideoListItem = {
  file: string
  date: string
}

/**
 * @method GET
 * @url /api/v1/video-list/{cameraId}
 * @description Получение списка видео с камеры
 */
export interface VideoListRequest {
  cameraId: number
  items?: 'all'
  page?: number
  start_date: string
  end_date: string
}

/**
 * @method GET
 * @url /api/v1/video-list/{id}
 * @description Получение списка видео с камеры
 */
export interface VideoListResponse {
  current_page: number
  data: VideoListItem[]
  first_page_url: string
  from: number
  last_page: number
  last_page_url: string
  next_page_url: string
  path: string
  per_page: string
  prev_page_url: string | null
  to: number
  total: number
}
