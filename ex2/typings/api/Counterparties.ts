/* eslint-disable @typescript-eslint/no-empty-interface */
export type CounterpartyType =
  | 'company'
  | 'businessman'
  | 'self_busy'
  | 'person'

export type Counterparty = {
  id: number
  counterparty_id: number
  type: CounterpartyType
  country: string
  rus_name: string
  abbreviation: string
  en_name: string
  inn: string
  ogrn: string
  kpp: string
  legal_address: string
  actual_address: string
  bank_name: string
  bic: string
  correspondent_account: string
  payment_account: string
  contacts: string
  notes: string
  person_name: string
  person_tel: string
  person_email: string
  created_at: string
  updated_at: string
}

/**
 * @method GET
 * @url /api/v1/counterparties/types
 * @description Получение возможных типов контрагентов
 */
export interface CounterpartyTypesListResponse {
  data: {
    [key in CounterpartyType]: string
  }
}

/**
 * @method GET
 * @url /api/v1/counterparties/
 * @description Получение списка контрагентов
 */
export interface CounterpartiesListRequest {
  search?: string
  orderby?: string
  direction?: 'asc' | 'desc'
  page?: number
}

/**
 * @method GET
 * @url /api/v1/counterparties/
 * @description Получение списка контрагентов
 */
export interface CounterpartiesListResponse {
  current_page: number
  data: Pick<
  Counterparty,
  | 'id'
  | 'type'
  | 'inn'
  | 'rus_name'
  | 'abbreviation'
  | 'en_name'
  | 'person_name'
  | 'actual_address'
  >[]
  first_page_url: string
  from: number
  last_page: number
  last_page_url: string
  next_page_url: string | null
  path: string
  per_page: string
  prev_page_url: string | null
  to: number
  total: number
}

/**
 * @method GET
 * @url /api/v1/counterparties/{id}
 * @description Получение информации о контрагенте
 */
export interface CounterpartyReadRequest {
  id: number
  on_date?: string
}

/**
 * @method GET
 * @url /api/v1/counterparties/{id}
 * @description Получение информации о контрагенте
 */
export interface CounterpartyReadResponse {
  data: Counterparty
}

/**
 * @method PUT
 * @url /api/v1/counterparties/{id}
 * @description Изменение данных контрагента
 */
export interface CounterpartyUpdateRequest {}

/**
 * @method PUT
 * @url /api/v1/counterparties/{id}
 * @description Изменение данных контрагента
 */
export interface CounterpartyUpdateResponse {}

/**
 * @method DELETE
 * @url /api/v1/counterparties/{id}
 * @description Удаление контрагента по id
 */
export interface CounterpartyDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/counterparties/{id}
 * @description Удаление контрагента по id
 */
export interface CounterpartyDeleteResponse {
  data: {
    status: 'success'
  }
}

/**
 * @method GET
 * @url /api/v1/counterparties/{id}/history
 * @description Получение информации о контрагенте из истории
 */
export interface CounterpartyHistoryReadRequest {
  id: number
}

/**
 * @method GET
 * @url /api/v1/counterparties/{id}/history
 * @description Получение информации о контрагенте из истории
 */
export interface CounterpartyHistoryReadResponse {
  current_page: number
  data: Counterparty[]
  first_page_url: string
  from: number
  last_page: number
  last_page_url: string
  next_page_url: string | null
  path: string
  per_page: string
  prev_page_url: string | null
  to: number
  total: number
}


/**
 * @method POST
 * @url /api/v1/counterparties
 * @description Создание контрагента
 */
export interface CounterpartyCreateRequest {}

/**
 * @method POST
 * @url /api/v1/counterparties
 * @description Создание контрагента
 */
export interface CounterpartyCreateResponse {}
