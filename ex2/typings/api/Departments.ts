/* eslint-disable @typescript-eslint/no-empty-interface */
export type Department = {
  id: number
  name: string
  description: string
  created_at: string
  updated_at: string
}

/**
 * @method GET
 * @url /api/v1/departments
 * @description Получение списка отделов
 */
export interface DepartmentsListRequest {

}

/**
 * @method GET
 * @url /api/v1/departments
 * @description Получение списка отделов
 */
export interface DepartmentsListResponse {
  data: Department[]
}

/**
 * @method POST
 * @url /api/v1/departments
 * @description Создание
 */
export interface DepartmentCreateRequest {}

/**
 * @method POST
 * @url /api/v1/departments
 * @description Создание
 */
export interface DepartmentCreateResponse {}

/**
 * @method GET
 * @url /api/v1/departments/{id}
 * @description Запрос одного отдела
 */
export interface DepartmentReadRequest {
  id: number
}

/**
 * @method GET
 * @url /api/v1/departments/{id}
 * @description Запрос одного отдела
 */
export interface DepartmentReadResponse {
  data: Department
}

/**
 * @method DELETE
 * @url /api/v1/departments/{id}
 * @description Удаление
 */
export interface DepartmentDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/departments/{id}
 * @description Удаление
 */
export interface DepartmentDeleteResponse {

}

/**
 * @method PUT
 * @url /api/v1/departments/{id}
 * @description Изменение
 */
export interface DepartmentUpdateRequest {
  id: number
}

/**
 * @method PUT
 * @url /api/v1/departments/{id}
 * @description Изменение
 */
export interface DepartmentUpdateResponse {

}
