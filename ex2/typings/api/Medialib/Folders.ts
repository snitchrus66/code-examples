/* eslint-disable @typescript-eslint/no-empty-interface */
import * as api from '~/typings/api'

export type Folder = {
  description: string | null
  name: string
  parent_id: null
  id: number | null
}

/**
 * @method POST
 * @url /api/v1/medialib-module/folders/create-folder
 * @description Создание
 */
export interface FolderCreateRequest {
  entity: api.Medialib.Entities
  entity_id: number
  group: api.Medialib.Groups
  name: string
  description: string
  parent_id: number | null
}

/**
 * @method POST
 * @url /api/v1/medialib-module/folders/create-folder
 * @description Создание
 */
export interface FolderCreateResponse {}

/**
 * @method DELETE
 * @url /api/v1/medialib-module/folders/{id}/soft-delete-folder
 * @description Удаление
 */
export interface FolderDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/medialib-module/folders/{id}/soft-delete-folder
 * @description Удаление
 */
export interface FolderDeleteResponse {

}

/**
 * @method PATCH
 * @url /api/v1/medialib-module/folders/{id}/update-folder
 * @description Изменение
 */
export interface FolderUpdateRequest {
  id: number
  // body params
  name: string
  desc: string
  parent_id: number | null
}

/**
 * @method PATCH
 * @url /api/v1/medialib-module/folders/{id}/update-folder
 * @description Изменение
 */
export interface FolderUpdateResponse {

}
