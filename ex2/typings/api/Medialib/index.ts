import * as Files from './Files'
import * as FileExtensions from './FileExtensions'
import * as FileTypes from './FileTypes'
import * as Folders from './Folders'

export {
  Files,
  FileExtensions,
  FileTypes,
  Folders
}

export enum Entities {
  MapObject = 1,
  User
}

export enum Groups {
  Photos = 1,
  Files
}
