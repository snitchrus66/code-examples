/* eslint-disable @typescript-eslint/no-empty-interface */
import * as api from '~/typings/api'

export type FileType = {
  id: number
  file_type_name: string
  description: string
}

/**
 * @method GET
 * @url /api/v1/medialib-file-types
 */
export interface FileTypesListRequest extends api.ListOptions {}

/**
 * @method GET
 * @url /api/v1/medialib-file-types
 */
export interface FileTypesListResponse {
  data: FileType[]
}

/**
 * @method POST
 * @url /api/v1/medialib-file-types
 */
export interface FileTypeCreateRequest {}

/**
 * @method POST
 * @url /api/v1/medialib-file-types
 */
export interface FileTypeCreateResponse {}

/**
 * @method PATCH
 * @url /api/v1/medialib-file-types/{id}
 */
export interface FileTypeUpdateRequest {}

/**
 * @method PATCH
 * @url /api/v1/medialib-file-types/{id}
 */
export interface FileTypeUpdateResponse {}


/**
 * @method GET
 * @url /api/v1/medialib-file-types/{id}
 */
export interface FileTypeReadRequest {
  id: number
}

/**
 * @method GET
 * @url /api/v1/medialib-file-types/{id}
 */
export interface FileTypeReadResponse {}

/**
 * @method DELETE
 * @url /api/v1/medialib-file-types/{id}
 */
export interface FileTypeDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/medialib-file-types/{id}
 */
export interface FileTypeDeleteResponse {}
