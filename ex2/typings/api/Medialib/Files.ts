/* eslint-disable @typescript-eslint/no-empty-interface */
import * as api from '~/typings/api'

export type File = {
  created_at: string
  description: string | null
  ext_id: number
  file_id: number
  file_ver_id: number
  filename: string
  folder_id: number
  name: string
  user_id: number
  ver_number: number
}

/**
 * @method GET
 * @url /api/v1/medialib-module/get-list
 * @description Получение списка файлов
 */
export interface FilesListRequest {
  entity: api.Medialib.Entities
  entity_id: number
  group: api.Medialib.Groups
  show?: 'removed'
}

/**
 * @method GET
 * @url /api/v1/medialib-module/get-list
 * @description Получение списка файлов
 */
export interface FilesListResponse {
  data: File[]
  links: {
    ext: {
      [extId: number]: {
        id: number
        ext: string
      }
    }
    file_storage_link: string
    folders: {
      [folderId: number]: api.Medialib.Folders.Folder
    }
    users: {
      first_name: string
      last_name: string
      user_id: number
    }[]
  }
}

/**
 * @method POST
 * @url /api/v1/medialib-module/create-file
 * @description Создание файла
 */
export interface FileCreateRequest {
  entity: api.Medialib.Entities
  entity_id: number
  group: api.Medialib.Groups
  filename: globalThis.File // file -> filename - потому что бэк так сказал
  folder_id?: number | null
  file_id?: number | null
}

/**
 * @method POST
 * @url /api/v1/medialib-module/create-file
 * @description Создание файла
 */
export interface FileCreateResponse {}

/**
 * @method POST
 * @url /api/v1/medialib-module/create-version-file
 * @description Создание версии файла
 */
export interface FileVersionCreateRequest {
  entity: api.Medialib.Entities
  entity_id: number
  group: api.Medialib.Groups
  file: globalThis.File
  folder_id: number | null
  parent_id: number
}

/**
 * @method POST
 * @url /api/v1/medialib-module/create-version-file
 * @description Создание версии файла
 */
export interface FileVersionCreateResponse {}

/**
 * @method PATCH
 * @url /api/v1/medialib-module/{id}/update-file
 * @description Удаление
 */
export interface FileUpdateRequest {
  id: number
  entity: api.Medialib.Entities
  entity_id: number
  group: api.Medialib.Groups
  name?: string
  description?: string
}

/**
 * @method PATCH
 * @url /api/v1/medialib-module/{id}/update-file
 * @description Удаление
 */
export interface FileUpdateResponse {}


/**
 * @method GET
 * @url /api/v1/medialib-module/file/{filename}
 * @description Запрос одного файла
 */
export interface FileReadRequest {
  filename: string
  // get params
  group: api.Medialib.Groups
  file_ver_id: number
  entity: api.Medialib.Entities
  entity_id: number
  size?: 'min' | 'medium' | 'big'
}

/**
 * @method GET
 * @url /api/v1/medialib-module/file/{filename}
 * @description Запрос одного файла
 */
export interface FileReadResponse {}

/**
 * @method DELETE
 * @url /api/v1/medialib-module/{id}/soft
 * @description Удаление мягкое
 */
export interface FileDeleteSoftRequest {
  id: number
  group: api.Medialib.Groups
  entity: api.Medialib.Entities
  entity_id: number
  file_ver_id?: number
}

/**
 * @method DELETE
 * @url /api/v1/medialib-module/{id}/soft
 * @description Удаление мягкое
 */
export interface FileDeleteSoftResponse {

}

/**
 * @method DELETE
 * @url /api/v1/medialib-module/{id}/force
 * @description Удаление принудительное
 */
export interface FileDeleteForceRequest {
  id: number
  group: api.Medialib.Groups
  entity: api.Medialib.Entities
  entity_id: number
  file_ver_id?: number
}

/**
 * @method DELETE
 * @url /api/v1/medialib-module/{id}/force
 * @description Удаление принудительное
 */
export interface FileDeleteForceResponse {

}

/**
 * @method PUT
 * @url /api/v1/medialib-module/{id}/recover
 * @description Удаление
 */
export interface FileRecoverRequest {
  id: number
  group: api.Medialib.Groups
  entity: api.Medialib.Entities
  entity_id: number
  file_ver_id?: number
}

/**
 * @method PUT
 * @url /api/v1/medialib-module/{id}/recover
 * @description Удаление
 */
export interface FileRecoverResponse {

}
