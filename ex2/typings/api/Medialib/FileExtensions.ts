/* eslint-disable @typescript-eslint/no-empty-interface */
import * as api from '~/typings/api'

export type FileExtension = {
  ext_id: number
  file_type_id: number
  ext: string
  description: string
}

export type FileExtensionLinks = {
  file_types: {
    file_type_id: number
    file_type_name: string
    description: string | null
  }[]
}

/**
 * @method GET
 * @url /api/v1/medialib-file-extensions
 */
export interface FileExtensionsListRequest extends api.ListOptions {}

/**
 * @method GET
 * @url /api/v1/medialib-file-extensions
 */
export interface FileExtensionsListResponse {
  data: FileExtension[]
  links: FileExtensionLinks
}

/**
 * @method POST
 * @url /api/v1/medialib-file-extensions
 */
export interface FileExtensionCreateRequest {}

/**
 * @method POST
 * @url /api/v1/medialib-file-extensions
 */
export interface FileExtensionCreateResponse {}

/**
 * @method PATCH
 * @url /api/v1/medialib-file-extensions/{id}
 */
export interface FileExtensionUpdateRequest {}

/**
 * @method PATCH
 * @url /api/v1/medialib-file-extensions/{id}
 */
export interface FileExtensionUpdateResponse {}


/**
 * @method GET
 * @url /api/v1/medialib-file-extensions/{id}
 */
export interface FileExtensionReadRequest {
  id: number
}

/**
 * @method GET
 * @url /api/v1/medialib-file-extensions/{id}
 */
export interface FileExtensionReadResponse {}

/**
 * @method DELETE
 * @url /api/v1/medialib-file-extensions/{id}
 */
export interface FileExtensionDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/medialib-file-extensions/{id}
 */
export interface FileExtensionDeleteResponse {}
