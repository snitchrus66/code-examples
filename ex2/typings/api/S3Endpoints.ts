/* eslint-disable @typescript-eslint/no-empty-interface */
export type S3Endpoint = {
  id: number
  s3_endpoint_url: string
  s3_public_key: string
  s3_private_key: string
  s3_bucket: string
  description: string | null
}

/**
  * @method GET
  * @url /api/v1/s3-endpoints
  * @description Получение списка эндпоинтов S3
  */
export interface S3EndpointsListRequest {

}

/**
  * @method GET
  * @url /api/v1/s3-endpoints
  * @description Получение списка эндпоинтов S3
  */
export interface S3EndpointsListResponse {
  data: S3Endpoint[]
}

/**
  * @method POST
  * @url /api/v1/s3-endpoints
  * @description Создание эндпоинта S3
  */
export interface S3EndpointCreateRequest {}

/**
  * @method POST
  * @url /api/v1/s3-endpoints
  * @description Создание эндпоинта S3
  */
export interface S3EndpointCreateResponse {}

/**
  * @method GET
  * @url /api/v1/s3-endpoints/{id}
  * @description Запрос одного эндпоинта S3
  */
export interface S3EndpointReadRequest {
  id: number
}

/**
  * @method GET
  * @url /api/v1/s3-endpoints/{id}
  * @description Запрос одного эндпоинта S3
  */
export interface S3EndpointReadResponse {
  data: S3Endpoint
}

/**
  * @method DELETE
  * @url /api/v1/s3-endpoints/{id}
  * @description Удаление эндпоинта S3
  */
export interface S3EndpointDeleteRequest {
  id: number
}

/**
  * @method DELETE
  * @url /api/v1/s3-endpoints/{id}
  * @description Удаление эндпоинта S3
  */
export interface S3EndpointDeleteResponse {

}

/**
  * @method PATCH
  * @url /api/v1/s3-endpoints/{id}
  * @description Изменение эндпоинта S3
  */
export interface S3EndpointUpdateRequest {
  id: number
}

/**
  * @method PATCH
  * @url /api/v1/s3-endpoints/{id}
  * @description Изменение эндпоинта S3
  */
export interface S3EndpointUpdateResponse {

}
