/* eslint-disable @typescript-eslint/no-empty-interface */
import * as api from '~/typings/api'

export type Videcam = {
  id: number
  object_id: number
  videcam_model_id: number
  s3_endpoint_id: number
  serial_number: string
  fqdn: string
  ip_address: string
  rtsp_proxy_external_port: string
  rtsp_proxy_username: string
  rtsp_proxy_password: string
  fps: string
  name: string
  login: string
  password: string
  http_port: string
  video_port: string
  status_flag: number
  frame_width: number
  frame_height: number
  delay: number
  max_age: number
  max_size: number
  detection_zone: string
  description: string | null
}

/**
  * @method GET
  * @url /api/v1/videcams
  * @description Получение списка видеокамер
  */
export interface VidecamsListRequest extends api.ListOptions {
  objects_type?: number[]
  objects_id?: number[]
}

/**
  * @method GET
  * @url /api/v1/videcams
  * @description Получение списка видеокамер
  */
export interface VidecamsListResponse extends api.ListOfItemsResponse {
  data: Videcam[]
}

/**
  * @method POST
  * @url /api/v1/videcams
  * @description Создание видеокамеры
  */
export interface VidecamCreateRequest {}

/**
  * @method POST
  * @url /api/v1/videcams
  * @description Создание видеокамеры
  */
export interface VidecamCreateResponse {}

/**
  * @method GET
  * @url /api/v1/videcams/{id}
  * @description Запрос одной видеокамеры
  */
export interface VidecamReadRequest {
  id: number
}

/**
  * @method GET
  * @url /api/v1/videcams/{id}
  * @description Запрос одной видеокамеры
  */
export interface VidecamReadResponse {
  data: Videcam
}

/**
  * @method DELETE
  * @url /api/v1/videcams/{id}
  * @description Удаление видеокамеры
  */
export interface VidecamDeleteRequest {
  id: number
}

/**
  * @method DELETE
  * @url /api/v1/videcams/{id}
  * @description Удаление видеокамеры
  */
export interface VidecamDeleteResponse {

}

/**
  * @method PATCH
  * @url /api/v1/videcams/{id}
  * @description Изменение видеокамеры
  */
export interface VidecamUpdateRequest {
  id: number
}

/**
  * @method PATCH
  * @url /api/v1/videcams/{id}
  * @description Изменение видеокамеры
  */
export interface VidecamUpdateResponse {

}
