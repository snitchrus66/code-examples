/* eslint-disable @typescript-eslint/no-empty-interface */
export type VidecamModel = {
  id: number
  model_name: string
  frame_width: number
  frame_height: number
  channel_1: string
  channel_2: string
  channel_3: string
  description: string | null
}
  
/**
  * @method GET
  * @url /api/v1/videcam-models
  * @description Получение списка моделей видеокамер
  */
export interface VidecamModelsListRequest {

}

/**
  * @method GET
  * @url /api/v1/videcam-models
  * @description Получение списка моделей видеокамер
  */
export interface VidecamModelsListResponse {
  data: VidecamModel[]
}
  
/**
  * @method POST
  * @url /api/v1/videcam-models
  * @description Создание модели видеокамеры
  */
export interface VidecamModelCreateRequest {}
  
/**
  * @method POST
  * @url /api/v1/videcam-models
  * @description Создание модели видеокамеры
  */
export interface VidecamModelCreateResponse {}
  
/**
  * @method GET
  * @url /api/v1/videcam-models/{id}
  * @description Запрос одного модели видеокамеры
  */
export interface VidecamModelReadRequest {
  id: number
}
  
/**
  * @method GET
  * @url /api/v1/videcam-models/{id}
  * @description Запрос одного модели видеокамеры
  */
export interface VidecamModelReadResponse {
  data: VidecamModel
}
  
/**
  * @method DELETE
  * @url /api/v1/videcam-models/{id}
  * @description Удаление модели видеокамеры
  */
export interface VidecamModelDeleteRequest {
  id: number
}
  
/**
  * @method DELETE
  * @url /api/v1/videcam-models/{id}
  * @description Удаление модели видеокамеры
  */
export interface VidecamModelDeleteResponse {
  
}
  
/**
  * @method PATCH
  * @url /api/v1/videcam-models/{id}
  * @description Изменение модели видеокамеры
  */
export interface VidecamModelUpdateRequest {
  id: number
}
  
/**
  * @method PATCH
  * @url /api/v1/videcam-models/{id}
  * @description Изменение модели видеокамеры
  */
export interface VidecamModelUpdateResponse {
  
}
  