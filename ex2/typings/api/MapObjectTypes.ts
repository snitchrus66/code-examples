/* eslint-disable @typescript-eslint/no-empty-interface */
export type MapObjectType = {
  id: number
  name: string
  icon: string
  scale: string
}

/**
 * @method GET
 * @url /api/v1/map-object-types
 * @description Получение возможных типов объектов
 */
export interface MapObjectTypesListRequest {
  page?: number
  items?: 'all'
}

/**
 * @method GET
 * @url /api/v1/map-object-types
 * @description Получение возможных типов объектов
 */
export interface MapObjectTypesListResponse {
  data: MapObjectType[]
  links: {
    storage: string
    first: string
    last: string
    prev: string | null
    next: string | null
  }
  meta: {
    current_page: number
    from: number
    last_page: number
    path: string
    per_page: string
    to: number
    total: number
  }
}

/**
 * @method POST
 * @url /api/v1/map-object-types
 * @description Добавление
 */
export interface MapObjectTypeCreateRequest {}

/**
 * @method POST
 * @url /api/v1/map-object-types
 * @description Добавление
 */
export interface MapObjectTypeCreateResponse {}

/**
 * @method GET
 * @url /api/v1/map-object-types/{id}
 * @description Запрос одного типа объекта
 */
export interface MapObjectTypeReadRequest {
  id: number
}

/**
 * @method GET
 * @url /api/v1/map-object-types/{id}
 * @description Запрос одного типа объекта
 */
export interface MapObjectTypeReadResponse {
  data: MapObjectType
  links: {
    storage: string
  }
}

/**
 * @method POST
 * @url /api/v1/map-object-types/{id}
 * @description Изменение
 */
export interface MapObjectTypeUpdateRequest {
  id: number
}

/**
 * @method POST
 * @url /api/v1/map-object-types/{id}
 * @description Изменение
 */
export interface MapObjectTypeUpdateResponse {}

/**
 * @method DELETE
 * @url /api/v1/map-object-types/{id}
 * @description Удаление
 */
export interface MapObjectTypeDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/map-object-types/{id}
 * @description Удаление
 */
export interface MapObjectTypeDeleteResponse {}
