/* eslint-disable @typescript-eslint/no-empty-interface */
export type StaffSchedule = {
  id: number
  name: string
  counterparty_id: number
  version_number: number
  order_number: number
  order_date: string
  start_date: string
  signed: boolean
  description: string | null
}

/**
 * @method GET
 * @url /api/v1/staff-schedules
 * @description Получение списка штатных расписаний
 */
export interface StaffSchedulesListRequest {

}

/**
 * @method GET
 * @url /api/v1/staff-schedules
 * @description Получение списка штатных расписаний
 */
export interface StaffSchedulesListResponse {
  data: StaffSchedule[]
}

/**
 * @method POST
 * @url /api/v1/staff-schedules
 * @description Создание
 */
export interface StaffScheduleCreateRequest {}

/**
 * @method POST
 * @url /api/v1/staff-schedules
 * @description Создание
 */
export interface StaffScheduleCreateResponse {}

/**
 * @method GET
 * @url /api/v1/staff-schedules/{id}
 * @description Запрос одного штатного расписания
 */
export interface StaffScheduleReadRequest {
  id: number
}

/**
 * @method GET
 * @url /api/v1/staff-schedules/{id}
 * @description Запрос одного штатного расписания
 */
export interface StaffScheduleReadResponse {
  data: StaffSchedule
}

/**
 * @method DELETE
 * @url /api/v1/staff-schedules/{id}
 * @description Удаление
 */
export interface StaffScheduleDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/staff-schedules/{id}
 * @description Удаление
 */
export interface StaffScheduleDeleteResponse {

}

/**
 * @method PUT
 * @url /api/v1/staff-schedules/{id}
 * @description Изменение
 */
export interface StaffScheduleUpdateRequest {
  id: number
}

/**
 * @method PUT
 * @url /api/v1/staff-schedules/{id}
 * @description Изменение
 */
export interface StaffScheduleUpdateResponse {

}
