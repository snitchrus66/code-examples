/* eslint-disable @typescript-eslint/no-empty-interface */
export type EmployeePosition = {
  id: number
  name: string
  code: string
  description: string | null
  created_at: string
  updated_at: string
}

/**
 * @method GET
 * @url /api/v1/employee-positions
 * @description Получение списка должностей
 */
export interface EmployeePositionsListRequest {
  search?: string
}

/**
 * @method GET
 * @url /api/v1/employee-positions
 * @description Получение списка должностей
 */
export interface EmployeePositionsListResponse {
  data: EmployeePosition[]
}

/**
 * @method POST
 * @url /api/v1/employee-positions
 * @description Создание
 */
export interface EmployeePositionCreateRequest {}

/**
 * @method POST
 * @url /api/v1/employee-positions
 * @description Создание
 */
export interface EmployeePositionCreateResponse {}

/**
 * @method GET
 * @url /api/v1/employee-positions/{id}
 * @description Запрос одной должности
 */
export interface EmployeePositionReadRequest {
  id: number
}

/**
 * @method GET
 * @url /api/v1/employee-positions/{id}
 * @description Запрос одной должности
 */
export interface EmployeePositionReadResponse {
  data: Position
}

/**
 * @method DELETE
 * @url /api/v1/employee-positions/{id}
 * @description Удаление
 */
export interface EmployeePositionDeleteRequest {
  id: number
}

/**
 * @method DELETE
 * @url /api/v1/employee-positions/{id}
 * @description Удаление
 */
export interface EmployeePositionDeleteResponse {

}

/**
 * @method PUT
 * @url /api/v1/employee-positions/{id}
 * @description Изменение
 */
export interface EmployeePositionUpdateRequest {
  id: number
}

/**
 * @method PUT
 * @url /api/v1/employee-positions/{id}
 * @description Изменение
 */
export interface EmployeePositionUpdateResponse {

}
