import * as Auth from './Auth'
import * as Controllers from './Controllers'
import * as Counterparties from './Counterparties'
import * as Departments from './Departments'
import * as EmployeePositions from './EmployeePositions'
import * as MapObjectTypes from './MapObjectTypes'
import * as MapObjects from './MapObjects'
import * as Medialib from './Medialib'
import * as PhotosMapObject from './PhotosMapObject'
import * as S3Endpoints from './S3Endpoints'
import * as StaffSchedulePositions from './StaffSchedulePositions'
import * as StaffSchedules from './StaffSchedules'
import * as Videcams from './Videcams'
import * as VideoList from './VideoList'


export {
  Auth,
  Controllers,
  Counterparties,
  Departments,
  EmployeePositions,
  MapObjectTypes,
  MapObjects,
  Medialib,
  PhotosMapObject,
  S3Endpoints,
  StaffSchedulePositions,
  StaffSchedules,
  Videcams,
  VideoList
}

export interface ListOptions {
  search?: string
  page?: number
  orderby?: string
  direction?: 'asc' | 'desc'
  items?: 'all'
}

export interface ListOfItemsResponse {
  links: {
    first: string
    last: string
    next: string | null
    prev: string | null
  }
  meta: {
    current_page: number
    from: number
    last_page: number
    path: string
    per_page: string
    to: number
    total: number
  }
}

export interface LoginPostRequest {
  email: string
  password: string
}

export interface LoginPostResponse {
  token: string
  token_type: 'bearer'
  expires_in: number
}

export interface ContactType {
  id: number
  name: string
  icon: string
}

export interface ContactTypesGetResponse {
  [contactTypeKey: string]: ContactType
}

export interface ContactTypePostRequest {
  contact_type: string
  name: string
  icon: string
}

export interface ContactTypePostResponse {
  res: '0' | 1
}

export interface ContactTypePutRequest {
  contact_type: string
  name: string
  icon: string
}

export interface TreeListItem {
  id: number | null
  parent_id: number | null
}
