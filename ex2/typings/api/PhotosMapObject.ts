/* eslint-disable @typescript-eslint/no-empty-interface */
export type MapObjectPhoto = {
  id: number
  file: string
  description: string | null
  object_id: number
  user_id: number
  user: {
    first_name: string
    last_name: string
  }
  created_at: string
}

/**
 * @method GET
 * @url /api/v1/photos-map-object
 * @description Получение списка фотографий объекта
 */
export interface PhotosMapObjectListRequest {
  object_id: number
}

/**
 * @method GET
 * @url /api/v1/photos-map-object
 * @description Получение списка фотографий объекта
 */
export interface PhotosMapObjectListResponse {
  data: MapObjectPhoto[]
  links: {
    storage: string
  }
}

/**
 * @method GET
 * @url /api/v1/photos-map-object/{filename}
 * @description Получение фотографии объекта
 */
export interface  PhotosMapObjectReadRequest {
  filename: string
  size: 'min' | 'medium' | 'big'
}

/**
 * @method GET
 * @url /api/v1/photos-map-object
 * @description Получение фотографии объекта
 */
export interface  PhotosMapObjectReadResponse {}

/**
 * @method POST
 * @url /api/v1/photos-map-object
 * @description Загрузка фотографии объекта
 */
export interface PhotosMapObjectCreateRequest {
  object_id: number
  file: File
}

/**
 * @method POST
 * @url /api/v1/photos-map-object
 * @description Загрузка фотографии объекта
 */
export interface PhotosMapObjectCreateResponse {
  data: {
    id: number
    file: string
    object_id: number
    user_id: number
    created_at: string
  }
  links: {
    storage: string
  }
}

/**
 * @method PATCH
 * @url /api/v1/photos-map-object/{id}
 * @description Создание/обновление описания к фотографии
 */
export interface PhotosMapObjectUpsertDescriptionRequest {
  id: number
  description: string
}

/**
 * @method PATCH
 * @url /api/v1/photos-map-object/{id}
 * @description Создание/обновление описания к фотографии
 */
export interface PhotosMapObjectUpsertDescriptionResponse {}

/**
 * @method DELETE
 * @url /api/v1/photos-map-object/{filename}
 * @description Удаление фотографии
 */
export interface PhotosMapObjectDeleteRequest {
  id: number
}


/**
 * @method DELETE
 * @url /api/v1/photos-map-object/{filename}
 * @description Удаление фотографии
 */
export interface PhotosMapObjectDeleteResponse {}
