export type PermissionsDictionary = {
  // users
  'create users': boolean
  'read users': boolean
  'update users': boolean
  'delete users': boolean
  'import users': boolean
  'read users avatars': boolean
  // roles
  'create roles': boolean
  'read roles': boolean
  'update roles': boolean
  'delete roles': boolean
  'assign roles': boolean
  // contacts
  'create contacts': boolean
  'read contacts': boolean
  'update contacts': boolean
  'delete contacts': boolean
  // contact types
  'create contact types': boolean
  'read contact types': boolean
  'update contact types': boolean
  'delete contact types': boolean
  // counterparties
  'create counterparties': boolean
  'read counterparties': boolean
  'update counterparties': boolean
  'delete counterparties': boolean
  // assignments
  'create assignments': boolean
  'read assignments': boolean
  'update assignments': boolean
  'delete assignments': boolean
  // map objects
  'create map objects': boolean
  'read map objects': boolean
  'update map objects': boolean
  'delete map objects': boolean
  // map object types
  'create map object types': boolean
  'read map object types': boolean
  'update map object types': boolean
  'delete map object types': boolean
  // map object medialib items
  'create map object medialib items': boolean           // Кнопки Добавить файл и фото должны отсутствовать у тех, у кого нет такого права. В идеале, чтобы они не могли никак отправить на создание файл (на твое усмотрение более сложные и действенные меры).
  'read map object medialib items': boolean             // Ссылки в гл меню и навбаре есть у тех, кто имеет это право. В гл. меню добавил, в навбар - нет. Должно быть невозможно подгрузить медиа-библиотеку, не имея этого права и запросы не должны соответственно улетать на получение списка файлов. Заблокируй компонент весь, короче.
  'update map object medialib items': boolean           // Минимальная мера - кнопки редактирования файлов и фоток должны отсутствовать у тех, кто не имеет право
  'delete my map object medialib items': boolean        // Минимальная мера - кнопки удаления для файлов, user_id (тот, кто их заливал) у которых сам пользователь, должны отсутстваоать, если нет права
  'delete any users map object medialib items': boolean // Минимальная мера - кнопки удаления для файлов, user_id (тот, кто их заливал) у которых другие пользователи (не он), должны отсутстваоать, если нет права
  'read deleted map object medialib items': boolean
  'recover my map object medialib items': boolean
  'recover any users map object medialib items': boolean
  'force delete map object medialib items': boolean
  // user medialib items
  'create user medialib items': boolean           // Кнопки Добавить файл и фото должны отсутствовать у тех, у кого нет такого права. В идеале, чтобы они не могли никак отправить на создание файл (на твое усмотрение более сложные и действенные меры).
  'read user medialib items': boolean             // Ссылки в гл меню и навбаре есть у тех, кто имеет это право. В гл. меню добавил, в навбар - нет. Должно быть невозможно подгрузить медиа-библиотеку, не имея этого права и запросы не должны соответственно улетать на получение списка файлов. Заблокируй компонент весь, короче.
  'update user medialib items': boolean           // Минимальная мера - кнопки редактирования файлов и фоток должны отсутствовать у тех, кто не имеет право
  'delete my user medialib items': boolean        // Минимальная мера - кнопки удаления для файлов, user_id (тот, кто их заливал) у которых сам пользователь, должны отсутстваоать, если нет права
  'delete any users user medialib items': boolean // Минимальная мера - кнопки удаления для файлов, user_id (тот, кто их заливал) у которых другие пользователи (не он), должны отсутстваоать, если нет права
  'read deleted user medialib items': boolean
  'recover my user medialib items': boolean
  'recover any users user medialib items': boolean
  'force delete user medialib items': boolean
  // medialib-file-extensions
  'create medialib-file-extensions': boolean
  'read medialib-file-extensions': boolean
  'update medialib-file-extensions': boolean
  'delete medialib-file-extensions': boolean
  // medialib-file-types
  'create medialib-file-types': boolean
  'read medialib-file-types': boolean
  'update medialib-file-types': boolean
  'delete medialib-file-types': boolean
  // employee positions
  'create employee positions': boolean
  'read employee positions': boolean
  'update employee positions': boolean
  'delete employee positions': boolean
  // departments
  'create departments': boolean
  'read departments': boolean
  'update departments': boolean
  'delete departments': boolean
  // staff schedules
  'create staff schedules': boolean
  'read staff schedules': boolean
  'update staff schedules': boolean
  'delete staff schedules': boolean
  'sign staff schedules': boolean
  // metric controllers
  'create metric controller': boolean
  'read metric controller': boolean
  'update metric controller': boolean
  'delete metric controller': boolean
  // videcams
  'create videcams': boolean
  'read videcams': boolean
  'update videcams': boolean
  'delete videcams': boolean
  // messenger
  'create messenger': boolean
  'read messenger': boolean
  'update messenger': boolean
  'delete messenger': boolean
  // s3 endpoints
  'create s3 endpoints': boolean
  'read s3 endpoints': boolean
  'update s3 endpoints': boolean
  'delete s3 endpoints': boolean
  // db object types
  'create db object type': boolean
  'read db object type': boolean
  'update db object type': boolean
  'delete db object type': boolean
  // processing modules
  'create processing module': boolean
  'read processing module': boolean
  'update processing module': boolean
  'delete processing module': boolean
}

export type User = {
  id: number
  first_name: string
  last_name: string
  middle_name: string
  config: string
  email: string
  email_verified_at: string | null
  deleted_at: string | null
  created_at: string
  updated_at: string
  roles: string[]
  permissions: (keyof PermissionsDictionary)[]
  can: PermissionsDictionary
  photo_url: {
    width_128: string
    width_512: string
  }
  vcard: string
  qr: string
}
