/* eslint-disable @typescript-eslint/no-empty-interface */
import * as api from '~/typings/api'

export type VirtualParentFile =
  & api.Medialib.Files.File
  & {
    parent_of: number
  }
