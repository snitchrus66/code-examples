import * as Medialib from './Medialib'

export {
  Medialib
}

export type MainMenuItem = {
  icon: string | string[]
  name: string
  route: string
}

export type MainMenuItemsGroup = {
  icon: string | string[]
  name: string
  items: MainMenuItem[]
}

export type UserConfigValue = string | number | string[] | number[] | null
