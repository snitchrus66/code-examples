const spec = [
  'fullscreen',
  'fullscreenEnabled',
  'fullscreenElement',
  'fullscreenchange',
  'fullscreenerror',
  'exitFullscreen',
  'requestFullscreen'
]

const webkit = [
  'webkitIsFullScreen',
  'webkitFullscreenEnabled',
  'webkitFullscreenElement',
  'webkitfullscreenchange',
  'webkitfullscreenerror',
  'webkitExitFullscreen',
  'webkitRequestFullscreen'
]

const moz = [
  'mozFullScreen',
  'mozFullScreenEnabled',
  'mozFullScreenElement',
  'mozfullscreenchange',
  'mozfullscreenerror',
  'mozCancelFullScreen',
  'mozRequestFullScreen'
]

const ms = [
  '',
  'msFullscreenEnabled',
  'msFullscreenElement',
  'MSFullscreenChange',
  'MSFullscreenError',
  'msExitFullscreen',
  'msRequestFullscreen'
]

declare global {
  interface Document {
    [anyString: string]: any
  }
  interface Element {
    [anyString: string]: any
  }
}

const fsVendorKeywords = (function getFullscreenApi() {
  const fullscreenEnabled = [spec[1], webkit[1], moz[1], ms[1]].find((prefix) => document[prefix])
  return [spec, webkit, moz, ms].find((vendor) => {
    return vendor.find((prefix) => prefix === fullscreenEnabled)
  }) || []
})()

function handleEvent(eventType: string, event: Event) {
  document[spec[0]] = document[fsVendorKeywords[0]] ||
		!!document[fsVendorKeywords[2]] || false
  document[spec[1]] = document[fsVendorKeywords[1]] || false
  document[spec[2]] = document[fsVendorKeywords[2]] || null
  //@ts-ignore
  document.dispatchEvent(new Event(eventType), event.target)
}

function setup() {
  // fullscreen
  document[spec[0]] = document[fsVendorKeywords[0]] ||
		!!document[fsVendorKeywords[2]] || false

  // fullscreenEnabled
  document[spec[1]] = document[fsVendorKeywords[1]] || false

  // fullscreenElement
  document[spec[2]] = document[fsVendorKeywords[2]] || null

  // onfullscreenchange
  document.addEventListener(fsVendorKeywords[3], handleEvent.bind(document, spec[3]), false)

  // onfullscreenerror
  document.addEventListener(fsVendorKeywords[4], handleEvent.bind(document, spec[4]), false)

  // exitFullscreen
  document[spec[5]] = function () { return document[fsVendorKeywords[5]]() }

  // requestFullscreen
  Element.prototype[spec[6]] = function () {
    // eslint-disable-next-line prefer-spread, prefer-rest-params
    return this[fsVendorKeywords[6]].apply(this, arguments)
  }
}

export default document[spec[1]] ? {} : setup()
