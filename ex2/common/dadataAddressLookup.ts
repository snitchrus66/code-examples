import axios from 'axios'

interface reqParams{
  query: string
  type_suggest: string
  lat: number | null
  lon: number | null
  radius_meters: number | null
}

export default async function (address: string, restriction?: any) {
  if (address) {
    let params: reqParams = {
      query: address,
      type_suggest: 'address',
      lat: null,
      lon: null,
      radius_meters: null
    }
    if (restriction && restriction.type === 'geo') {
      params = { ...params, ...restriction.filter}
    }
    const response = await axios
      .get(
        '/api/v1/dadata/suggest',
        {
          params: params
        })
    if (response?.data?.length) {
      return response.data.filter((item: any) => {
        return item.data.geo_lat
      }).map((item: any) => { 
        return { 
          value: item.value, 
          lat: item.data.geo_lat,
          lon: item.data.geo_lon
        } 
      })
    } else {
      return []
    }
  } else {
    return []
  }
}
