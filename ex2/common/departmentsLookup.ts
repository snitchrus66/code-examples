import axios from 'axios'
import * as api from '~/typings/api'

export default async function (departmentSearchString: string) {
  const params: api.Departments.DepartmentsListRequest = { search: departmentSearchString }
  const response = await axios.get<api.Departments.DepartmentsListResponse>(`/api/v1/departments`, { params })

  if (response?.data?.data?.length) {
    const result = response.data.data.map((item) => {
      if (item.name) {
        // @ts-ignore
        item.value = item.name
      } else {
        // @ts-ignore
        item.value = ''
      }

      return item
    })

    return result
  } else {
    return []
  }
}
