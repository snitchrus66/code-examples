import Form from 'vform'

export default function<T extends object>(data: T) {
  return new Form(data) as Form & T
}
