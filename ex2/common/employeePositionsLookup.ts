import axios from 'axios'
import * as api from '~/typings/api'

export default async function (employeePositionSearchString: string) {
  const params: api.EmployeePositions.EmployeePositionsListRequest = { search: employeePositionSearchString }
  const response = await axios.get<api.EmployeePositions.EmployeePositionsListResponse>(`/api/v1/employee-positions`, { params })

  if (response?.data?.data?.length) {
    const result = response.data.data.map((item) => {
      if (item.name) {
        // @ts-ignore
        item.value = item.name
      } else {
        // @ts-ignore
        item.value = ''
      }

      return item
    })

    return result
  } else {
    return []
  }
}
