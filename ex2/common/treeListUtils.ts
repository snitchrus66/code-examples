import { differenceWith } from 'lodash-es'

type TreeListItem<ID_PROP extends string = 'id', PARENT_ID_PROP extends string = 'parent_id'> =
  {
    [key in ID_PROP | PARENT_ID_PROP]: number | null
  }

export function getAllChildrenTreeListItems<T, ID_PROP extends string = 'id', PARENT_ID_PROP extends string = 'parent_id'>(
  treeAsList: (TreeListItem<ID_PROP, PARENT_ID_PROP> & T)[],
  currentParentId: number | null,
  memo: (TreeListItem<ID_PROP, PARENT_ID_PROP> & T)[],
  options = {
    id_prop: 'id',
    parent_id_prop: 'parent_id'
  }
) {
  const children = treeAsList.filter(item => item[options.parent_id_prop as PARENT_ID_PROP] === currentParentId)

  if (children.length) {
    children.forEach((item) => {
      memo.push(item)
      getAllChildrenTreeListItems(treeAsList, item[options.id_prop as ID_PROP], memo)
    })
  }

  return memo
}

export function getAllTreeListItemsWithExcept<T, ID_PROP extends string = 'id', PARENT_ID_PROP extends string = 'parent_id'>(
  treeAsList: (TreeListItem<ID_PROP, PARENT_ID_PROP> & T)[],
  currentItem: (TreeListItem<ID_PROP, PARENT_ID_PROP> & T),
  except?: {
    self?: boolean
    children?: boolean
    parent?: 'one' | 'all'
  },
  options = {
    id_prop: 'id',
    parent_id_prop: 'parent_id'
  }
): (TreeListItem<ID_PROP, PARENT_ID_PROP> & T)[] {
  const memo: (TreeListItem<ID_PROP, PARENT_ID_PROP> & T)[] = []
  let itemsToExpect: (TreeListItem<ID_PROP, PARENT_ID_PROP> & T)[] = []

  if (except) {
    if (except.self) {
      itemsToExpect = [...itemsToExpect, currentItem]
    }
    if (except.children) {
      itemsToExpect = [...itemsToExpect, ...getAllChildrenTreeListItems(treeAsList, currentItem[options.id_prop as ID_PROP], memo, options)]
    }
    if (except.parent === 'one') {
      const parent = treeAsList.find((item) => {
        const itemId = item[options.id_prop as ID_PROP] as number | null
        const currentItemParentId = currentItem[options.parent_id_prop as PARENT_ID_PROP] as number | null

        return itemId === currentItemParentId
      })

      if (parent) {
        itemsToExpect = [...itemsToExpect, parent]
      }
    } else if (except.parent === 'all') {
      itemsToExpect = [
        ...itemsToExpect,
        ...getAllTreeListItemsWithExcept(
          treeAsList,
          currentItem,
          { self: true, children: true },
          options
        )
      ]
    }
  }

  return differenceWith(treeAsList, itemsToExpect, (a, b) => a[options.id_prop as ID_PROP] === b[options.id_prop as ID_PROP])
}
