import axios from 'axios'

export default async function (fio: string) {
  if (fio) {
    const response = await axios
      .get(
        '/api/v1/dadata/suggest',
        {
          params: {
            query: fio,
            type_suggest: 'fio'
          }
        })

    if (response?.data?.length) {
      return response.data.map((item: any) => { return { value: item.value } })
    } else {
      return []
    }
  } else {
    return []
  }
}
