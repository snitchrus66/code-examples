import axios from 'axios'
import * as api from '~/typings/api'

export default async function (counterpartySearchString: string) {
  const params: api.Counterparties.CounterpartiesListRequest = { search: counterpartySearchString }
  const response = await axios.get<api.Counterparties.CounterpartiesListResponse>(`/api/v1/counterparties`, { params })

  if (response?.data?.data?.length) {
    const result = response.data.data.map((item) => {
      if (item.rus_name) {
        // @ts-ignore
        item.value = item.rus_name
      } else if (item.person_name) {
        // @ts-ignore
        item.value = item.person_name
      } else {
        // @ts-ignore
        item.value = ''
      }

      return item
    })

    return result
  } else {
    return []
  }
}
