import axios from 'axios'

export default async function (inn: string) {
  if (inn) {
    const response = await axios
      .get(
        '/api/v1/dadata/suggest',
        {
          params: {
            query: inn,
            type_suggest: 'party'
          }
        })

    if (response?.data?.length) {
      return response.data
    } else {
      return []
    }
  } else {
    return []
  }
}
