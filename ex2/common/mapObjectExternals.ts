export function generateYandexMapsLink(latitude: number, longitude: number, zoom: number = 15) {
  const link = `https://yandex.ru/maps/?`
  const params: { [key: string]: string } = {
    ll: `ll=${longitude},${latitude}`,
    z: `z=${zoom}`,
    mode: `mode=whatshere`,
    whatsherePoint: `whatshere[point]=${longitude},${latitude}`,
    whatshereZoom: `whatshere[zoom]=${zoom}`
  }


  return `${link}${Object.keys(params).map(key => params[key]).join('&')}`
}

export function generateYandexNavigatorLink(latitude: number, longitude: number) {
  const link = `yandexnavi://build_route_on_map?`
  const params: { [key: string]: string } = {
    lon_to: `lon_to=${longitude}`,
    lat_to: `lat_to=${latitude}`
  }

  return `${link}${Object.keys(params).map(key => params[key]).join('&')}`
}
